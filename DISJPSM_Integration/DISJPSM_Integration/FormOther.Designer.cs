﻿namespace DISJPSM_Integration
{
    partial class FormOther
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFormConnectionTester = new System.Windows.Forms.Button();
            this.btnFormDatabaseCleaning = new System.Windows.Forms.Button();
            this.btnFormIntegrationTotal = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFormConnectionTester
            // 
            this.btnFormConnectionTester.Location = new System.Drawing.Point(12, 12);
            this.btnFormConnectionTester.Name = "btnFormConnectionTester";
            this.btnFormConnectionTester.Size = new System.Drawing.Size(200, 23);
            this.btnFormConnectionTester.TabIndex = 0;
            this.btnFormConnectionTester.Text = "FormConnectionTester";
            this.btnFormConnectionTester.UseVisualStyleBackColor = true;
            this.btnFormConnectionTester.Click += new System.EventHandler(this.btnFormConnectionTester_Click);
            // 
            // btnFormDatabaseCleaning
            // 
            this.btnFormDatabaseCleaning.Location = new System.Drawing.Point(12, 41);
            this.btnFormDatabaseCleaning.Name = "btnFormDatabaseCleaning";
            this.btnFormDatabaseCleaning.Size = new System.Drawing.Size(200, 23);
            this.btnFormDatabaseCleaning.TabIndex = 1;
            this.btnFormDatabaseCleaning.Text = "FormDatabaseCleaning";
            this.btnFormDatabaseCleaning.UseVisualStyleBackColor = true;
            this.btnFormDatabaseCleaning.Click += new System.EventHandler(this.btnFormDatabaseCleaning_Click);
            // 
            // btnFormIntegrationTotal
            // 
            this.btnFormIntegrationTotal.Location = new System.Drawing.Point(12, 70);
            this.btnFormIntegrationTotal.Name = "btnFormIntegrationTotal";
            this.btnFormIntegrationTotal.Size = new System.Drawing.Size(200, 23);
            this.btnFormIntegrationTotal.TabIndex = 2;
            this.btnFormIntegrationTotal.Text = "FormIntegrationTotal";
            this.btnFormIntegrationTotal.UseVisualStyleBackColor = true;
            this.btnFormIntegrationTotal.Click += new System.EventHandler(this.btnFormIntegrationTotal_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 99);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(200, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(218, 12);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(200, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(218, 41);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(200, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(218, 70);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(200, 23);
            this.button7.TabIndex = 6;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(218, 99);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(200, 23);
            this.button8.TabIndex = 7;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(424, 12);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(200, 23);
            this.button9.TabIndex = 8;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(424, 41);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(200, 23);
            this.button10.TabIndex = 9;
            this.button10.Text = "button10";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(424, 70);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(200, 23);
            this.button11.TabIndex = 10;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(424, 99);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(200, 23);
            this.button12.TabIndex = 11;
            this.button12.Text = "button12";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // FormOther
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 136);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnFormIntegrationTotal);
            this.Controls.Add(this.btnFormDatabaseCleaning);
            this.Controls.Add(this.btnFormConnectionTester);
            this.Name = "FormOther";
            this.Text = "FormOther";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFormConnectionTester;
        private System.Windows.Forms.Button btnFormDatabaseCleaning;
        private System.Windows.Forms.Button btnFormIntegrationTotal;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
    }
}