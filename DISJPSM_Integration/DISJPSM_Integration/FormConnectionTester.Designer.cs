﻿namespace DISJPSM_Integration
{
    partial class FormConnectionTester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnMain = new System.Windows.Forms.Button();
            this.btnMYSQL = new System.Windows.Forms.Button();
            this.btnMSSQL = new System.Windows.Forms.Button();
            this.listBoxConnMain = new System.Windows.Forms.ListBox();
            this.listBoxMYSQL = new System.Windows.Forms.ListBox();
            this.listBoxMSSQL = new System.Windows.Forms.ListBox();
            this.btnConnAll = new System.Windows.Forms.Button();
            this.lblConnMainDone = new System.Windows.Forms.Label();
            this.lblConnMYSQLDone = new System.Windows.Forms.Label();
            this.lblConnMSSQLDone = new System.Windows.Forms.Label();
            this.lblConnMainError = new System.Windows.Forms.Label();
            this.lblConnMainTotal = new System.Windows.Forms.Label();
            this.lblConnMYSQLError = new System.Windows.Forms.Label();
            this.lblConnMYSQLTotal = new System.Windows.Forms.Label();
            this.lblConnMSSQLError = new System.Windows.Forms.Label();
            this.lblConnMSSQLTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnConnMain
            // 
            this.btnConnMain.Location = new System.Drawing.Point(12, 74);
            this.btnConnMain.Name = "btnConnMain";
            this.btnConnMain.Size = new System.Drawing.Size(100, 121);
            this.btnConnMain.TabIndex = 0;
            this.btnConnMain.Text = "Main Connection";
            this.btnConnMain.UseVisualStyleBackColor = true;
            this.btnConnMain.Click += new System.EventHandler(this.btnConnMain_Click);
            // 
            // btnMYSQL
            // 
            this.btnMYSQL.Location = new System.Drawing.Point(12, 201);
            this.btnMYSQL.Name = "btnMYSQL";
            this.btnMYSQL.Size = new System.Drawing.Size(100, 121);
            this.btnMYSQL.TabIndex = 2;
            this.btnMYSQL.Text = "MYSQL";
            this.btnMYSQL.UseVisualStyleBackColor = true;
            this.btnMYSQL.Click += new System.EventHandler(this.btnMYSQL_Click);
            // 
            // btnMSSQL
            // 
            this.btnMSSQL.Location = new System.Drawing.Point(12, 328);
            this.btnMSSQL.Name = "btnMSSQL";
            this.btnMSSQL.Size = new System.Drawing.Size(100, 121);
            this.btnMSSQL.TabIndex = 3;
            this.btnMSSQL.Text = "MSSQL";
            this.btnMSSQL.UseVisualStyleBackColor = true;
            this.btnMSSQL.Click += new System.EventHandler(this.btnMSSQL_Click);
            // 
            // listBoxConnMain
            // 
            this.listBoxConnMain.FormattingEnabled = true;
            this.listBoxConnMain.HorizontalScrollbar = true;
            this.listBoxConnMain.Location = new System.Drawing.Point(118, 74);
            this.listBoxConnMain.Name = "listBoxConnMain";
            this.listBoxConnMain.ScrollAlwaysVisible = true;
            this.listBoxConnMain.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxConnMain.Size = new System.Drawing.Size(432, 121);
            this.listBoxConnMain.TabIndex = 12;
            // 
            // listBoxMYSQL
            // 
            this.listBoxMYSQL.FormattingEnabled = true;
            this.listBoxMYSQL.HorizontalScrollbar = true;
            this.listBoxMYSQL.Location = new System.Drawing.Point(118, 201);
            this.listBoxMYSQL.Name = "listBoxMYSQL";
            this.listBoxMYSQL.ScrollAlwaysVisible = true;
            this.listBoxMYSQL.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxMYSQL.Size = new System.Drawing.Size(432, 121);
            this.listBoxMYSQL.TabIndex = 13;
            // 
            // listBoxMSSQL
            // 
            this.listBoxMSSQL.FormattingEnabled = true;
            this.listBoxMSSQL.HorizontalScrollbar = true;
            this.listBoxMSSQL.Location = new System.Drawing.Point(118, 328);
            this.listBoxMSSQL.Name = "listBoxMSSQL";
            this.listBoxMSSQL.ScrollAlwaysVisible = true;
            this.listBoxMSSQL.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxMSSQL.Size = new System.Drawing.Size(432, 121);
            this.listBoxMSSQL.TabIndex = 14;
            // 
            // btnConnAll
            // 
            this.btnConnAll.Location = new System.Drawing.Point(12, 12);
            this.btnConnAll.Name = "btnConnAll";
            this.btnConnAll.Size = new System.Drawing.Size(538, 56);
            this.btnConnAll.TabIndex = 15;
            this.btnConnAll.Text = "All Connection";
            this.btnConnAll.UseVisualStyleBackColor = true;
            this.btnConnAll.Click += new System.EventHandler(this.btnConnAll_Click);
            // 
            // lblConnMainDone
            // 
            this.lblConnMainDone.AutoSize = true;
            this.lblConnMainDone.Location = new System.Drawing.Point(556, 74);
            this.lblConnMainDone.Name = "lblConnMainDone";
            this.lblConnMainDone.Size = new System.Drawing.Size(97, 13);
            this.lblConnMainDone.TabIndex = 16;
            this.lblConnMainDone.Text = "[lblConnMainDone]";
            // 
            // lblConnMYSQLDone
            // 
            this.lblConnMYSQLDone.AutoSize = true;
            this.lblConnMYSQLDone.Location = new System.Drawing.Point(556, 201);
            this.lblConnMYSQLDone.Name = "lblConnMYSQLDone";
            this.lblConnMYSQLDone.Size = new System.Drawing.Size(111, 13);
            this.lblConnMYSQLDone.TabIndex = 17;
            this.lblConnMYSQLDone.Text = "[lblConnMYSQLDone]";
            // 
            // lblConnMSSQLDone
            // 
            this.lblConnMSSQLDone.AutoSize = true;
            this.lblConnMSSQLDone.Location = new System.Drawing.Point(556, 328);
            this.lblConnMSSQLDone.Name = "lblConnMSSQLDone";
            this.lblConnMSSQLDone.Size = new System.Drawing.Size(111, 13);
            this.lblConnMSSQLDone.TabIndex = 18;
            this.lblConnMSSQLDone.Text = "[lblConnMSSQLDone]";
            // 
            // lblConnMainError
            // 
            this.lblConnMainError.AutoSize = true;
            this.lblConnMainError.Location = new System.Drawing.Point(556, 89);
            this.lblConnMainError.Name = "lblConnMainError";
            this.lblConnMainError.Size = new System.Drawing.Size(93, 13);
            this.lblConnMainError.TabIndex = 19;
            this.lblConnMainError.Text = "[lblConnMainError]";
            // 
            // lblConnMainTotal
            // 
            this.lblConnMainTotal.AutoSize = true;
            this.lblConnMainTotal.Location = new System.Drawing.Point(556, 104);
            this.lblConnMainTotal.Name = "lblConnMainTotal";
            this.lblConnMainTotal.Size = new System.Drawing.Size(95, 13);
            this.lblConnMainTotal.TabIndex = 20;
            this.lblConnMainTotal.Text = "[lblConnMainTotal]";
            // 
            // lblConnMYSQLError
            // 
            this.lblConnMYSQLError.AutoSize = true;
            this.lblConnMYSQLError.Location = new System.Drawing.Point(556, 216);
            this.lblConnMYSQLError.Name = "lblConnMYSQLError";
            this.lblConnMYSQLError.Size = new System.Drawing.Size(107, 13);
            this.lblConnMYSQLError.TabIndex = 21;
            this.lblConnMYSQLError.Text = "[lblConnMYSQLError]";
            // 
            // lblConnMYSQLTotal
            // 
            this.lblConnMYSQLTotal.AutoSize = true;
            this.lblConnMYSQLTotal.Location = new System.Drawing.Point(556, 231);
            this.lblConnMYSQLTotal.Name = "lblConnMYSQLTotal";
            this.lblConnMYSQLTotal.Size = new System.Drawing.Size(109, 13);
            this.lblConnMYSQLTotal.TabIndex = 22;
            this.lblConnMYSQLTotal.Text = "[lblConnMYSQLTotal]";
            // 
            // lblConnMSSQLError
            // 
            this.lblConnMSSQLError.AutoSize = true;
            this.lblConnMSSQLError.Location = new System.Drawing.Point(556, 343);
            this.lblConnMSSQLError.Name = "lblConnMSSQLError";
            this.lblConnMSSQLError.Size = new System.Drawing.Size(107, 13);
            this.lblConnMSSQLError.TabIndex = 23;
            this.lblConnMSSQLError.Text = "[lblConnMSSQLError]";
            // 
            // lblConnMSSQLTotal
            // 
            this.lblConnMSSQLTotal.AutoSize = true;
            this.lblConnMSSQLTotal.Location = new System.Drawing.Point(556, 358);
            this.lblConnMSSQLTotal.Name = "lblConnMSSQLTotal";
            this.lblConnMSSQLTotal.Size = new System.Drawing.Size(109, 13);
            this.lblConnMSSQLTotal.TabIndex = 24;
            this.lblConnMSSQLTotal.Text = "[lblConnMSSQLTotal]";
            // 
            // FormConnectionTester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 461);
            this.Controls.Add(this.lblConnMSSQLTotal);
            this.Controls.Add(this.lblConnMSSQLError);
            this.Controls.Add(this.lblConnMYSQLTotal);
            this.Controls.Add(this.lblConnMYSQLError);
            this.Controls.Add(this.lblConnMainTotal);
            this.Controls.Add(this.lblConnMainError);
            this.Controls.Add(this.lblConnMSSQLDone);
            this.Controls.Add(this.lblConnMYSQLDone);
            this.Controls.Add(this.lblConnMainDone);
            this.Controls.Add(this.btnConnAll);
            this.Controls.Add(this.listBoxMSSQL);
            this.Controls.Add(this.listBoxMYSQL);
            this.Controls.Add(this.listBoxConnMain);
            this.Controls.Add(this.btnMSSQL);
            this.Controls.Add(this.btnMYSQL);
            this.Controls.Add(this.btnConnMain);
            this.Name = "FormConnectionTester";
            this.Text = "FormConnectionTester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnMain;
        private System.Windows.Forms.Button btnMYSQL;
        private System.Windows.Forms.Button btnMSSQL;
        private System.Windows.Forms.ListBox listBoxConnMain;
        private System.Windows.Forms.ListBox listBoxMYSQL;
        private System.Windows.Forms.ListBox listBoxMSSQL;
        private System.Windows.Forms.Button btnConnAll;
        private System.Windows.Forms.Label lblConnMainDone;
        private System.Windows.Forms.Label lblConnMYSQLDone;
        private System.Windows.Forms.Label lblConnMSSQLDone;
        private System.Windows.Forms.Label lblConnMainError;
        private System.Windows.Forms.Label lblConnMainTotal;
        private System.Windows.Forms.Label lblConnMYSQLError;
        private System.Windows.Forms.Label lblConnMYSQLTotal;
        private System.Windows.Forms.Label lblConnMSSQLError;
        private System.Windows.Forms.Label lblConnMSSQLTotal;
    }
}