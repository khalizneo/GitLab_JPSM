﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DISJPSM_Integration
{
    public partial class Form2 : Form
    {
        Form opener;
        string sistem_suasana = Properties.Settings.Default.sistem_suasana;

        public Form2(Form parentForm)
        {
            InitializeComponent();
            opener = parentForm;
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            if (sistem_suasana == "dev")
            {
                tabControlConnection.SelectedTab = tabPageDevelopment;
                tabPageProduction.Enabled = false;
                tabPageDevelopment.Enabled = true;

                txtDevServer.Text = Properties.Settings.Default.devmssql_sdb_server;
                txtDevDatabase.Text = Properties.Settings.Default.devmssql_sdb_database;
                txtDevUserName.Text = Properties.Settings.Default.devmssql_sdb_username;
                txtDevUserPassword.Text = Properties.Settings.Default.devmssql_sdb_userpassword;
                txtDevServerFull.Text = "Data Source="+ Properties.Settings.Default.devmssql_sdb_server + ";Initial Catalog="+ Properties.Settings.Default.devmssql_sdb_database + ";User ID="+ Properties.Settings.Default.devmssql_sdb_username + ";Password="+ Properties.Settings.Default.devmssql_sdb_userpassword + ";Persist Security Info=True";
            }
            else if (sistem_suasana == "prod")
            {
                tabControlConnection.SelectedTab = tabPageProduction;
                tabPageProduction.Enabled = true;
                tabPageDevelopment.Enabled = false;

                txtProdServer.Text = Properties.Settings.Default.prodmssql_sdb_server;
                txtProdDatabase.Text = Properties.Settings.Default.prodmssql_sdb_database;
                txtProdUserName.Text = Properties.Settings.Default.prodmssql_sdb_username;
                txtProdUserPassword.Text = Properties.Settings.Default.prodmssql_sdb_userpassword;
                txtProdServerFull.Text = "Data Source=" + Properties.Settings.Default.prodmssql_sdb_server + ";Initial Catalog=" + Properties.Settings.Default.prodmssql_sdb_database + ";User ID=" + Properties.Settings.Default.prodmssql_sdb_username + ";Password=" + Properties.Settings.Default.prodmssql_sdb_userpassword + ";Persist Security Info=True";
            }
            else
            {
                tabControlConnection.SelectedTab = tabPageDevelopment;
                tabPageProduction.Enabled = false;
                tabPageDevelopment.Enabled = true;

                txtDevServer.Text = Properties.Settings.Default.devmssql_sdb_server;
                txtDevDatabase.Text = Properties.Settings.Default.devmssql_sdb_database;
                txtDevUserName.Text = Properties.Settings.Default.devmssql_sdb_username;
                txtDevUserPassword.Text = Properties.Settings.Default.devmssql_sdb_userpassword;
                txtDevServerFull.Text = "Data Source=" + Properties.Settings.Default.devmssql_sdb_server + ";Initial Catalog=" + Properties.Settings.Default.devmssql_sdb_database + ";User ID=" + Properties.Settings.Default.devmssql_sdb_username + ";Password=" + Properties.Settings.Default.devmssql_sdb_userpassword + ";Persist Security Info=True";
            }
        }

        private void btnConnBack_Click(object sender, EventArgs e)
        {
            //opener.Close();
            this.Close();
        }

        private void btnConnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Properties.Settings.Default.dev_sdbconnstring = "Data Source=DESKTOPSERVER;Initial Catalog=disjpsm_sdb;User ID=sa;Password=@dm!n123;Persist Security Info=True";

                if (sistem_suasana == "dev")
                {
                    Properties.Settings.Default.devmssql_sdb_server = txtDevServer.Text;
                    Properties.Settings.Default.devmssql_sdb_database = txtDevDatabase.Text;
                    Properties.Settings.Default.devmssql_sdb_username = txtDevUserName.Text;
                    Properties.Settings.Default.devmssql_sdb_userpassword = txtDevUserPassword.Text;
                }
                else if (sistem_suasana == "prod")
                {
                    Properties.Settings.Default.prodmssql_sdb_server = txtProdServer.Text;
                    Properties.Settings.Default.prodmssql_sdb_database = txtProdDatabase.Text;
                    Properties.Settings.Default.prodmssql_sdb_username = txtProdUserName.Text;
                    Properties.Settings.Default.prodmssql_sdb_userpassword = txtProdUserPassword.Text;
                }
                else
                {
                    Properties.Settings.Default.devmssql_sdb_server = txtDevServer.Text;
                    Properties.Settings.Default.devmssql_sdb_database = txtDevDatabase.Text;
                    Properties.Settings.Default.devmssql_sdb_username = txtDevUserName.Text;
                    Properties.Settings.Default.devmssql_sdb_userpassword = txtDevUserPassword.Text;
                }

                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error-" + ex);
            }
            finally
            {
                MessageBox.Show("done!");
            }
        }
    }
}
