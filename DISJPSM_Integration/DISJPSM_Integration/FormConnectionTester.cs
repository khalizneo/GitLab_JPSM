﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DISJPSM_Integration
{
    public partial class FormConnectionTester : Form
    {
        public string sistem_suasana = Properties.Settings.Default.sistem_suasana;

        public string sistem_servername;
        public string sistem_database;
        public string sistem_username;
        public string sistem_password;
        public string sistem_suasana2 = "development";

        public int xa1 = 1;
        public int xa2 = 1;
        public int xa3 = 1;

        public int xb1 = 1;
        public int xb2 = 1;
        public int xb3 = 1;

        public int xc1 = 1;
        public int xc2 = 1;
        public int xc3 = 1;

        public SqlConnection conn = new SqlConnection();        
        public SqlConnectionStringBuilder build = new SqlConnectionStringBuilder();

        public MySqlConnection connMYSQL = new MySqlConnection();
        public MySqlConnectionStringBuilder buildMYSQL = new MySqlConnectionStringBuilder();

        public SqlConnection connMSSQL = new SqlConnection();
        public SqlConnectionStringBuilder buildMSSQL = new SqlConnectionStringBuilder();

        public FormConnectionTester()
        {
            InitializeComponent();
        }
        private async Task sdbdatabaseconnection()
        {
            if (sistem_suasana == "prod")
            {
                sistem_servername = Properties.Settings.Default.prodmssql_sdb_database;
                sistem_database = Properties.Settings.Default.prodmssql_sdb_server;
                sistem_username = Properties.Settings.Default.prodmssql_sdb_username;
                sistem_password = Properties.Settings.Default.prodmssql_sdb_userpassword;
                sistem_suasana2 = "production";
            }
            else if (sistem_suasana == "dev")
            {
                sistem_servername = Properties.Settings.Default.devmssql_sdb_server;
                sistem_database = Properties.Settings.Default.devmssql_sdb_database;
                sistem_username = Properties.Settings.Default.devmssql_sdb_username;
                sistem_password = Properties.Settings.Default.devmssql_sdb_userpassword;
                sistem_suasana2 = "development";
            }
            else if (sistem_suasana == "dev2")
            {
                sistem_servername = Properties.Settings.Default.dev2mssql_sdb_server;
                sistem_database = Properties.Settings.Default.dev2mssql_sdb_database;
                sistem_username = Properties.Settings.Default.dev2mssql_sdb_username;
                sistem_password = Properties.Settings.Default.dev2mssql_sdb_userpassword;
                sistem_suasana2 = "development2";
            }

            build.DataSource = sistem_servername;
            build.InitialCatalog = sistem_database;
            build.UserID = sistem_username;
            build.Password = sistem_password;
            build.PersistSecurityInfo = true;
            
            await Task.Delay(0);
        }
        private async Task connMain() {
            try
            {
                
                lblConnMainTotal.Text = xa1.ToString();
                xa1++;

                conn = new SqlConnection(build.ToString());
                conn.Open();

                listBoxConnMain.Items.Add("Connection "+ sistem_servername + "-"+ sistem_database + ": Done");

                lblConnMainDone.Text = xa2.ToString();
                xa2++;
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    listBoxConnMain.Items.Add("Sql Exception " + sistem_servername + "-" + sistem_database + ": " + ex.Message.ToString());
                }
                else
                {
                    listBoxConnMain.Items.Add("Other Exception " + sistem_servername + "-" + sistem_database + ": " + ex.Message.ToString());
                }

                lblConnMainError.Text = xa3.ToString();
                xa3++;
            }
            finally
            {
                conn.Close();
            }

            await Task.Delay(1);
        }

        private async Task connCheckMYSQL()
        {
            try
            {
                
                conn = new SqlConnection(build.ToString());
                string queryCheckSDB = "SELECT * FROM [dis_database_connection];";
                SqlCommand cmdCheckSDB = new SqlCommand(queryCheckSDB, conn);

                conn.Open();
                SqlDataReader readerCheckSDB = cmdCheckSDB.ExecuteReader();
                if (readerCheckSDB.HasRows)
                {
                    while (readerCheckSDB.Read())
                    {
                        if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 & readerCheckSDB["dbconn_servertype"].ToString() == "mysql")
                        {
                            try
                            {
                                lblConnMYSQLTotal.Text = xb1.ToString();
                                xb1++;

                                buildMYSQL.Server = readerCheckSDB["dbconn_server"].ToString();
                                buildMYSQL.Database = readerCheckSDB["dbconn_database"].ToString();
                                buildMYSQL.UserID = readerCheckSDB["dbconn_username"].ToString();
                                buildMYSQL.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                                buildMYSQL.PersistSecurityInfo = true;
                                buildMYSQL.ConvertZeroDateTime = true;
                                connMYSQL = new MySqlConnection(buildMYSQL.ToString());
                                connMYSQL.Open();

                                listBoxMYSQL.Items.Add("Connection " + readerCheckSDB["dbconn_server"].ToString() + "-" + readerCheckSDB["dbconn_database"].ToString() + ": Done");

                                lblConnMYSQLDone.Text = xb2.ToString();
                                xb2++;
                            }
                            catch (Exception ex)
                            {
                                lblConnMYSQLError.Text = xb3.ToString();
                                xb3++;

                                if (ex is MySqlException)
                                {
                                    listBoxMYSQL.Items.Add("MySql Exception " + readerCheckSDB["dbconn_server"].ToString() + "-" + readerCheckSDB["dbconn_database"].ToString() + ": " + ex.Message.ToString());
                                }
                                else
                                {
                                    listBoxMYSQL.Items.Add("Other Exception " + readerCheckSDB["dbconn_server"].ToString() + "-" + readerCheckSDB["dbconn_database"].ToString() + ": " + ex.Message.ToString());
                                }
                            }
                            finally
                            {
                                connMYSQL.Close();
                            }
                            await Task.Delay(1);
                        }
                    }
                }
                else
                {
                    listBoxMYSQL.Items.Add("Connection: empty row");
                }
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    listBoxMYSQL.Items.Add("Sql Exception " + sistem_servername + "-" + sistem_database + ": " + ex.Message.ToString());
                }
                else
                {
                    listBoxMYSQL.Items.Add("Other Exception " + sistem_servername + "-" + sistem_database + ": " + ex.Message.ToString());
                }
            }
            finally
            {
                conn.Close();
            }
            
            await Task.Delay(0);
        }
        private async Task connCheckMSSQL()
        {
            try
            {
                conn = new SqlConnection(build.ToString());
                string queryCheckSDB = "SELECT * FROM [dis_database_connection];";
                SqlCommand cmdCheckSDB = new SqlCommand(queryCheckSDB, conn);

                conn.Open();
                SqlDataReader readerCheckSDB = cmdCheckSDB.ExecuteReader();
                if (readerCheckSDB.HasRows)
                {
                    while (readerCheckSDB.Read())
                    {
                        if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 & readerCheckSDB["dbconn_servertype"].ToString() == "mssql")
                        {
                            try
                            {

                                lblConnMSSQLTotal.Text = xc1.ToString();
                                xc1++;

                                buildMSSQL.DataSource = readerCheckSDB["dbconn_server"].ToString();
                                buildMSSQL.InitialCatalog = readerCheckSDB["dbconn_database"].ToString();
                                buildMSSQL.UserID = readerCheckSDB["dbconn_username"].ToString();
                                buildMSSQL.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                                buildMSSQL.PersistSecurityInfo = true;

                                connMSSQL = new SqlConnection(buildMSSQL.ToString());
                                connMSSQL.Open();

                                listBoxMSSQL.Items.Add("Connection " + readerCheckSDB["dbconn_server"].ToString() + "-" + readerCheckSDB["dbconn_database"].ToString() + ": Done");

                                lblConnMSSQLDone.Text = xc2.ToString();
                                xc2++;
                            }
                            catch (Exception ex)
                            {
                                lblConnMSSQLError.Text = xc3.ToString();
                                xc3++;

                                if (ex is SqlException)
                                {
                                    listBoxMSSQL.Items.Add("MSSql Exception " + readerCheckSDB["dbconn_server"].ToString() + "-" + readerCheckSDB["dbconn_database"].ToString() + ": " + ex.Message.ToString());
                                }
                                else
                                {
                                    listBoxMSSQL.Items.Add("Other Exception " + readerCheckSDB["dbconn_server"].ToString() + "-" + readerCheckSDB["dbconn_database"].ToString() + ": " + ex.Message.ToString());
                                }
                            }
                            finally
                            {
                                connMSSQL.Close();
                            }
                            await Task.Delay(1);
                        }
                    }
                }
                else
                {
                    listBoxMSSQL.Items.Add("Connection: empty row");
                }
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    listBoxMSSQL.Items.Add("Sql Exception " + sistem_servername + "-" + sistem_database + ": " + ex.Message.ToString());
                }
                else
                {
                    listBoxMSSQL.Items.Add("Other Exception " + sistem_servername + "-" + sistem_database + ": " + ex.Message.ToString());
                }
            }
            finally
            {
                conn.Close();
            }

            await Task.Delay(0);
        }
        private async void btnConnMain_Click(object sender, EventArgs e)
        {
            await sdbdatabaseconnection();
            listBoxConnMain.Items.Clear();
            await connMain();            
            await Task.Delay(0);
        }

        private async void btnMYSQL_Click(object sender, EventArgs e)
        {
            await sdbdatabaseconnection();
            listBoxMYSQL.Items.Clear();
            await connCheckMYSQL();
            await Task.Delay(0);
        }

        private async void btnMSSQL_Click(object sender, EventArgs e)
        {
            await sdbdatabaseconnection();
            listBoxMSSQL.Items.Clear();
            await connCheckMSSQL();
            await Task.Delay(0);
        }

        private async void btnConnAll_Click(object sender, EventArgs e)
        {
            await sdbdatabaseconnection();

            listBoxConnMain.Items.Clear();
            await connMain();
            await Task.Delay(0);

            //await sdbdatabaseconnection();
            listBoxMYSQL.Items.Clear();
            await connCheckMYSQL();
            await Task.Delay(0);

            //await sdbdatabaseconnection();
            listBoxMSSQL.Items.Clear();
            await connCheckMSSQL();
            await Task.Delay(0);
        }
    }
}
