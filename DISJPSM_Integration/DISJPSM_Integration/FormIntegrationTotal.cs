﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DISJPSM_Integration
{
    public partial class FormIntegrationTotal : Form
    {
        public string sistem_suasana;
        public string sistem_servername;
        public string sistem_database;
        public string sistem_username;
        public string sistem_password;
        public string sistem_suasana2;

        public string nilaiserver;
        public string nilaidatabase;
        public string nilaiusername;
        public string nilaipassword;

        public string nilaidatabase01;
        public string nilaiscema01;
        public string nilaitable01;
        public string nilaimain01;
        public string nilaimaintype01;
        public string nilaisec01;
        public string nilaisectype01;

        public string nilaidatabase02;
        public string nilaiscema02;
        public string nilaitable02;
        public string nilaimain02;
        public string nilaimaintype02;
        public string nilaisec02;
        public string nilaisectype02;

        public int totalvalue_sml01;
        public int totalvalue_sml02;

        public int totalvalue_esmash01;
        public int totalvalue_esmash02;

        public int totalvalue_pgdp01;
        public int totalvalue_pgdp02;

        public SqlConnectionStringBuilder build = new SqlConnectionStringBuilder();
        public MySqlConnectionStringBuilder dis_mysql_smldb = new MySqlConnectionStringBuilder();
        public SqlConnectionStringBuilder dis_mssql_smldb = new SqlConnectionStringBuilder();
        public MySqlConnectionStringBuilder dis_mysql_esmashdb = new MySqlConnectionStringBuilder();
        public SqlConnectionStringBuilder dis_mssql_esmashdb = new SqlConnectionStringBuilder();

        public SqlConnection conn = new SqlConnection();
        public MySqlConnection connMYSQLDB_SMLSDB01 = new MySqlConnection();
        public MySqlConnection connMYSQLDB_eSmashSDB01 = new MySqlConnection();

        public SqlConnection conndis_sdb2 = new SqlConnection();

        public SqlConnectionStringBuilder dis_pgdp01 = new SqlConnectionStringBuilder();
        public SqlConnection disconn_pgdp01 = new SqlConnection();

        SqlConnectionStringBuilder dis_updaterselect = new SqlConnectionStringBuilder();
        public SqlConnection conndis_updaterselect = new SqlConnection();
        


        public FormIntegrationTotal()
        {
            InitializeComponent();
        }
        private async Task initialconnection()
        {
            sistem_suasana = Properties.Settings.Default.sistem_suasana;

            if (sistem_suasana == "prod")
            {
                sistem_servername = Properties.Settings.Default.prodmssql_sdb_server;
                sistem_database = Properties.Settings.Default.prodmssql_sdb_database;
                sistem_username = Properties.Settings.Default.prodmssql_sdb_username;
                sistem_password = Properties.Settings.Default.prodmssql_sdb_userpassword;
                sistem_suasana2 = "production";
            }
            else if (sistem_suasana == "dev")
            {
                sistem_servername = Properties.Settings.Default.devmssql_sdb_server;
                sistem_database = Properties.Settings.Default.devmssql_sdb_database;
                sistem_username = Properties.Settings.Default.devmssql_sdb_username;
                sistem_password = Properties.Settings.Default.devmssql_sdb_userpassword;
                sistem_suasana2 = "development";
            }
            else if (sistem_suasana == "dev2")
            {
                sistem_servername = Properties.Settings.Default.dev2mssql_sdb_server;
                sistem_database = Properties.Settings.Default.dev2mssql_sdb_database;
                sistem_username = Properties.Settings.Default.dev2mssql_sdb_username;
                sistem_password = Properties.Settings.Default.dev2mssql_sdb_userpassword;
                sistem_suasana2 = "development2";
            }

            build.DataSource = sistem_servername;
            build.InitialCatalog = sistem_database;
            build.UserID = sistem_username;
            build.Password = sistem_password;
            build.PersistSecurityInfo = true;

            conn = new SqlConnection(build.ToString());
            string queryCheckSDB = "SELECT * FROM [dis_database_connection];";
            SqlCommand cmdCheckSDB = new SqlCommand(queryCheckSDB, conn);

            conn.Open();
            SqlDataReader readerCheckSDB = cmdCheckSDB.ExecuteReader();
            if (readerCheckSDB.HasRows)
            {
                while (readerCheckSDB.Read())
                {
                    if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mysql" && readerCheckSDB["dbconn_servername"].ToString() == "sml")
                    {
                        dis_mysql_smldb.Server = readerCheckSDB["dbconn_server"].ToString();
                        dis_mysql_smldb.Database = readerCheckSDB["dbconn_database"].ToString();
                        dis_mysql_smldb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mysql_smldb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mysql_smldb.PersistSecurityInfo = true;
                        dis_mysql_smldb.ConvertZeroDateTime = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mssql" && readerCheckSDB["dbconn_servername"].ToString() == "sml")
                    {
                        dis_mssql_smldb.DataSource = readerCheckSDB["dbconn_server"].ToString();
                        dis_mssql_smldb.InitialCatalog = readerCheckSDB["dbconn_database"].ToString();
                        dis_mssql_smldb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mssql_smldb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mssql_smldb.PersistSecurityInfo = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mysql" && readerCheckSDB["dbconn_servername"].ToString() == "esmash")
                    {
                        dis_mysql_esmashdb.Server = readerCheckSDB["dbconn_server"].ToString();
                        dis_mysql_esmashdb.Database = readerCheckSDB["dbconn_database"].ToString();
                        dis_mysql_esmashdb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mysql_esmashdb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mysql_esmashdb.PersistSecurityInfo = true;
                        dis_mysql_esmashdb.ConvertZeroDateTime = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mssql" && readerCheckSDB["dbconn_servername"].ToString() == "esmash")
                    {
                        dis_mssql_esmashdb.DataSource = readerCheckSDB["dbconn_server"].ToString();
                        dis_mssql_esmashdb.InitialCatalog = readerCheckSDB["dbconn_database"].ToString();
                        dis_mssql_esmashdb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mssql_esmashdb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mssql_esmashdb.PersistSecurityInfo = true;
                    }
                }
            }
            else
            {
            }
            await Task.Delay(1);
        }
        public async Task checktotalupdater_sml()
        {
            int waitx = 0;
            totalvalue_esmash01 = 0;

            string[] fibarray = new string[]
            {
                "maklumat_lesen2"
            };

            for (int i = 0; i < fibarray.Length; i++)
            {
                string abc = fibarray[i];

                await initialconnection();
                try
                {
                    connMYSQLDB_SMLSDB01 = new MySqlConnection(dis_mysql_smldb.ToString());
                    string cmdText = "SELECT COUNT(*) AS totalvalue FROM " + abc + ";";
                    MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_SMLSDB01);
                    connMYSQLDB_SMLSDB01.Open();
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            totalvalue_sml02 = totalvalue_sml01 + Convert.ToInt32(reader["totalvalue"]);
                            totalvalue_sml01 = totalvalue_sml02;                            
                        }
                    }
                }
                catch
                {
                    //error
                }
                finally
                {
                    connMYSQLDB_SMLSDB01.Close();
                }
                label1.Text = totalvalue_sml01.ToString();
                await Task.Delay(waitx);
            }
        }
        public async Task checktotalupdater_esmash()
        {
            int waitx = 0;
            totalvalue_esmash01 = 0;

            List<string> list_table = new List<string>();

            list_table.Add("cadangan_fungsi_hutan");
            list_table.Add("cadangan_hutan_simpanan_kekal");
            list_table.Add("cadangan_jenis_hutan");
            list_table.Add("daerah_hutan");
            list_table.Add("hsk_dan_tanah_hutan_1960_2015");
            list_table.Add("hsk_mengikut_jenis_hutan_1960_2015");
            list_table.Add("hutan_simpanan_kekal");
            list_table.Add("lap_uh_10");
            list_table.Add("lap_uh_11");
            list_table.Add("lap_uh_20");
            list_table.Add("lap_uh_21");
            list_table.Add("lap_uh_21a");
            list_table.Add("lap_uh_21b");
            list_table.Add("lap_uh_22");
            list_table.Add("lap_uh_23");
            list_table.Add("lap_uh_30");
            list_table.Add("lap_uh_40");
            list_table.Add("lap_uh_50");
            list_table.Add("ops_fungsi_hutan");
            list_table.Add("pewartaan_fungsi_hutan");
            list_table.Add("pewartaan_jenis_hutan");

            //string abcdedjhasjkd = list_table.ToString();
            //MessageBox.Show(abcdedjhasjkd);

            for (int i = 0; i < list_table.Count; i++)
            {
                string abc = list_table[i];

                await initialconnection();
                try
                {
                    connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                    string cmdText = "SELECT COUNT(*) AS totalvalue FROM " + abc + ";";
                    MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                    connMYSQLDB_eSmashSDB01.Open();
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            totalvalue_esmash02 = totalvalue_esmash01 + Convert.ToInt32(reader["totalvalue"]);
                            totalvalue_esmash01 = totalvalue_esmash02;                            
                        }
                    }
                }
                catch
                {
                    //error
                }
                finally
                {
                    connMYSQLDB_eSmashSDB01.Close();
                }
                label2.Text = totalvalue_esmash01.ToString();
                await Task.Delay(waitx);
            }
        }
        public async Task checktotalupdater_pgdb() {

            int waitx = 0;
            totalvalue_pgdp01 = 0;

            await initialconnection();
            try
            {
                //get database
                conndis_sdb2 = new SqlConnection(build.ToString());
                string cmdTextdis_sdb2 = "SELECT [dbconn_server],[dbconn_database],[dbconn_username],[dbconn_userpassword] FROM [dis_database_connection] WHERE [dbconn_servername] = @dbconn_servername AND [dbconn_serverenvironment] = @dbconn_serverenvironment GROUP BY [dbconn_server],[dbconn_database],[dbconn_username],[dbconn_userpassword];";
                SqlCommand cmddis_sdb2 = new SqlCommand(cmdTextdis_sdb2, conndis_sdb2);
                cmddis_sdb2.Parameters.AddWithValue("@dbconn_servername", "pdgp");
                cmddis_sdb2.Parameters.AddWithValue("@dbconn_serverenvironment", sistem_suasana2);
                conndis_sdb2.Open();
                SqlDataReader reader_sdb2 = cmddis_sdb2.ExecuteReader();
                if (reader_sdb2.HasRows)
                {
                    while (reader_sdb2.Read())
                    {
                        nilaiserver = reader_sdb2["dbconn_server"].ToString();
                        nilaidatabase = reader_sdb2["dbconn_database"].ToString();
                        nilaiusername = reader_sdb2["dbconn_username"].ToString();
                        nilaipassword = reader_sdb2["dbconn_userpassword"].ToString();

                        try
                        {
                            disconn_pgdp01 = new SqlConnection(build.ToString());                            
                            string cmdTextdis_sdb = "SELECT * FROM [dbo].[dis_system_updater_all] WHERE [updaterall_database_02] = @updaterall_database_02;";
                            SqlCommand cmddis_sdb = new SqlCommand(cmdTextdis_sdb, disconn_pgdp01);
                            cmddis_sdb.Parameters.AddWithValue("@updaterall_database_02", nilaidatabase);
                            disconn_pgdp01.Open();
                            SqlDataReader reader_sdb = cmddis_sdb.ExecuteReader();
                            if (reader_sdb.HasRows)
                            {
                                while (reader_sdb.Read())
                                {
                                    nilaidatabase01 = reader_sdb["updaterall_database_01"].ToString();
                                    nilaiscema01 = reader_sdb["updaterall_scema_01"].ToString();
                                    nilaitable01 = reader_sdb["updaterall_table_01"].ToString();
                                    nilaimain01 = reader_sdb["updaterall_fieldkey_01"].ToString();
                                    nilaimaintype01 = reader_sdb["updaterall_fieldkeytype_01"].ToString();
                                    nilaisec01 = reader_sdb["updaterall_fieldname_01"].ToString();
                                    nilaisectype01 = reader_sdb["updaterall_fieldtype_01"].ToString();

                                    nilaidatabase02 = reader_sdb["updaterall_database_02"].ToString();
                                    nilaiscema02 = reader_sdb["updaterall_scema_02"].ToString();
                                    nilaitable02 = reader_sdb["updaterall_table_02"].ToString();
                                    nilaimain02 = reader_sdb["updaterall_fieldkey_02"].ToString();
                                    nilaimaintype02 = reader_sdb["updaterall_fieldkeytype_02"].ToString();
                                    nilaisec02 = reader_sdb["updaterall_fieldname_02"].ToString();
                                    nilaisectype02 = reader_sdb["updaterall_fieldtype_02"].ToString();


                                    try {

                                        dis_updaterselect.DataSource = sistem_servername;
                                        dis_updaterselect.InitialCatalog = nilaidatabase01;
                                        dis_updaterselect.UserID = sistem_username;
                                        dis_updaterselect.Password = sistem_password;
                                        dis_updaterselect.PersistSecurityInfo = true;

                                        conndis_updaterselect = new SqlConnection(dis_updaterselect.ToString());
                                        //string cmdTextdis_updaterselect = "SELECT [" + nilaimain01 + "],[" + nilaisec01 + "] FROM [" + nilaiscema01 + "].[" + nilaitable01 + "];";
                                        string cmdTextdis_updaterselect = "SELECT COUNT(*) AS totalvalue FROM [" + nilaiscema01 + "].[" + nilaitable01 + "];";
                                        SqlCommand cmddis_updaterselect = new SqlCommand(cmdTextdis_updaterselect, conndis_updaterselect);
                                        conndis_updaterselect.Open();
                                        SqlDataReader reader_updaterselect = cmddis_updaterselect.ExecuteReader();
                                        if (reader_updaterselect.HasRows)
                                        {
                                            while (reader_updaterselect.Read())
                                            {
                                                totalvalue_pgdp02 = totalvalue_pgdp01 + Convert.ToInt32(reader_updaterselect["totalvalue"]);
                                                totalvalue_pgdp01 = totalvalue_pgdp02;                                                
                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.ToString());
                                    }
                                    finally
                                    {
                                        conndis_updaterselect.Close();
                                    }
                                }
                            }
                            else
                            {
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        finally
                        {
                            disconn_pgdp01.Close();
                        }
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                conndis_sdb2.Close();
            }

            label3.Text = totalvalue_pgdp01.ToString();
            await Task.Delay(waitx);
        }
        private async void button1_Click(object sender, EventArgs e)
        {
            await checktotalupdater_sml();
            await checktotalupdater_esmash();
            await checktotalupdater_pgdb();

            var varDIS_SystemUpdater2 = new DIS_SystemUpdater2();

            await varDIS_SystemUpdater2.updater_pgdp();
        }
    }
}
