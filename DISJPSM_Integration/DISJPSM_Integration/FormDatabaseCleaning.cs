﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DISJPSM_Integration
{
    public partial class FormDatabaseCleaning : Form
    {
        public SqlConnection conn = new SqlConnection();
        public SqlConnectionStringBuilder build = new SqlConnectionStringBuilder();

        string sistem_suasana = Properties.Settings.Default.sistem_suasana;
        string sistem_servername;
        string sistem_database;
        string sistem_username;
        string sistem_password;
        string sistem_suasana2;

        int totalcount_statusintegration;
        int totalcount_statusupdater;

        public FormDatabaseCleaning()
        {
            InitializeComponent();
        }
        private async Task sdbdatabaseconnection()
        {
            if (sistem_suasana == "prod")
            {
                sistem_servername = Properties.Settings.Default.prodmssql_sdb_database;
                sistem_database = Properties.Settings.Default.prodmssql_sdb_server;
                sistem_username = Properties.Settings.Default.prodmssql_sdb_username;
                sistem_password = Properties.Settings.Default.prodmssql_sdb_userpassword;
                sistem_suasana2 = "production";
            }
            else if (sistem_suasana == "dev")
            {
                sistem_servername = Properties.Settings.Default.devmssql_sdb_server;
                sistem_database = Properties.Settings.Default.devmssql_sdb_database;
                sistem_username = Properties.Settings.Default.devmssql_sdb_username;
                sistem_password = Properties.Settings.Default.devmssql_sdb_userpassword;
                sistem_suasana2 = "development";
            }
            else if (sistem_suasana == "dev2")
            {
                sistem_servername = Properties.Settings.Default.dev2mssql_sdb_server;
                sistem_database = Properties.Settings.Default.dev2mssql_sdb_database;
                sistem_username = Properties.Settings.Default.dev2mssql_sdb_username;
                sistem_password = Properties.Settings.Default.dev2mssql_sdb_userpassword;
                sistem_suasana2 = "development2";
            }

            build.DataSource = sistem_servername;
            build.InitialCatalog = sistem_database;
            build.UserID = sistem_username;
            build.Password = sistem_password;
            build.PersistSecurityInfo = true;

            await Task.Delay(0);
        }
        private async Task totalvalue()
        {
            try
            {
                await sdbdatabaseconnection();
                conn = new SqlConnection(build.ToString());
                string queryconn = "SELECT COUNT( [statusintegration_id]) AS totalcount FROM [dbo].[dis_status_integration];";
                SqlCommand commandconn = new SqlCommand(queryconn, conn);
                conn.Open();
                SqlDataReader readerconn = commandconn.ExecuteReader();
                if (readerconn.HasRows)
                {
                    while (readerconn.Read())
                    {
                        totalcount_statusintegration = Convert.ToInt32(readerconn["totalcount"]);
                        lblTotalcountStatusintegration2.Text = ":  " + totalcount_statusintegration.ToString();
                    }
                }
                else
                {
                    totalcount_statusintegration = 0;
                }
            }
            catch (Exception ex)
            {
                string errorstatusintegration_a = ex.ToString();
                string errorstatusintegration_b = ex.Message.ToString();
            }
            finally
            {
                conn.Close();
            }

            await Task.Delay(1);

            try
            {
                await sdbdatabaseconnection();
                conn = new SqlConnection(build.ToString());
                string queryconn = "SELECT COUNT( [statusupdater_id]) AS totalcount FROM [dbo].[dis_status_updater];";
                SqlCommand commandconn = new SqlCommand(queryconn, conn);
                conn.Open();
                SqlDataReader readerconn = commandconn.ExecuteReader();
                if (readerconn.HasRows)
                {
                    while (readerconn.Read())
                    {
                        totalcount_statusupdater = Convert.ToInt32(readerconn["totalcount"]);
                        lblTotalcountStatusupdater2.Text = ":  " + totalcount_statusupdater.ToString();
                    }
                }
                else
                {
                    totalcount_statusupdater = 0;
                }
            }
            catch (Exception ex)
            {
                string errorstatusupdater_a = ex.ToString();
                string errorstatusupdater_b = ex.Message.ToString();
            }
            finally
            {
                conn.Close();
            }

            await Task.Delay(1);
        }
        private async void FormDatabaseCleaning_Load(object sender, EventArgs e)
        {
            await totalvalue();
        }

        private async void btnEmptyStatusintegration_Click(object sender, EventArgs e)
        {
            try
            {
                await sdbdatabaseconnection();
                conn = new SqlConnection(build.ToString());
                string queryconn = "TRUNCATE TABLE [dbo].[dis_status_integration];";
                SqlCommand commandconn = new SqlCommand(queryconn, conn);
                conn.Open();
                int rowsAffected = commandconn.ExecuteNonQuery();
                if (rowsAffected == 0)
                {
                    MessageBox.Show("Status dis_status_integration : Non");
                }
                else
                {
                    MessageBox.Show("Status dis_status_integration : Done");
                }
            }
            catch
            {
            }
            finally
            {
                conn.Close();
                await totalvalue();
            }
        }

        private async void btnEmptyStatusupdater_Click(object sender, EventArgs e)
        {
            try
            {
                await sdbdatabaseconnection();
                conn = new SqlConnection(build.ToString());
                string queryconn = "TRUNCATE TABLE [dbo].[dis_status_updater];";
                SqlCommand commandconn = new SqlCommand(queryconn, conn);
                conn.Open();
                int rowsAffected = commandconn.ExecuteNonQuery();
                if (rowsAffected == 0)
                {
                    MessageBox.Show("Status dis_status_updater : Non");
                }
                else
                {
                    MessageBox.Show("Status dis_status_updater : Done");
                }
            }
            catch
            {
            }
            finally
            {
                conn.Close();
                await totalvalue();
            }
        }
    }
}
