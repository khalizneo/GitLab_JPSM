﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DISJPSM_Integration
{
    public partial class Form1 : Form
    {
        public string sistem_suasana_new = Properties.Settings.Default.sistem_suasana;
        public string error_sdb;

        public Form1()
        {
            InitializeComponent();
        }
        public void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                DISjpsmNotifyIcon.Visible = true;
            }
        }

        public void DISjpsmNotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            DISjpsmNotifyIcon.Visible = false;
        }

        public void btnMinimized_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public void btnSetting_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2(this);
            frm.Show();
        }
        public async Task countdown(string nilai01, string nilai02)
        {
            if (nilai01 == "mula") {
                DISjpsmNotifyIcon.Text = "Starting!";
                DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                DISjpsmNotifyIcon.BalloonTipText = "Starting!";
                DISjpsmNotifyIcon.ShowBalloonTip(1000);
            }
            else if (nilai01 == "kira")
            {
                for (int i = 3; i >= 1; i--)
                {
                    await Task.Delay(3000);

                    lblCounter.Text = i.ToString();
                    DISjpsmNotifyIcon.Text = i.ToString();
                    DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                    DISjpsmNotifyIcon.BalloonTipText = i.ToString();
                    DISjpsmNotifyIcon.ShowBalloonTip(1000);
                }
            }
            else if(nilai01 == "semasa")
            {
                DISjpsmNotifyIcon.Text = "Running!";
                DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                DISjpsmNotifyIcon.BalloonTipText = "Running!";
                DISjpsmNotifyIcon.ShowBalloonTip(1000);
            }
            else if(nilai01 == "sml")
            {
                DISjpsmNotifyIcon.Text = "SML Done!";
                DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                DISjpsmNotifyIcon.BalloonTipText = "SML Done!";
                DISjpsmNotifyIcon.ShowBalloonTip(1000);
            }
            else if(nilai01 == "esmash")
            {
                DISjpsmNotifyIcon.Text = "eSmash Done!";
                DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                DISjpsmNotifyIcon.BalloonTipText = "eSmash Done!";
                DISjpsmNotifyIcon.ShowBalloonTip(1000);
            }
            else if(nilai01 == "pgdp")
            {
                DISjpsmNotifyIcon.Text = "pgdp Done!";
                DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                DISjpsmNotifyIcon.BalloonTipText = "pgdp Done!";
                DISjpsmNotifyIcon.ShowBalloonTip(1000);
            }
            else if (nilai01 == "tamat") {
                DISjpsmNotifyIcon.Text = "Done!";
                DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                DISjpsmNotifyIcon.BalloonTipText = "Done!";
                DISjpsmNotifyIcon.ShowBalloonTip(1000);
            }
            else if (nilai01 == "ralat") {
                DISjpsmNotifyIcon.Text = "Error!-" + nilai02;
                DISjpsmNotifyIcon.BalloonTipTitle = "Database Integration System JPSM." + Environment.NewLine + "Countdown";
                DISjpsmNotifyIcon.BalloonTipText = "Error!-" + nilai02;
                DISjpsmNotifyIcon.ShowBalloonTip(1000);
            }
        }
        public async void Form1_Load(object sender, EventArgs e)
        {
            if (sistem_suasana_new == "prod") {
                btnStart.Enabled = false;

                this.WindowState = FormWindowState.Minimized;

                try
                {
                    var varDIS_SystemIntegration = new DIS_SystemIntegration();
                    var varDIS_SystemUpdater = new DIS_SystemUpdater2();

                    await countdown("mula", null);
                    await countdown("kira", null);
                    await countdown("semasa", null);

                    await varDIS_SystemIntegration.updater_sml();
                    await countdown("sml", null);
                    await varDIS_SystemIntegration.updater_esmash();
                    await countdown("esmash", null);
                    await varDIS_SystemUpdater.updater_pgdp();
                    await countdown("pgdp", null);
                }
                catch (Exception ex)
                {
                    await countdown("ralat", ex.ToString());
                }
                finally
                {
                    await countdown("tamat", null);
                    this.Close();
                }

            }
            else if (sistem_suasana_new == "dev")
            {
                btnStart.Enabled = true;
            }
            else if (sistem_suasana_new == "dev2")
            {
                btnStart.Enabled = true;
            }
        }
        private async void btnStart_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            try
            {
                var varDIS_SystemIntegration = new DIS_SystemIntegration();
                var varDIS_SystemUpdater = new DIS_SystemUpdater2();

                await countdown("mula", null);
                await countdown("kira", null);
                await countdown("semasa", null);

                await varDIS_SystemIntegration.updater_sml();
                await countdown("sml", null);
                await varDIS_SystemIntegration.updater_esmash();
                await countdown("esmash", null);
                await varDIS_SystemUpdater.updater_pgdp();
                await countdown("pgdp", null);
            }
            catch (Exception ex)
            {
                await countdown("ralat", ex.ToString());
            }
            finally
            {
                await countdown("tamat", null);
                this.Close();
            }
        }
        private async void btnSML_Click(object sender, EventArgs e)
        {
            var varDIS_SystemIntegration = new DIS_SystemIntegration();
            await varDIS_SystemIntegration.updater_sml();
            MessageBox.Show("SML: Proses Done!");
        }

        private async void btnESMASH_Click(object sender, EventArgs e)
        {
            var varDIS_SystemIntegration = new DIS_SystemIntegration();
            await varDIS_SystemIntegration.updater_esmash();
            MessageBox.Show("ESMASH: Proses Done!");
        }

        private async void btnPGDP_Click(object sender, EventArgs e)
        {
            var varDIS_SystemUpdater = new DIS_SystemUpdater2();
            await varDIS_SystemUpdater.updater_pgdp();
            MessageBox.Show("PGDP: Proses Done!");
        }
        private void btnOther_Click(object sender, EventArgs e)
        {
            FormOther formother = new FormOther();
            formother.Show(this);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
