﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISJPSM_Integration
{
    class DIS_StatusSystem
    {
        string sistem_suasana = Properties.Settings.Default.sistem_suasana;
        string systemerror;

        SqlConnection conn_DataUpdate;
        SqlConnectionStringBuilder dis_sdb;

        public async Task DatabaseConnection_Update()
        {
            if (sistem_suasana == "prod")
            {
                dis_sdb = new SqlConnectionStringBuilder();
                dis_sdb.DataSource = Properties.Settings.Default.prodmssql_sdb_server;
                dis_sdb.InitialCatalog = Properties.Settings.Default.prodmssql_sdb_database;
                dis_sdb.UserID = Properties.Settings.Default.prodmssql_sdb_username;
                dis_sdb.Password = Properties.Settings.Default.prodmssql_sdb_userpassword;
                dis_sdb.PersistSecurityInfo = true;
            }
            else if (sistem_suasana == "dev")
            {
                dis_sdb = new SqlConnectionStringBuilder();
                dis_sdb.DataSource = Properties.Settings.Default.devmssql_sdb_server;
                dis_sdb.InitialCatalog = Properties.Settings.Default.devmssql_sdb_database;
                dis_sdb.UserID = Properties.Settings.Default.devmssql_sdb_username;
                dis_sdb.Password = Properties.Settings.Default.devmssql_sdb_userpassword;
                dis_sdb.PersistSecurityInfo = true;
            }
            else if (sistem_suasana == "dev2")
            {
                dis_sdb = new SqlConnectionStringBuilder();
                dis_sdb.DataSource = Properties.Settings.Default.dev2mssql_sdb_server;
                dis_sdb.InitialCatalog = Properties.Settings.Default.dev2mssql_sdb_database;
                dis_sdb.UserID = Properties.Settings.Default.dev2mssql_sdb_username;
                dis_sdb.Password = Properties.Settings.Default.dev2mssql_sdb_userpassword;
                dis_sdb.PersistSecurityInfo = true;
            }
            else
            {
                // nak letak error ??
            }
            await Task.Delay(1);
        }
        public async Task XXStatusSystem_Integration(string nilai001aa, string nilai002aa, string nilai003aa, string nilai004aa, string nilai005aa)
        {
            await DatabaseConnection_Update();
            try
            {
                conn_DataUpdate = new SqlConnection(dis_sdb.ToString());
                string query_DataUpdate = "INSERT INTO [dbo].[dis_status_integration] ([statusintegration_database],[statusintegration_table],[statusintegration_fieldmain],[statusintegration_status],[statusintegration_statustext],[statusintegration_datecreate]) VALUES(@statusintegration_database,@statusintegration_table,@statusintegration_fieldmain,@statusintegration_status,@statusintegration_statustext,@statusintegration_datecreate)";
                SqlCommand cmdInsert_dissdb = new SqlCommand(query_DataUpdate, conn_DataUpdate);

                if (string.IsNullOrEmpty(nilai001aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_database", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_database", nilai001aa); }
                if (string.IsNullOrEmpty(nilai002aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_table", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_table", nilai002aa); }
                if (string.IsNullOrEmpty(nilai003aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_fieldmain", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_fieldmain", nilai003aa); }
                if (string.IsNullOrEmpty(nilai004aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_status", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_status", nilai004aa); }
                if (string.IsNullOrEmpty(nilai005aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext", nilai005aa); }

                cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_datecreate", DateTime.Now);
                conn_DataUpdate.Open();
                cmdInsert_dissdb.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                systemerror = ex.ToString();
            }
            finally
            {
                conn_DataUpdate.Close();
            }
            await Task.Delay(1);
        }

        public async Task XXStatusSystem_Updater(string nilai001aa, string nilai002aa, string nilai003aa, string nilai004aa, string nilai005aa, string nilai006aa, string nilai007aa)
        {
            await DatabaseConnection_Update();
            try
            {
                conn_DataUpdate = new SqlConnection(dis_sdb.ToString());
                string query_DataUpdate = "INSERT INTO [dbo].[dis_status_updater] ([statusupdater_server],[statusupdater_database],[statusupdater_table],[statusupdater_fieldmain],[statusupdater_fieldsec],[statusupdater_status],[statusupdater_statustext],[statusupdater_datecreate]) VALUES(@statusupdater_server,@statusupdater_database,@statusupdater_table,@statusupdater_fieldmain,@statusupdater_fieldsec,@statusupdater_status,@statusupdater_statustext,@statusupdater_datecreate)";
                SqlCommand cmdInsert_dissdb = new SqlCommand(query_DataUpdate, conn_DataUpdate);

                if (string.IsNullOrEmpty(nilai001aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_server", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_server", nilai001aa); }
                if (string.IsNullOrEmpty(nilai002aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_database", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_database", nilai002aa); }
                if (string.IsNullOrEmpty(nilai003aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_table", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_table", nilai003aa); }
                if (string.IsNullOrEmpty(nilai004aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldmain", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldmain", nilai004aa); }
                if (string.IsNullOrEmpty(nilai005aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldsec", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldsec", nilai005aa); }
                if (string.IsNullOrEmpty(nilai006aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_status", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_status", nilai006aa); }
                if (string.IsNullOrEmpty(nilai007aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext", nilai007aa); }

                cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_datecreate", DateTime.Now);
                conn_DataUpdate.Open();
                cmdInsert_dissdb.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                systemerror = ex.ToString();
            }
            finally
            {
                conn_DataUpdate.Close();
            }
            await Task.Delay(1);
        }
        public async Task StatusSystem_Updater2(string nilai001aa, string nilai002aa, string nilai003aa, string nilai004aa, string nilai005aa, string nilai006aa, string nilai007aa, string nilai008aa, string nilai009aa)
        {
            await DatabaseConnection_Update();
            try
            {
                conn_DataUpdate = new SqlConnection(dis_sdb.ToString());
                string query_DataUpdate = "INSERT INTO [dbo].[dis_status_updater] ([statusupdater_server],[statusupdater_database],[statusupdater_table],[statusupdater_fieldmain],[statusupdater_fieldsec],[statusupdater_status],[statusupdater_statustext],[statusupdater_statustext_01],[statusupdater_statustext_02],[statusupdater_datecreate]) VALUES(@statusupdater_server,@statusupdater_database,@statusupdater_table,@statusupdater_fieldmain,@statusupdater_fieldsec,@statusupdater_status,@statusupdater_statustext,@statusupdater_statustext_01,@statusupdater_statustext_02,@statusupdater_datecreate)";
                SqlCommand cmdInsert_dissdb = new SqlCommand(query_DataUpdate, conn_DataUpdate);

                if (string.IsNullOrEmpty(nilai001aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_server", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_server", nilai001aa); }
                if (string.IsNullOrEmpty(nilai002aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_database", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_database", nilai002aa); }
                if (string.IsNullOrEmpty(nilai003aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_table", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_table", nilai003aa); }
                if (string.IsNullOrEmpty(nilai004aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldmain", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldmain", nilai004aa); }
                if (string.IsNullOrEmpty(nilai005aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldsec", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_fieldsec", nilai005aa); }
                if (string.IsNullOrEmpty(nilai006aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_status", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_status", nilai006aa); }
                if (string.IsNullOrEmpty(nilai007aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext", nilai007aa); }

                if (string.IsNullOrEmpty(nilai008aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext_01", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext1", nilai008aa); }
                if (string.IsNullOrEmpty(nilai009aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext_02", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_statustext2", nilai009aa); }

                cmdInsert_dissdb.Parameters.AddWithValue("@statusupdater_datecreate", DateTime.Now);
                conn_DataUpdate.Open();
                cmdInsert_dissdb.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                systemerror = ex.ToString();
            }
            finally
            {
                conn_DataUpdate.Close();
            }
            await Task.Delay(1);
        }
        public async Task StatusSystem_Integration2(string nilai001aa, string nilai002aa, string nilai003aa, string nilai004aa, string nilai005aa, string nilai006aa, string nilai007aa, string nilai008aa, string nilai009aa)
        {
            await DatabaseConnection_Update();
            try
            {
                conn_DataUpdate = new SqlConnection(dis_sdb.ToString());
                string query_DataUpdate = "INSERT INTO [dbo].[dis_status_integration] ([statusintegration_server],[statusintegration_database],[statusintegration_table],[statusintegration_fieldmain],[statusintegration_fieldsec],[statusintegration_status],[statusintegration_statustext],[statusintegration_statustext_01],[statusintegration_statustext_02],[statusintegration_datecreate]) VALUES(@statusintegration_server,@statusintegration_database,@statusintegration_table,@statusintegration_fieldmain,@statusintegration_fieldsec,@statusintegration_status,@statusintegration_statustext,@statusintegration_statustext_01,@statusintegration_statustext_02,@statusintegration_datecreate)";
                SqlCommand cmdInsert_dissdb = new SqlCommand(query_DataUpdate, conn_DataUpdate);

                if (string.IsNullOrEmpty(nilai001aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_server", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_server", nilai001aa); }
                if (string.IsNullOrEmpty(nilai002aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_database", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_database", nilai002aa); }
                if (string.IsNullOrEmpty(nilai003aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_table", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_table", nilai003aa); }
                if (string.IsNullOrEmpty(nilai004aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_fieldmain", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_fieldmain", nilai004aa); }
                if (string.IsNullOrEmpty(nilai005aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_fieldsec", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_fieldsec", nilai005aa); }

                if (string.IsNullOrEmpty(nilai006aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_status", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_status", nilai006aa); }
                if (string.IsNullOrEmpty(nilai007aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext", nilai007aa); }

                if (string.IsNullOrEmpty(nilai008aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext_01", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext_01", nilai008aa); }
                if (string.IsNullOrEmpty(nilai009aa)) { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext_02", DBNull.Value); }
                else { cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_statustext_02", nilai009aa); }

                cmdInsert_dissdb.Parameters.AddWithValue("@statusintegration_datecreate", DateTime.Now);
                conn_DataUpdate.Open();
                cmdInsert_dissdb.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                systemerror = ex.ToString();
            }
            finally
            {
                conn_DataUpdate.Close();
            }
            await Task.Delay(1);
        }

    }
}
