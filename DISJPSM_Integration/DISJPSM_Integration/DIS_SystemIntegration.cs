﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISJPSM_Integration
{
    class DIS_SystemIntegration
    {
        public MySqlConnectionStringBuilder dis_mysql_smldb = new MySqlConnectionStringBuilder();
        public MySqlConnectionStringBuilder dis_mysql_esmashdb = new MySqlConnectionStringBuilder();

        public SqlConnectionStringBuilder build = new SqlConnectionStringBuilder();
        public SqlConnectionStringBuilder dis_mssql_smldb = new SqlConnectionStringBuilder();
        public SqlConnectionStringBuilder dis_mssql_esmashdb = new SqlConnectionStringBuilder();

        public MySqlConnection connMYSQLDB_SMLSDB01 = new MySqlConnection();
        public MySqlConnection connMYSQLDB_eSmashSDB01 = new MySqlConnection();

        public SqlConnection conn = new SqlConnection();
        public SqlConnection connMSSQLDB_SMLSDB01 = new SqlConnection();
        public SqlConnection connMSSQLDB_SMLSDB02 = new SqlConnection();
        public SqlConnection connMSSQLDB_SMLSDB0X = new SqlConnection();
        public SqlConnection connMSSQLDB_eSmashSDB01 = new SqlConnection();
        public SqlConnection connMSSQLDB_eSmashSDB02 = new SqlConnection();
        public SqlConnection connMSSQLDB_eSmashDB0X = new SqlConnection();

        public string sistem_servername;
        public string sistem_database;
        public string sistem_username;
        public string sistem_password;

        public string sistem_suasana;
        public string sistem_suasana2;


        private async Task initialconnection()
        {
            sistem_suasana = Properties.Settings.Default.sistem_suasana;

            if (sistem_suasana == "prod")
            {
                sistem_servername = Properties.Settings.Default.prodmssql_sdb_server;
                sistem_database = Properties.Settings.Default.prodmssql_sdb_database;
                sistem_username = Properties.Settings.Default.prodmssql_sdb_username;
                sistem_password = Properties.Settings.Default.prodmssql_sdb_userpassword;
                sistem_suasana2 = "production";
            }
            else if (sistem_suasana == "dev")
            {
                sistem_servername = Properties.Settings.Default.devmssql_sdb_server;
                sistem_database = Properties.Settings.Default.devmssql_sdb_database;
                sistem_username = Properties.Settings.Default.devmssql_sdb_username;
                sistem_password = Properties.Settings.Default.devmssql_sdb_userpassword;
                sistem_suasana2 = "development";
            }
            else if (sistem_suasana == "dev2")
            {
                sistem_servername = Properties.Settings.Default.dev2mssql_sdb_server;
                sistem_database = Properties.Settings.Default.dev2mssql_sdb_database;
                sistem_username = Properties.Settings.Default.dev2mssql_sdb_username;
                sistem_password = Properties.Settings.Default.dev2mssql_sdb_userpassword;
                sistem_suasana2 = "development2";
            }

            build.DataSource = sistem_servername;
            build.InitialCatalog = sistem_database;
            build.UserID = sistem_username;
            build.Password = sistem_password;
            build.PersistSecurityInfo = true;

            conn = new SqlConnection(build.ToString());
            string queryCheckSDB = "SELECT * FROM [dis_database_connection];";
            SqlCommand cmdCheckSDB = new SqlCommand(queryCheckSDB, conn);

            conn.Open();
            SqlDataReader readerCheckSDB = cmdCheckSDB.ExecuteReader();
            if (readerCheckSDB.HasRows)
            {
                while (readerCheckSDB.Read())
                {
                    if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mysql" && readerCheckSDB["dbconn_servername"].ToString() == "sml")
                    {
                        dis_mysql_smldb.Server = readerCheckSDB["dbconn_server"].ToString();
                        dis_mysql_smldb.Database = readerCheckSDB["dbconn_database"].ToString();
                        dis_mysql_smldb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mysql_smldb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mysql_smldb.PersistSecurityInfo = true;
                        dis_mysql_smldb.ConvertZeroDateTime = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mssql" && readerCheckSDB["dbconn_servername"].ToString() == "sml")
                    {
                        dis_mssql_smldb.DataSource = readerCheckSDB["dbconn_server"].ToString();
                        dis_mssql_smldb.InitialCatalog = readerCheckSDB["dbconn_database"].ToString();
                        dis_mssql_smldb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mssql_smldb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mssql_smldb.PersistSecurityInfo = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mysql" && readerCheckSDB["dbconn_servername"].ToString() == "esmash")
                    {
                        dis_mysql_esmashdb.Server = readerCheckSDB["dbconn_server"].ToString();
                        dis_mysql_esmashdb.Database = readerCheckSDB["dbconn_database"].ToString();
                        dis_mysql_esmashdb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mysql_esmashdb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mysql_esmashdb.PersistSecurityInfo = true;
                        dis_mysql_esmashdb.ConvertZeroDateTime = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mssql" && readerCheckSDB["dbconn_servername"].ToString() == "esmash")
                    {
                        dis_mssql_esmashdb.DataSource = readerCheckSDB["dbconn_server"].ToString();
                        dis_mssql_esmashdb.InitialCatalog = readerCheckSDB["dbconn_database"].ToString();
                        dis_mssql_esmashdb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mssql_esmashdb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mssql_esmashdb.PersistSecurityInfo = true;
                    }
                }
            }
            else
            {
            }
            await Task.Delay(1);
        }

        public async Task updater_sml()
        {
            var syssts = new DIS_StatusSystem();

            await initialconnection();

            try
            {
                connMYSQLDB_SMLSDB01 = new MySqlConnection(dis_mysql_smldb.ToString());
                string cmdText = "SELECT * FROM `maklumat_lesen`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_SMLSDB01);
                connMYSQLDB_SMLSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["no"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoLesen"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["NamaPelesen"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["AlamatPelesen"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["Negeri"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["NamaKontraktor"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["AlamatKontraktor"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["Negeri3"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["negeri1"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["Tanah"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["daerah_hutan"].ToString(); string nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = null; } else { try { nilai011 = nilai011x.ToString(); } catch (Exception) { nilai011 = null; } }
                        string nilai012x = reader["daerah_tadbir"].ToString(); string nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = null; } else { try { nilai012 = nilai012x.ToString(); } catch (Exception) { nilai012 = null; } }
                        string nilai013x = reader["Keluasan"].ToString(); string nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = null; } else { try { nilai013 = nilai013x.ToString(); } catch (Exception) { nilai013 = null; } }
                        string nilai014x = reader["HSK"].ToString(); string nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = null; } else { try { nilai014 = nilai014x.ToString(); } catch (Exception) { nilai014 = null; } }
                        string nilai015x = reader["NoKompartment"].ToString(); string nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = null; } else { try { nilai015 = nilai015x.ToString(); } catch (Exception) { nilai015 = null; } }
                        string nilai016x = reader["Meterpadu"].ToString(); string nilai016; if (string.IsNullOrEmpty(nilai016x)) { nilai016 = null; } else { try { nilai016 = nilai016x.ToString(); } catch (Exception) { nilai016 = null; } }
                        string nilai017x = reader["Batang"].ToString(); string nilai017; if (string.IsNullOrEmpty(nilai017x)) { nilai017 = null; } else { try { nilai017 = nilai017x.ToString(); } catch (Exception) { nilai017 = null; } }
                        string nilai018x = reader["Deposit"].ToString(); decimal nilai018; if (string.IsNullOrEmpty(nilai018x)) { nilai018 = 0; } else { try { nilai018 = decimal.Parse(nilai018x); } catch (Exception) { nilai018 = 0; } }
                        string nilai019x = reader["Royalti"].ToString(); decimal nilai019; if (string.IsNullOrEmpty(nilai019x)) { nilai019 = 0; } else { try { nilai019 = decimal.Parse(nilai019x); } catch (Exception) { nilai019 = 0; } }
                        string nilai020x = reader["Ses"].ToString(); decimal nilai020; if (string.IsNullOrEmpty(nilai020x)) { nilai020 = 0; } else { try { nilai020 = decimal.Parse(nilai020x); } catch (Exception) { nilai020 = 0; } }
                        string nilai021x = reader["Cukai"].ToString(); decimal nilai021; if (string.IsNullOrEmpty(nilai021x)) { nilai021 = 0; } else { try { nilai021 = decimal.Parse(nilai021x); } catch (Exception) { nilai021 = 0; } }
                        string nilai022x = reader["Pengeluaran"].ToString(); string nilai022; if (string.IsNullOrEmpty(nilai022x)) { nilai022 = null; } else { try { nilai022 = nilai022x.ToString(); } catch (Exception) { nilai022 = null; } }
                        string nilai023x = reader["SahDari"].ToString(); string nilai023; if (string.IsNullOrEmpty(nilai023x) || nilai023x == "0/0/0000" || nilai023x == "1/01/0001 12:00:00 AM" || nilai023x == "1/1/0001 12:00:00 AM" || nilai023x == "1/01/0001 12:00:00 PG" || nilai023x == "null") { nilai023 = null; } else { try { nilai023 = DateTime.ParseExact(nilai023x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { nilai023 = null; } }
                        string nilai024x = reader["SahHingga"].ToString(); string nilai024; if (string.IsNullOrEmpty(nilai024x) || nilai024x == "0/0/0000" || nilai024x == "1/01/0001 12:00:00 AM" || nilai024x == "1/1/0001 12:00:00 AM" || nilai024x == "1/01/0001 12:00:00 PG" || nilai024x == "null") { nilai024 = null; } else { try { nilai024 = DateTime.ParseExact(nilai024x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { nilai024 = null; } }
                        string nilai025x = reader["TarikhLesen"].ToString(); string nilai025; if (string.IsNullOrEmpty(nilai025x) || nilai025x == "0/0/0000" || nilai025x == "1/01/0001 12:00:00 AM" || nilai025x == "1/1/0001 12:00:00 AM" || nilai025x == "1/01/0001 12:00:00 PG" || nilai025x == "null") { nilai025 = null; } else { try { nilai025 = DateTime.ParseExact(nilai025x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { nilai025 = null; } }
                        string nilai026x = reader["status"].ToString(); string nilai026; if (string.IsNullOrEmpty(nilai026x)) { nilai026 = null; } else { try { nilai026 = nilai026x.ToString(); } catch (Exception) { nilai026 = null; } }
                        string nilai027x = reader["TarikhTamat"].ToString(); string nilai027; if (string.IsNullOrEmpty(nilai027x) || nilai027x == "0/0/0000" || nilai027x == "1/01/0001 12:00:00 AM" || nilai027x == "1/1/0001 12:00:00 AM" || nilai027x == "1/01/0001 12:00:00 PG" || nilai027x == "null") { nilai027 = null; } else { try { nilai027 = DateTime.ParseExact(nilai027x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { nilai027 = null; } }
                        string nilai028x = reader["Meterpadu_awal"].ToString(); string nilai028; if (string.IsNullOrEmpty(nilai028x)) { nilai028 = null; } else { try { nilai028 = nilai028x.ToString(); } catch (Exception) { nilai028 = null; } }
                        string nilai029x = reader["Batang_awal"].ToString(); string nilai029; if (string.IsNullOrEmpty(nilai029x)) { nilai029 = null; } else { try { nilai029 = nilai029x.ToString(); } catch (Exception) { nilai029 = null; } }
                        string nilai030x = reader["Cukai_awal"].ToString(); decimal nilai030; if (string.IsNullOrEmpty(nilai030x)) { nilai030 = 0; } else { try { nilai030 = decimal.Parse(nilai030x); } catch (Exception) { nilai030 = 0; } }
                        string nilai031x = reader["BBCagaran"].ToString(); decimal nilai031; if (string.IsNullOrEmpty(nilai031x)) { nilai031 = 0; } else { try { nilai031 = decimal.Parse(nilai031x); } catch (Exception) { nilai031 = 0; } }
                        string nilai032x = reader["BBCukai"].ToString(); decimal nilai032; if (string.IsNullOrEmpty(nilai032x)) { nilai032 = 0; } else { try { nilai032 = decimal.Parse(nilai032x); } catch (Exception) { nilai032 = 0; } }
                        string nilai033x = reader["resit"].ToString(); string nilai033; if (string.IsNullOrEmpty(nilai033x)) { nilai033 = null; } else { try { nilai033 = nilai033x.ToString(); } catch (Exception) { nilai033 = null; } }
                        string nilai034x = reader["Tarikh_pulangan"].ToString(); string nilai034; if (string.IsNullOrEmpty(nilai034x) || nilai034x == "0/0/0000" || nilai034x == "1/01/0001 12:00:00 AM" || nilai034x == "1/1/0001 12:00:00 AM" || nilai034x == "1/01/0001 12:00:00 PG" || nilai034x == "null") { nilai034 = null; } else { try { nilai034 = DateTime.ParseExact(nilai034x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { nilai034 = null; } }

                        try
                        {
                            connMSSQLDB_SMLSDB02 = new SqlConnection(dis_mssql_smldb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[maklumat_lesen] WHERE [maklumat_lesen_no] = @maklumat_lesen_no;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_SMLSDB02);
                            cmdCheck01.Parameters.AddWithValue("@maklumat_lesen_no", nilai001.ToString());

                            connMSSQLDB_SMLSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["maklumat_lesen_no"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["maklumat_lesen_nolesen"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["maklumat_lesen_namapelesen"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["maklumat_lesen_alamatpelesen"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["maklumat_lesen_negeri"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["maklumat_lesen_namakontraktor"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["maklumat_lesen_alamatkontraktor"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["maklumat_lesen_negeri3"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["maklumat_lesen_negeri1"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["maklumat_lesen_tanah"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["maklumat_lesen_daerah_hutan"].ToString(); string checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = null; } else { try { checknilai011 = checknilai011x.ToString(); } catch (Exception) { checknilai011 = null; } }
                                    string checknilai012x = readerCheck01["maklumat_lesen_daerah_tadbir"].ToString(); string checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = null; } else { try { checknilai012 = checknilai012x.ToString(); } catch (Exception) { checknilai012 = null; } }
                                    string checknilai013x = readerCheck01["maklumat_lesen_keluasan"].ToString(); string checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = null; } else { try { checknilai013 = checknilai013x.ToString(); } catch (Exception) { checknilai013 = null; } }
                                    string checknilai014x = readerCheck01["maklumat_lesen_hsk"].ToString(); string checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = null; } else { try { checknilai014 = checknilai014x.ToString(); } catch (Exception) { checknilai014 = null; } }
                                    string checknilai015x = readerCheck01["maklumat_lesen_nokompartment"].ToString(); string checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = null; } else { try { checknilai015 = checknilai015x.ToString(); } catch (Exception) { checknilai015 = null; } }
                                    string checknilai016x = readerCheck01["maklumat_lesen_meterpadu"].ToString(); string checknilai016; if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = null; } else { try { checknilai016 = checknilai016x.ToString(); } catch (Exception) { checknilai016 = null; } }
                                    string checknilai017x = readerCheck01["maklumat_lesen_batang"].ToString(); string checknilai017; if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = null; } else { try { checknilai017 = checknilai017x.ToString(); } catch (Exception) { checknilai017 = null; } }
                                    string checknilai018x = readerCheck01["maklumat_lesen_deposit"].ToString(); decimal checknilai018; if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = 0; } else { try { checknilai018 = decimal.Parse(checknilai018x); } catch (Exception) { checknilai018 = 0; } }
                                    string checknilai019x = readerCheck01["maklumat_lesen_royalti"].ToString(); decimal checknilai019; if (string.IsNullOrEmpty(checknilai019x)) { checknilai019 = 0; } else { try { checknilai019 = decimal.Parse(checknilai019x); } catch (Exception) { checknilai019 = 0; } }
                                    string checknilai020x = readerCheck01["maklumat_lesen_ses"].ToString(); decimal checknilai020; if (string.IsNullOrEmpty(checknilai020x)) { checknilai020 = 0; } else { try { checknilai020 = decimal.Parse(checknilai020x); } catch (Exception) { checknilai020 = 0; } }
                                    string checknilai021x = readerCheck01["maklumat_lesen_cukai"].ToString(); decimal checknilai021; if (string.IsNullOrEmpty(checknilai021x)) { checknilai021 = 0; } else { try { checknilai021 = decimal.Parse(checknilai021x); } catch (Exception) { checknilai021 = 0; } }
                                    string checknilai022x = readerCheck01["maklumat_lesen_pengeluaran"].ToString(); string checknilai022; if (string.IsNullOrEmpty(checknilai022x)) { checknilai022 = null; } else { try { checknilai022 = checknilai022x.ToString(); } catch (Exception) { checknilai022 = null; } }
                                    string checknilai023x = readerCheck01["maklumat_lesen_sahdari"].ToString(); string checknilai023; if (string.IsNullOrEmpty(checknilai023x) || checknilai023x == "0/0/0000" || checknilai023x == "1/01/0001 12:00:00 AM" || checknilai023x == "1/1/0001 12:00:00 AM" || checknilai023x == "1/01/0001 12:00:00 PG" || checknilai023x == "null") { checknilai023 = null; } else { try { checknilai023 = DateTime.ParseExact(checknilai023x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { checknilai023 = null; } }
                                    string checknilai024x = readerCheck01["maklumat_lesen_sahhingga"].ToString(); string checknilai024; if (string.IsNullOrEmpty(checknilai024x) || checknilai024x == "0/0/0000" || checknilai024x == "1/01/0001 12:00:00 AM" || checknilai024x == "1/1/0001 12:00:00 AM" || checknilai024x == "1/01/0001 12:00:00 PG" || checknilai024x == "null") { checknilai024 = null; } else { try { checknilai024 = DateTime.ParseExact(checknilai024x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { checknilai024 = null; } }
                                    string checknilai025x = readerCheck01["maklumat_lesen_tarikhlesen"].ToString(); string checknilai025; if (string.IsNullOrEmpty(checknilai025x) || checknilai025x == "0/0/0000" || checknilai025x == "1/01/0001 12:00:00 AM" || checknilai025x == "1/1/0001 12:00:00 AM" || checknilai025x == "1/01/0001 12:00:00 PG" || checknilai025x == "null") { checknilai025 = null; } else { try { checknilai025 = DateTime.ParseExact(checknilai025x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { checknilai025 = null; } }
                                    string checknilai026x = readerCheck01["maklumat_lesen_status"].ToString(); string checknilai026; if (string.IsNullOrEmpty(checknilai026x)) { checknilai026 = null; } else { try { checknilai026 = checknilai026x.ToString(); } catch (Exception) { checknilai026 = null; } }
                                    string checknilai027x = readerCheck01["maklumat_lesen_tarikhtamat"].ToString(); string checknilai027; if (string.IsNullOrEmpty(checknilai027x) || checknilai027x == "0/0/0000" || checknilai027x == "1/01/0001 12:00:00 AM" || checknilai027x == "1/1/0001 12:00:00 AM" || checknilai027x == "1/01/0001 12:00:00 PG" || checknilai027x == "null") { checknilai027 = null; } else { try { checknilai027 = DateTime.ParseExact(checknilai027x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { checknilai027 = null; } }
                                    string checknilai028x = readerCheck01["maklumat_lesen_meterpadu_awal"].ToString(); string checknilai028; if (string.IsNullOrEmpty(checknilai028x)) { checknilai028 = null; } else { try { checknilai028 = checknilai028x.ToString(); } catch (Exception) { checknilai028 = null; } }
                                    string checknilai029x = readerCheck01["maklumat_lesen_batang_awal"].ToString(); string checknilai029; if (string.IsNullOrEmpty(checknilai029x)) { checknilai029 = null; } else { try { checknilai029 = checknilai029x.ToString(); } catch (Exception) { checknilai029 = null; } }
                                    string checknilai030x = readerCheck01["maklumat_lesen_cukai_awal"].ToString(); decimal checknilai030; if (string.IsNullOrEmpty(checknilai030x)) { checknilai030 = 0; } else { try { checknilai030 = decimal.Parse(checknilai030x); } catch (Exception) { checknilai030 = 0; } }
                                    string checknilai031x = readerCheck01["maklumat_lesen_bbcagaran"].ToString(); decimal checknilai031; if (string.IsNullOrEmpty(checknilai031x)) { checknilai031 = 0; } else { try { checknilai031 = decimal.Parse(checknilai031x); } catch (Exception) { checknilai031 = 0; } }
                                    string checknilai032x = readerCheck01["maklumat_lesen_bbcukai"].ToString(); decimal checknilai032; if (string.IsNullOrEmpty(checknilai032x)) { checknilai032 = 0; } else { try { checknilai032 = decimal.Parse(checknilai032x); } catch (Exception) { checknilai032 = 0; } }
                                    string checknilai033x = readerCheck01["maklumat_lesen_resit"].ToString(); string checknilai033; if (string.IsNullOrEmpty(checknilai033x)) { checknilai033 = null; } else { try { checknilai033 = checknilai033x.ToString(); } catch (Exception) { checknilai033 = null; } }
                                    string checknilai034x = readerCheck01["maklumat_lesen_tarikh_pulangan"].ToString(); string checknilai034; if (string.IsNullOrEmpty(checknilai034x) || checknilai034x == "0/0/0000" || checknilai034x == "1/01/0001 12:00:00 AM" || checknilai034x == "1/1/0001 12:00:00 AM" || checknilai034x == "1/01/0001 12:00:00 PG" || checknilai034x == "null") { checknilai034 = null; } else { try { checknilai034 = DateTime.ParseExact(checknilai034x, "d/M/yyyy hh:mm:ss tt", null).ToString(); } catch (Exception) { checknilai034 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai017 != nilai017 || checknilai018 != nilai018 || checknilai019 != nilai019 || checknilai020 != nilai020 || checknilai021 != nilai021 || checknilai022 != nilai022 || checknilai023 != nilai023 || checknilai024 != nilai024 || checknilai025 != nilai025 || checknilai026 != nilai026 || checknilai027 != nilai027 || checknilai028 != nilai028 || checknilai029 != nilai029 || checknilai030 != nilai030 || checknilai031 != nilai031 || checknilai032 != nilai032 || checknilai033 != nilai033 || checknilai034 != nilai034)
                                    {
                                        try
                                        {
                                            connMSSQLDB_SMLSDB01 = new SqlConnection(dis_mssql_smldb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[maklumat_lesen] SET [maklumat_lesen_nolesen]=@maklumat_lesen_nolesen,[maklumat_lesen_namapelesen]=@maklumat_lesen_namapelesen,[maklumat_lesen_alamatpelesen]=@maklumat_lesen_alamatpelesen,[maklumat_lesen_negeri]=@maklumat_lesen_negeri,[maklumat_lesen_namakontraktor]=@maklumat_lesen_namakontraktor,[maklumat_lesen_alamatkontraktor]=@maklumat_lesen_alamatkontraktor,[maklumat_lesen_negeri3]=@maklumat_lesen_negeri3,[maklumat_lesen_negeri1]=@maklumat_lesen_negeri1,[maklumat_lesen_tanah]=@maklumat_lesen_tanah,[maklumat_lesen_daerah_hutan]=@maklumat_lesen_daerah_hutan,[maklumat_lesen_daerah_tadbir]=@maklumat_lesen_daerah_tadbir,[maklumat_lesen_keluasan]=@maklumat_lesen_keluasan,[maklumat_lesen_hsk]=@maklumat_lesen_hsk,[maklumat_lesen_nokompartment]=@maklumat_lesen_nokompartment,[maklumat_lesen_meterpadu]=@maklumat_lesen_meterpadu,[maklumat_lesen_batang]=@maklumat_lesen_batang,[maklumat_lesen_deposit]=@maklumat_lesen_deposit,[maklumat_lesen_royalti]=@maklumat_lesen_royalti,[maklumat_lesen_ses]=@maklumat_lesen_ses,[maklumat_lesen_cukai]=@maklumat_lesen_cukai,[maklumat_lesen_pengeluaran]=@maklumat_lesen_pengeluaran,[maklumat_lesen_sahdari]=@maklumat_lesen_sahdari,[maklumat_lesen_sahhingga]=@maklumat_lesen_sahhingga,[maklumat_lesen_tarikhlesen]=@maklumat_lesen_tarikhlesen,[maklumat_lesen_status]=@maklumat_lesen_status,[maklumat_lesen_tarikhtamat]=@maklumat_lesen_tarikhtamat,[maklumat_lesen_meterpadu_awal]=@maklumat_lesen_meterpadu_awal,[maklumat_lesen_batang_awal]=@maklumat_lesen_batang_awal,[maklumat_lesen_cukai_awal]=@maklumat_lesen_cukai_awal,[maklumat_lesen_bbcagaran]=@maklumat_lesen_bbcagaran,[maklumat_lesen_bbcukai]=@maklumat_lesen_bbcukai,[maklumat_lesen_resit]=@maklumat_lesen_resit,[maklumat_lesen_tarikh_pulangan]=@maklumat_lesen_tarikh_pulangan WHERE [maklumat_lesen_no]=@maklumat_lesen_no;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_SMLSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_no", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_no", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_nolesen", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_nolesen", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_namapelesen", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_namapelesen", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_alamatpelesen", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_alamatpelesen", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_negeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_negeri", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_namakontraktor", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_namakontraktor", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_alamatkontraktor", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_alamatkontraktor", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_negeri3", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_negeri3", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_negeri1", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_negeri1", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tanah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tanah", nilai010); }
                                            if (string.IsNullOrEmpty(nilai011)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_daerah_hutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_daerah_hutan", nilai011); }
                                            if (string.IsNullOrEmpty(nilai012)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_daerah_tadbir", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_daerah_tadbir", nilai012); }
                                            if (string.IsNullOrEmpty(nilai013)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_keluasan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_keluasan", nilai013); }
                                            if (string.IsNullOrEmpty(nilai014)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_hsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_hsk", nilai014); }
                                            if (string.IsNullOrEmpty(nilai015)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_nokompartment", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_nokompartment", nilai015); }
                                            if (string.IsNullOrEmpty(nilai016)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_meterpadu", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_meterpadu", nilai016); }
                                            if (string.IsNullOrEmpty(nilai017)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_batang", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_batang", nilai017); }
                                            if (nilai018 == 0) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_deposit", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_deposit", nilai018); }
                                            if (nilai019 == 0) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_royalti", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_royalti", nilai019); }
                                            if (nilai020 == 0) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_ses", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_ses", nilai020); }
                                            if (nilai021 == 0) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_cukai", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_cukai", nilai021); }
                                            if (string.IsNullOrEmpty(nilai022)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_pengeluaran", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_pengeluaran", nilai022); }
                                            if (string.IsNullOrEmpty(nilai023) || nilai023 == "0/0/0000" || nilai023 == "1/01/0001 12:00:00 AM" || nilai023 == "1/1/0001 12:00:00 AM" || nilai023 == "1/01/0001 12:00:00 PG" || nilai023 == "null") { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_sahdari", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_sahdari", DateTime.ParseExact(nilai023, "d/M/yyyy hh:mm:ss tt", null)); }
                                            if (string.IsNullOrEmpty(nilai024) || nilai024 == "0/0/0000" || nilai024 == "1/01/0001 12:00:00 AM" || nilai024 == "1/1/0001 12:00:00 AM" || nilai024 == "1/01/0001 12:00:00 PG" || nilai024 == "null") { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_sahhingga", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_sahhingga", DateTime.ParseExact(nilai024, "d/M/yyyy hh:mm:ss tt", null)); }
                                            if (string.IsNullOrEmpty(nilai025) || nilai025 == "0/0/0000" || nilai025 == "1/01/0001 12:00:00 AM" || nilai025 == "1/1/0001 12:00:00 AM" || nilai025 == "1/01/0001 12:00:00 PG" || nilai025 == "1/1/0001 12:00:00 AM" || nilai025 == "null") { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tarikhlesen", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tarikhlesen", DateTime.ParseExact(nilai025, "d/M/yyyy hh:mm:ss tt", null)); }
                                            if (string.IsNullOrEmpty(nilai026)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_status", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_status", nilai026); }
                                            if (string.IsNullOrEmpty(nilai027) || nilai027 == "0/0/0000" || nilai027 == "1/01/0001 12:00:00 AM" || nilai027 == "1/1/0001 12:00:00 AM" || nilai027 == "1/01/0001 12:00:00 PG" || nilai027 == "null") { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tarikhtamat", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tarikhtamat", DateTime.ParseExact(nilai027, "d/M/yyyy hh:mm:ss tt", null)); }
                                            if (string.IsNullOrEmpty(nilai028)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_meterpadu_awal", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_meterpadu_awal", nilai028); }
                                            if (string.IsNullOrEmpty(nilai029)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_batang_awal", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_batang_awal", nilai029); }
                                            if (nilai030 == 0) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_cukai_awal", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_cukai_awal", nilai030); }
                                            if (nilai031 == 0) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_bbcagaran", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_bbcagaran", nilai031); }
                                            if (nilai032 == 0) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_bbcukai", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_bbcukai", nilai032); }
                                            if (string.IsNullOrEmpty(nilai033)) { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_resit", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_resit", nilai033); }
                                            if (string.IsNullOrEmpty(nilai034) || nilai034 == "0/0/0000" || nilai034 == "1/01/0001 12:00:00 AM" || nilai034 == "1/1/0001 12:00:00 AM" || nilai034 == "1/01/0001 12:00:00 PG" || nilai034 == "null") { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tarikh_pulangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@maklumat_lesen_tarikh_pulangan", DateTime.ParseExact(nilai034, "d/M/yyyy hh:mm:ss tt", null)); }

                                            connMSSQLDB_SMLSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_SMLSDB0X = new SqlConnection(dis_mssql_smldb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[maklumat_lesen] SET [maklumat_lesen_date_update] = @maklumat_lesen_date_update,[maklumat_lesen_date_view] = @maklumat_lesen_date_view WHERE [maklumat_lesen_no] = @maklumat_lesen_no;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_SMLSDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_no", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_date_view", DateTime.Now);

                                                connMSSQLDB_SMLSDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();

                                            }
                                            catch (Exception ex)
                                            {
                                                await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_SMLSDB0X.Close();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_SMLSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_SMLSDB0X = new SqlConnection(dis_mssql_smldb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[maklumat_lesen] SET [maklumat_lesen_date_view] = @maklumat_lesen_date_view WHERE [maklumat_lesen_no] = @maklumat_lesen_no;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_SMLSDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_no", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_date_view", DateTime.Now);

                                            connMSSQLDB_SMLSDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();

                                        }
                                        catch (Exception ex)
                                        {
                                            await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_SMLSDB0X.Close();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_SMLSDB01 = new SqlConnection(dis_mssql_smldb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[maklumat_lesen] ([maklumat_lesen_no],[maklumat_lesen_nolesen],[maklumat_lesen_namapelesen],[maklumat_lesen_alamatpelesen],[maklumat_lesen_negeri],[maklumat_lesen_namakontraktor],[maklumat_lesen_alamatkontraktor],[maklumat_lesen_negeri3],[maklumat_lesen_negeri1],[maklumat_lesen_tanah],[maklumat_lesen_daerah_hutan],[maklumat_lesen_daerah_tadbir],[maklumat_lesen_keluasan],[maklumat_lesen_hsk],[maklumat_lesen_nokompartment],[maklumat_lesen_meterpadu],[maklumat_lesen_batang],[maklumat_lesen_deposit],[maklumat_lesen_royalti],[maklumat_lesen_ses],[maklumat_lesen_cukai],[maklumat_lesen_pengeluaran],[maklumat_lesen_sahdari],[maklumat_lesen_sahhingga],[maklumat_lesen_tarikhlesen],[maklumat_lesen_status],[maklumat_lesen_tarikhtamat],[maklumat_lesen_meterpadu_awal],[maklumat_lesen_batang_awal],[maklumat_lesen_cukai_awal],[maklumat_lesen_bbcagaran],[maklumat_lesen_bbcukai],[maklumat_lesen_resit],[maklumat_lesen_tarikh_pulangan]) VALUES(@maklumat_lesen_no,@maklumat_lesen_nolesen,@maklumat_lesen_namapelesen,@maklumat_lesen_alamatpelesen,@maklumat_lesen_negeri,@maklumat_lesen_namakontraktor,@maklumat_lesen_alamatkontraktor,@maklumat_lesen_negeri3,@maklumat_lesen_negeri1,@maklumat_lesen_tanah,@maklumat_lesen_daerah_hutan,@maklumat_lesen_daerah_tadbir,@maklumat_lesen_keluasan,@maklumat_lesen_hsk,@maklumat_lesen_nokompartment,@maklumat_lesen_meterpadu,@maklumat_lesen_batang,@maklumat_lesen_deposit,@maklumat_lesen_royalti,@maklumat_lesen_ses,@maklumat_lesen_cukai,@maklumat_lesen_pengeluaran,@maklumat_lesen_sahdari,@maklumat_lesen_sahhingga,@maklumat_lesen_tarikhlesen,@maklumat_lesen_status,@maklumat_lesen_tarikhtamat,@maklumat_lesen_meterpadu_awal,@maklumat_lesen_batang_awal,@maklumat_lesen_cukai_awal,@maklumat_lesen_bbcagaran,@maklumat_lesen_bbcukai,@maklumat_lesen_resit,@maklumat_lesen_tarikh_pulangan);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_SMLSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_no", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_no", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_nolesen", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_nolesen", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_namapelesen", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_namapelesen", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_alamatpelesen", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_alamatpelesen", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_negeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_negeri", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_namakontraktor", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_namakontraktor", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_alamatkontraktor", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_alamatkontraktor", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_negeri3", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_negeri3", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_negeri1", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_negeri1", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tanah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tanah", nilai010); }
                                    if (string.IsNullOrEmpty(nilai011)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_daerah_hutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_daerah_hutan", nilai011); }
                                    if (string.IsNullOrEmpty(nilai012)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_daerah_tadbir", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_daerah_tadbir", nilai012); }
                                    if (string.IsNullOrEmpty(nilai013)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_keluasan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_keluasan", nilai013); }
                                    if (string.IsNullOrEmpty(nilai014)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_hsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_hsk", nilai014); }
                                    if (string.IsNullOrEmpty(nilai015)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_nokompartment", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_nokompartment", nilai015); }
                                    if (string.IsNullOrEmpty(nilai016)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_meterpadu", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_meterpadu", nilai016); }
                                    if (string.IsNullOrEmpty(nilai017)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_batang", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_batang", nilai017); }
                                    if (nilai018 == 0) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_deposit", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_deposit", nilai018); }
                                    if (nilai019 == 0) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_royalti", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_royalti", nilai019); }
                                    if (nilai020 == 0) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_ses", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_ses", nilai020); }
                                    if (nilai021 == 0) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_cukai", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_cukai", nilai021); }
                                    if (string.IsNullOrEmpty(nilai022)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_pengeluaran", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_pengeluaran", nilai022); }
                                    if (nilai023 == null || nilai023 == "0/0/0000" || nilai023 == "1/01/0001 12:00:00 AM" || nilai023 == "1/1/0001 12:00:00 AM" || nilai023 == "1/01/0001 12:00:00 PG" || nilai023 == "null") { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_sahdari", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_sahdari", DateTime.ParseExact(nilai023, "d/M/yyyy hh:mm:ss tt", null)); }
                                    if (string.IsNullOrEmpty(nilai024) || nilai024 == "0/0/0000" || nilai024 == "1/01/0001 12:00:00 AM" || nilai024 == "1/1/0001 12:00:00 AM" || nilai024 == "1/01/0001 12:00:00 PG" || nilai024 == "null") { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_sahhingga", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_sahhingga", DateTime.ParseExact(nilai024, "d/M/yyyy hh:mm:ss tt", null)); }
                                    if (string.IsNullOrEmpty(nilai025) || nilai025 == "0/0/0000" || nilai025 == "1/01/0001 12:00:00 AM" || nilai025 == "1/1/0001 12:00:00 AM" || nilai025 == "1/01/0001 12:00:00 PG" || nilai025 == "null") { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tarikhlesen", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tarikhlesen", DateTime.ParseExact(nilai025, "d/M/yyyy hh:mm:ss tt", null)); }
                                    if (string.IsNullOrEmpty(nilai026)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_status", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_status", nilai026); }
                                    if (string.IsNullOrEmpty(nilai027) || nilai027 == "0/0/0000" || nilai027 == "1/01/0001 12:00:00 AM" || nilai027 == "1/1/0001 12:00:00 AM" || nilai027 == "1/01/0001 12:00:00 PG" || nilai027 == "null") { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tarikhtamat", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tarikhtamat", DateTime.ParseExact(nilai027, "d/M/yyyy hh:mm:ss tt", null)); }
                                    if (string.IsNullOrEmpty(nilai028)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_meterpadu_awal", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_meterpadu_awal", nilai028); }
                                    if (string.IsNullOrEmpty(nilai029)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_batang_awal", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_batang_awal", nilai029); }
                                    if (nilai030 == 0) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_cukai_awal", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_cukai_awal", nilai030); }
                                    if (nilai031 == 0) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_bbcagaran", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_bbcagaran", nilai031); }
                                    if (nilai032 == 0) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_bbcukai", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_bbcukai", nilai032); }
                                    if (string.IsNullOrEmpty(nilai033)) { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_resit", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_resit", nilai033); }
                                    if (string.IsNullOrEmpty(nilai034) || nilai034 == "0/0/0000" || nilai034 == "1/01/0001 12:00:00 AM" || nilai034 == "1/1/0001 12:00:00 AM" || nilai034 == "1/01/0001 12:00:00 PG" || nilai034 == "null") { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tarikh_pulangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@maklumat_lesen_tarikh_pulangan", DateTime.ParseExact(nilai034, "d/M/yyyy hh:mm:ss tt", null)); }

                                    connMSSQLDB_SMLSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_SMLSDB0X = new SqlConnection(dis_mssql_smldb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[maklumat_lesen] SET [maklumat_lesen_date_add] = @maklumat_lesen_date_add,[maklumat_lesen_date_view] = @maklumat_lesen_date_view WHERE [maklumat_lesen_no] = @maklumat_lesen_no;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_SMLSDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_no", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@maklumat_lesen_date_view", DateTime.Now);

                                        connMSSQLDB_SMLSDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();

                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("sml", "maklumat_lesen", nilai001, "error-date-add", ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_SMLSDB0X.Close();
                                    }
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("sml", "maklumat_lesen", nilai001, "error-insert-insert-", ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_SMLSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("sml", "maklumat_lesen", nilai001, "error-second-second-", ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_SMLSDB02.Close();
                        }
                    }
                }
                else
                {
                    await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_SMLSDB01.Close();
                //await syssts.StatusSystem_Integration("sml", "maklumat_lesen", null, "done", "done");
                await syssts.StatusSystem_Integration2(sistem_servername, "sml", "maklumat_lesen", null, null, "done", "DONE_SYSTEMINTEGRATION_SML", null, null);
            }
            await Task.Delay(10);
        }

        public async Task updater_esmash()
        {

            var syssts = new DIS_StatusSystem();
            await initialconnection();

            //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan
            //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `cadangan_fungsi_hutan`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodDaerah"].ToString();                                                          //string
                        string nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; }
                        else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["Tahun"].ToString();                                                              //string
                        string nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; }
                        else { try { nilai004 = nilai001x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodSukuTahun"].ToString();                                                       //string
                        string nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; }
                        else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["CadanganHSK"].ToString();                                                        //string
                        string nilai006;
                        if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; }
                        else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["KodFungsiHutan"].ToString();                                                     //string
                        string nilai007;
                        if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; }
                        else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["LuasWartaCadangan"].ToString();                                                  //decimal
                        decimal nilai008;
                        if (string.IsNullOrEmpty(nilai008x)) { nilai008 = 0; }
                        else { try { nilai008 = decimal.Parse(nilai008x); } catch (Exception) { nilai008 = 0; } }
                        string nilai009x = reader["LuasWartaDiwarta"].ToString();                                                   //decimal
                        decimal nilai009;
                        if (string.IsNullOrEmpty(nilai009x)) { nilai009 = 0; }
                        else { try { nilai009 = decimal.Parse(nilai009x); } catch (Exception) { nilai009 = 0; } }
                        string nilai010x = reader["LuasDigitalCadangan"].ToString();                                                //decimal
                        decimal nilai010;
                        if (string.IsNullOrEmpty(nilai010x)) { nilai010 = 0; }
                        else { try { nilai010 = decimal.Parse(nilai010x); } catch (Exception) { nilai010 = 0; } }
                        string nilai011x = reader["LuasDigitalDiwarta"].ToString();                                                 //decimal
                        decimal nilai011;
                        if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; }
                        else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["IndikatorBertindih"].ToString();                                                 //string
                        string nilai012;
                        if (string.IsNullOrEmpty(nilai012x)) { nilai012 = null; }
                        else { try { nilai012 = nilai012x.ToString(); } catch (Exception) { nilai012 = null; } }
                        string nilai013x = reader["NoWarta"].ToString();                                                            //string
                        string nilai013;
                        if (string.IsNullOrEmpty(nilai013x)) { nilai013 = null; }
                        else { try { nilai013 = nilai013x.ToString(); } catch (Exception) { nilai013 = null; } }
                        string nilai014x = reader["NoPelanWarta"].ToString();                                                       //string
                        string nilai014;
                        if (string.IsNullOrEmpty(nilai014x)) { nilai014 = null; }
                        else { try { nilai014 = nilai014x.ToString(); } catch (Exception) { nilai014 = null; } }
                        string nilai015x = reader["TarikhWarta"].ToString();                                                        //string
                        string nilai015;
                        if (string.IsNullOrEmpty(nilai015x)) { nilai015 = null; }
                        else { try { nilai015 = nilai015x.ToString(); } catch (Exception) { nilai015 = null; } }


                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[cadangan_fungsi_hutan] WHERE [cadanganfungsihutan_recordid] = @cadanganfungsihutan_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@cadanganfungsihutan_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["cadanganfungsihutan_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["cadanganfungsihutan_kodnegeri"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["cadanganfungsihutan_koddaerah"].ToString();                                                          //string
                                    string checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; }
                                    else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["cadanganfungsihutan_tahun"].ToString();                                                              //string
                                    string checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; }
                                    else { try { checknilai004 = checknilai001x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["cadanganfungsihutan_kodsukutahun"].ToString();                                                       //string
                                    string checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; }
                                    else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["cadanganfungsihutan_cadanganhsk"].ToString();                                                        //string
                                    string checknilai006;
                                    if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; }
                                    else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["cadanganfungsihutan_kodfungsihutan"].ToString();                                                     //string
                                    string checknilai007;
                                    if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; }
                                    else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["cadanganfungsihutan_luaswartacadangan"].ToString();                                                  //decimal
                                    decimal checknilai008;
                                    if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = 0; }
                                    else { try { checknilai008 = decimal.Parse(checknilai008x); } catch (Exception) { checknilai008 = 0; } }
                                    string checknilai009x = readerCheck01["cadanganfungsihutan_luaswartadiwarta"].ToString();                                                   //decimal
                                    decimal checknilai009;
                                    if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = 0; }
                                    else { try { checknilai009 = decimal.Parse(checknilai009x); } catch (Exception) { checknilai009 = 0; } }
                                    string checknilai010x = readerCheck01["cadanganfungsihutan_luasdigitalcadangan"].ToString();                                                //decimal
                                    decimal checknilai010;
                                    if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = 0; }
                                    else { try { checknilai010 = decimal.Parse(checknilai010x); } catch (Exception) { checknilai010 = 0; } }
                                    string checknilai011x = readerCheck01["cadanganfungsihutan_luasdigitaldiwarta"].ToString();                                                 //decimal
                                    decimal checknilai011;
                                    if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; }
                                    else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["cadanganfungsihutan_indikatorbertindih"].ToString();                                                 //string
                                    string checknilai012;
                                    if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = null; }
                                    else { try { checknilai012 = checknilai012x.ToString(); } catch (Exception) { checknilai012 = null; } }
                                    string checknilai013x = readerCheck01["cadanganfungsihutan_nowarta"].ToString();                                                            //string
                                    string checknilai013;
                                    if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = null; }
                                    else { try { checknilai013 = checknilai013x.ToString(); } catch (Exception) { checknilai013 = null; } }
                                    string checknilai014x = readerCheck01["cadanganfungsihutan_nopelanwarta"].ToString();                                                       //string
                                    string checknilai014;
                                    if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = null; }
                                    else { try { checknilai014 = checknilai014x.ToString(); } catch (Exception) { checknilai014 = null; } }
                                    string checknilai015x = readerCheck01["cadanganfungsihutan_tarikhwarta"].ToString();                                                        //string
                                    string checknilai015;
                                    if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = null; }
                                    else { try { checknilai015 = checknilai015x.ToString(); } catch (Exception) { checknilai015 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[cadangan_fungsi_hutan] SET [cadanganfungsihutan_kodnegeri]=@cadanganfungsihutan_kodnegeri, [cadanganfungsihutan_koddaerah]=@cadanganfungsihutan_koddaerah, [cadanganfungsihutan_tahun]=@cadanganfungsihutan_tahun, [cadanganfungsihutan_kodsukutahun]=@cadanganfungsihutan_kodsukutahun, [cadanganfungsihutan_cadanganhsk]=@cadanganfungsihutan_cadanganhsk, [cadanganfungsihutan_kodfungsihutan]=@cadanganfungsihutan_kodfungsihutan, [cadanganfungsihutan_luaswartacadangan]=@cadanganfungsihutan_luaswartacadangan, [cadanganfungsihutan_luaswartadiwarta]=@cadanganfungsihutan_luaswartadiwarta, [cadanganfungsihutan_luasdigitalcadangan]=@cadanganfungsihutan_luasdigitalcadangan, [cadanganfungsihutan_luasdigitaldiwarta]=@cadanganfungsihutan_luasdigitaldiwarta, [cadanganfungsihutan_indikatorbertindih]=@cadanganfungsihutan_indikatorbertindih, [cadanganfungsihutan_nowarta]=@cadanganfungsihutan_nowarta, [cadanganfungsihutan_nopelanwarta]=@cadanganfungsihutan_nopelanwarta, [cadanganfungsihutan_tarikhwarta]=@cadanganfungsihutan_tarikhwarta WHERE [cadanganfungsihutan_recordid]=@cadanganfungsihutan_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_recordid", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_kodnegeri", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_koddaerah", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_koddaerah", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_tahun", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_tahun", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_kodsukutahun", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_kodsukutahun", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_cadanganhsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_cadanganhsk", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_kodfungsihutan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_kodfungsihutan", nilai007); }
                                            if (nilai008 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartacadangan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartacadangan", nilai008); }
                                            if (nilai009 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartadiwarta", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartadiwarta", nilai009); }
                                            if (nilai010 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitalcadangan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitalcadangan", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitaldiwarta", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitaldiwarta", nilai011); }
                                            if (string.IsNullOrEmpty(nilai012)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_indikatorbertindih", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_indikatorbertindih", nilai012); }
                                            if (string.IsNullOrEmpty(nilai013)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_nowarta", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_nowarta", nilai013); }
                                            if (string.IsNullOrEmpty(nilai014)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_nopelanwarta", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_nopelanwarta", nilai014); }
                                            if (string.IsNullOrEmpty(nilai015)) { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_tarikhwarta", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganfungsihutan_tarikhwarta", nilai015); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[cadangan_fungsi_hutan] SET [cadanganfungsihutan_date_update] = @cadanganfungsihutan_date_update,[cadanganfungsihutan_date_view] = @cadanganfungsihutan_date_view WHERE [cadanganfungsihutan_recordid] = @cadanganfungsihutan_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", nilai001, "error-date-update", ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", nilai001, "error-update-update", ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[cadangan_fungsi_hutan] SET [cadanganfungsihutan_date_view] = @cadanganfungsihutan_date_view WHERE [cadanganfungsihutan_recordid] = @cadanganfungsihutan_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", nilai001, "error-date-view", ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[cadangan_fungsi_hutan] ([cadanganfungsihutan_recordid],[cadanganfungsihutan_kodnegeri],[cadanganfungsihutan_koddaerah],[cadanganfungsihutan_tahun],[cadanganfungsihutan_kodsukutahun],[cadanganfungsihutan_cadanganhsk],[cadanganfungsihutan_kodfungsihutan],[cadanganfungsihutan_luaswartacadangan],[cadanganfungsihutan_luaswartadiwarta],[cadanganfungsihutan_luasdigitalcadangan],[cadanganfungsihutan_luasdigitaldiwarta],[cadanganfungsihutan_indikatorbertindih],[cadanganfungsihutan_nowarta],[cadanganfungsihutan_nopelanwarta],[cadanganfungsihutan_tarikhwarta]) VALUES(@cadanganfungsihutan_recordid,@cadanganfungsihutan_kodnegeri,@cadanganfungsihutan_koddaerah,@cadanganfungsihutan_tahun,@cadanganfungsihutan_kodsukutahun,@cadanganfungsihutan_cadanganhsk,@cadanganfungsihutan_kodfungsihutan,@cadanganfungsihutan_luaswartacadangan,@cadanganfungsihutan_luaswartadiwarta,@cadanganfungsihutan_luasdigitalcadangan,@cadanganfungsihutan_luasdigitaldiwarta,@cadanganfungsihutan_indikatorbertindih,@cadanganfungsihutan_nowarta,@cadanganfungsihutan_nopelanwarta,@cadanganfungsihutan_tarikhwarta);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_recordid", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_kodnegeri", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_koddaerah", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_koddaerah", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_tahun", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_tahun", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_kodsukutahun", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_kodsukutahun", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_cadanganhsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_cadanganhsk", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_kodfungsihutan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_kodfungsihutan", nilai007); }
                                    if (nilai008 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartacadangan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartacadangan", nilai008); }
                                    if (nilai009 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartadiwarta", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luaswartadiwarta", nilai009); }
                                    if (nilai010 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitalcadangan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitalcadangan", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitaldiwarta", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_luasdigitaldiwarta", nilai011); }
                                    if (string.IsNullOrEmpty(nilai012)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_indikatorbertindih", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_indikatorbertindih", nilai012); }
                                    if (string.IsNullOrEmpty(nilai013)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_nowarta", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_nowarta", nilai013); }
                                    if (string.IsNullOrEmpty(nilai014)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_nopelanwarta", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_nopelanwarta", nilai014); }
                                    if (string.IsNullOrEmpty(nilai015)) { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_tarikhwarta", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganfungsihutan_tarikhwarta", nilai015); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[cadangan_fungsi_hutan] SET [cadanganfungsihutan_date_add] = @cadanganfungsihutan_date_add,[cadanganfungsihutan_date_view] = @cadanganfungsihutan_date_view WHERE [cadanganfungsihutan_recordid] = @cadanganfungsihutan_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganfungsihutan_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", nilai001, "error-date-add", ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", nilai001, "error-insert-insert-", ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", nilai001, "error-second-second-", ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", null, "nodata", "nodata");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", null, "error-first-first-", ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "cadangan_fungsi_hutan", null, "done", "done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_fungsi_hutan", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan
            //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan //controller_eSmashCadanganFungsiHutan

            await Task.Delay(1);

            //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal
            //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `cadangan_hutan_simpanan_kekal`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString();                                                           //string
                        string nilai001;
                        if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; }
                        else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NamaHSK"].ToString();                                                          //string
                        string nilai002;
                        if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; }
                        else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodNegeri"].ToString();                                                          //string
                        string nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; }
                        else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodDaerah"].ToString();                                                              //string
                        string nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; }
                        else { try { nilai004 = nilai001x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodHSK"].ToString();                                                       //string
                        string nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; }
                        else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["KeluasanHutan"].ToString();                                                  //decimal
                        decimal nilai006;
                        if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; }
                        else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[cadangan_hutan_simpanan_kekal] WHERE [cadanganhutansimpanankekal_recordid] = @cadanganhutansimpanankekal_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["cadanganhutansimpanankekal_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["cadanganhutansimpanankekal_namahsk"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["cadanganhutansimpanankekal_kodnegeri"].ToString();                                                          //string
                                    string checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; }
                                    else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["cadanganhutansimpanankekal_koddaerah"].ToString();                                                              //string
                                    string checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; }
                                    else { try { checknilai004 = checknilai001x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["cadanganhutansimpanankekal_kodhsk"].ToString();                                                       //string
                                    string checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; }
                                    else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["cadanganhutansimpanankekal_keluasanhutan"].ToString();                                                  //decimal
                                    decimal checknilai006;
                                    if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; }
                                    else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[cadangan_hutan_simpanan_kekal] SET [cadanganhutansimpanankekal_namahsk] = @cadanganhutansimpanankekal_namahsk, [cadanganhutansimpanankekal_kodnegeri] = @cadanganhutansimpanankekal_kodnegeri, [cadanganhutansimpanankekal_koddaerah] = @cadanganhutansimpanankekal_koddaerah, [cadanganhutansimpanankekal_kodhsk] = @cadanganhutansimpanankekal_kodhsk, [cadanganhutansimpanankekal_keluasanhutan] = @@cadanganhutansimpanankekal_keluasanhutan WHERE [cadanganhutansimpanankekal_recordid] = @cadanganhutansimpanankekal_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_namahsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_namahsk", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodnegeri", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodnegeri", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_koddaerah", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_koddaerah", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodhsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodhsk", nilai005); }

                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_keluasanhutan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganhutansimpanankekal_keluasanhutan", nilai006); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[cadangan_hutan_simpanan_kekal] SET [cadanganhutansimpanankekal_date_update] = @cadanganhutansimpanankekal_date_update,[cadanganhutansimpanankekal_date_view] = @cadanganhutansimpanankekal_date_view WHERE [cadanganhutansimpanankekal_recordid] = @cadanganhutansimpanankekal_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", nilai001, "error-date-update", ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", nilai001, "error-update-update", ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[cadangan_hutan_simpanan_kekal] SET [cadanganhutansimpanankekal_date_view] = @cadanganhutansimpanankekal_date_view WHERE [cadanganhutansimpanankekal_recordid] = @cadanganhutansimpanankekal_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", nilai001, "error-date-view", ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[cadangan_hutan_simpanan_kekal] ([cadanganhutansimpanankekal_recordid],[cadanganhutansimpanankekal_namahsk],[cadanganhutansimpanankekal_kodnegeri],[cadanganhutansimpanankekal_koddaerah],[cadanganhutansimpanankekal_kodhsk],[cadanganhutansimpanankekal_keluasanhutan]) VALUES( @cadanganhutansimpanankekal_recordid, @cadanganhutansimpanankekal_namahsk, @cadanganhutansimpanankekal_kodnegeri, @cadanganhutansimpanankekal_koddaerah, @cadanganhutansimpanankekal_kodhsk, @cadanganhutansimpanankekal_keluasanhutan);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_namahsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_namahsk", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodnegeri", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodnegeri", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_koddaerah", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_koddaerah", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodhsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_kodhsk", nilai005); }

                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_keluasanhutan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganhutansimpanankekal_keluasanhutan", nilai006); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[cadangan_hutan_simpanan_kekal] SET [cadanganhutansimpanankekal_date_add] = @cadanganhutansimpanankekal_date_add,[cadanganhutansimpanankekal_date_view] = @cadanganhutansimpanankekal_date_view WHERE [cadanganhutansimpanankekal_recordid] = @cadanganhutansimpanankekal_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganhutansimpanankekal_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", nilai001, "error-date-add", ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", nilai001, "error-insert-insert-", ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", nilai001, "error-second-second-", ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", null, "nodata", "nodata");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", null, "error-first-first-", ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                // syssts.StatusSystem_Integration("esmash", "cadangan_hutan_simpanan_kekal", null, "done", "done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_hutan_simpanan_kekal", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal
            //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal //controller_eSmashCadanganHutanSimpananKekal

            await Task.Delay(1);

            //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan
            //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan 
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `cadangan_jenis_hutan`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString();                                                           //string
                        string nilai001;
                        if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; }
                        else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString();                                                          //string
                        string nilai002;
                        if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; }
                        else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodDaerah"].ToString();                                                          //string
                        string nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; }
                        else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["Tahun"].ToString();                                                              //string
                        string nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; }
                        else { try { nilai004 = nilai001x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodSukuTahun"].ToString();                                                       //string
                        string nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; }
                        else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["CadanganHSK"].ToString();                                                        //string
                        string nilai006;
                        if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; }
                        else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["TarikhMMKN"].ToString();                                                         //string
                        string nilai007;
                        if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; }
                        else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["NoRujukanMMKN"].ToString();                                                     //string
                        string nilai008;
                        if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; }
                        else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["Catatan"].ToString();                                                            //string
                        string nilai009;
                        if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; }
                        else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["JH_01_LSediaAda"].ToString();                                                 //decimal
                        decimal nilai010;
                        if (string.IsNullOrEmpty(nilai010x)) { nilai010 = 0; }
                        else { try { nilai010 = decimal.Parse(nilai010x); } catch (Exception) { nilai010 = 0; } }
                        string nilai011x = reader["JH_02_LSediaAda"].ToString();                                                 //decimal
                        decimal nilai011;
                        if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; }
                        else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["JH_03_LSediaAda"].ToString();                                                 //decimal
                        decimal nilai012;
                        if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; }
                        else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["JH_04_LSediaAda"].ToString();                                                 //decimal
                        decimal nilai013;
                        if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; }
                        else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["JH_01_LCadangan"].ToString();                                                 //decimal
                        decimal nilai014;
                        if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; }
                        else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["JH_02_LCadangan"].ToString();                                                 //decimal
                        decimal nilai015;
                        if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; }
                        else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }
                        string nilai016x = reader["JH_03_LCadangan"].ToString();                                                 //decimal
                        decimal nilai016;
                        if (string.IsNullOrEmpty(nilai016x)) { nilai016 = 0; }
                        else { try { nilai016 = decimal.Parse(nilai016x); } catch (Exception) { nilai016 = 0; } }
                        string nilai017x = reader["JH_04_LCadangan"].ToString();                                                 //decimal
                        decimal nilai017;
                        if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; }
                        else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }
                        string nilai018x = reader["JenisRekod"].ToString();                                                     //string
                        string nilai018;
                        if (string.IsNullOrEmpty(nilai018x)) { nilai018 = null; }
                        else { try { nilai018 = nilai018x.ToString(); } catch (Exception) { nilai018 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[cadangan_jenis_hutan] WHERE [cadanganjenishutan_recordid] = @cadanganjenishutan_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@cadanganjenishutan_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["cadanganjenishutan_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["cadanganjenishutan_kodnegeri"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["cadanganjenishutan_koddaerah"].ToString();                                                          //string
                                    string checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; }
                                    else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["cadanganjenishutan_tahun"].ToString();                                                              //string
                                    string checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; }
                                    else { try { checknilai004 = checknilai001x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["cadanganjenishutan_kodsukutahun"].ToString();                                                       //string
                                    string checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; }
                                    else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["cadanganjenishutan_cadanganhsk"].ToString();                                                        //string
                                    string checknilai006;
                                    if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; }
                                    else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["cadanganjenishutan_tarikhmmkn"].ToString();                                                     //string
                                    string checknilai007;
                                    if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; }
                                    else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["cadanganjenishutan_norujukanmmkn"].ToString();                                                     //string
                                    string checknilai008;
                                    if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; }
                                    else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["cadanganjenishutan_catatan"].ToString();                                                     //string
                                    string checknilai009;
                                    if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; }
                                    else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["cadanganjenishutan_jh_01_lsediaada"].ToString();                                                  //decimal
                                    decimal checknilai010;
                                    if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = 0; }
                                    else { try { checknilai010 = decimal.Parse(checknilai010x); } catch (Exception) { checknilai010 = 0; } }
                                    string checknilai011x = readerCheck01["cadanganjenishutan_jh_02_lsediaada"].ToString();                                                  //decimal
                                    decimal checknilai011;
                                    if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; }
                                    else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["cadanganjenishutan_jh_03_lsediaAda"].ToString();                                                  //decimal
                                    decimal checknilai012;
                                    if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; }
                                    else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["cadanganjenishutan_jh_04_lsediaAda"].ToString();                                                  //decimal
                                    decimal checknilai013;
                                    if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; }
                                    else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["cadanganjenishutan_jh_01_lcadangan"].ToString();                                                  //decimal
                                    decimal checknilai014;
                                    if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; }
                                    else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["cadanganjenishutan_jh_02_lcadangan"].ToString();                                                  //decimal
                                    decimal checknilai015;
                                    if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; }
                                    else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }
                                    string checknilai016x = readerCheck01["cadanganjenishutan_jh_03_lcadangan"].ToString();                                                  //decimal
                                    decimal checknilai016;
                                    if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = 0; }
                                    else { try { checknilai016 = decimal.Parse(checknilai016x); } catch (Exception) { checknilai016 = 0; } }
                                    string checknilai017x = readerCheck01["cadanganjenishutan_jh_04_lcadangan"].ToString();                                                  //decimal
                                    decimal checknilai017;
                                    if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; }
                                    else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }
                                    string checknilai018x = readerCheck01["cadanganjenishutan_jenisrekod"].ToString();                                                     //string
                                    string checknilai018;
                                    if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = null; }
                                    else { try { checknilai018 = checknilai018x.ToString(); } catch (Exception) { checknilai018 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai016 != nilai016 || checknilai017 != nilai017 || checknilai018 != nilai018)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[cadangan_jenis_hutan] SET [cadanganjenishutan_kodnegeri] = @cadanganjenishutan_kodnegeri, [cadanganjenishutan_koddaerah] = @cadanganjenishutan_koddaerah, [cadanganjenishutan_tahun] = @cadanganjenishutan_tahun, [cadanganjenishutan_kodsukutahun] = @cadanganjenishutan_kodsukutahun, [cadanganjenishutan_cadanganhsk] = @cadanganjenishutan_cadanganhsk, [cadanganjenishutan_tarikhmmkn] = @cadanganjenishutan_tarikhmmkn, [cadanganjenishutan_norujukanmmkn] = @cadanganjenishutan_norujukanmmkn, [cadanganjenishutan_catatan] = @cadanganjenishutan_catatan, [cadanganjenishutan_jh_01_lsediaada] = @cadanganjenishutan_jh_01_lsediaada, [cadanganjenishutan_jh_02_lsediaada] = @cadanganjenishutan_jh_02_lsediaada, [cadanganjenishutan_jh_03_lsediaada] = @cadanganjenishutan_jh_03_lsediaada, [cadanganjenishutan_jh_04_lsediaada] = @cadanganjenishutan_jh_04_lsediaada, [cadanganjenishutan_jh_01_lcadangan] = @cadanganjenishutan_jh_01_lcadangan, [cadanganjenishutan_jh_02_lcadangan] = @cadanganjenishutan_jh_02_lcadangan, [cadanganjenishutan_jh_03_lcadangan] = @cadanganjenishutan_jh_03_lcadangan, [cadanganjenishutan_jh_04_lcadangan] = @cadanganjenishutan_jh_04_lcadangan, [cadanganjenishutan_jenisrekod] = @cadanganjenishutan_jenisrekod WHERE [cadanganjenishutan_recordid] = @cadanganjenishutan_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_recordid", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_kodnegeri", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_koddaerah", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_koddaerah", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_tahun", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_tahun", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_kodsukutahun", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_kodsukutahun", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_cadanganhsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_cadanganhsk", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_tarikhmmkn", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_tarikhmmkn", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_norujukanmmkn", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_norujukanmmkn", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_catatan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_catatan", nilai009); }

                                            if (nilai010 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lsediaada", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lsediaada", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lsediaada", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lsediaada", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lsediaada", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lsediaada", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lsediaada", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lsediaada", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lcadangan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lcadangan", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lcadangan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lcadangan", nilai015); }
                                            if (nilai016 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lcadangan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lcadangan", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lcadangan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lcadangan", nilai017); }

                                            if (string.IsNullOrEmpty(nilai018)) { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jenisrekod", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@cadanganjenishutan_jenisrekod", nilai018); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[cadangan_jenis_hutan] SET [cadanganjenishutan_date_update] = @cadanganjenishutan_date_update,[cadanganjenishutan_date_view] = @cadanganjenishutan_date_view WHERE [cadanganjenishutan_recordid] = @cadanganjenishutan_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-cadangan_jenis_hutan-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("xxxeSmash-cadangan_jenis_hutan-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("xxxeSmash-cadangan_jenis_hutan-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            //MessageBox.Show("SML - Update - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[cadangan_jenis_hutan] SET [cadanganjenishutan_date_view] = @cadanganjenishutan_date_view WHERE [cadanganjenishutan_recordid] = @cadanganjenishutan_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-cadangan_jenis_hutan-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("xxxeSmash-cadangan_jenis_hutan-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[cadangan_jenis_hutan] ([cadanganjenishutan_recordid],[cadanganjenishutan_kodnegeri],[cadanganjenishutan_koddaerah],[cadanganjenishutan_tahun],[cadanganjenishutan_kodsukutahun],[cadanganjenishutan_cadanganhsk],[cadanganjenishutan_tarikhmmkn],[cadanganjenishutan_norujukanmmkn],[cadanganjenishutan_catatan],[cadanganjenishutan_jh_01_lsediaada],[cadanganjenishutan_jh_02_lsediaada],[cadanganjenishutan_jh_03_lsediaada],[cadanganjenishutan_jh_04_lsediaada],[cadanganjenishutan_jh_01_lcadangan],[cadanganjenishutan_jh_02_lcadangan],[cadanganjenishutan_jh_03_lcadangan],[cadanganjenishutan_jh_04_lcadangan],[cadanganjenishutan_jenisrekod]) VALUES(@cadanganjenishutan_recordid,@cadanganjenishutan_kodnegeri,@cadanganjenishutan_koddaerah,@cadanganjenishutan_tahun,@cadanganjenishutan_kodsukutahun,@cadanganjenishutan_cadanganhsk,@cadanganjenishutan_tarikhmmkn,@cadanganjenishutan_norujukanmmkn,@cadanganjenishutan_catatan,@cadanganjenishutan_jh_01_lsediaada,@cadanganjenishutan_jh_02_lsediaada,@cadanganjenishutan_jh_03_lsediaada,@cadanganjenishutan_jh_04_lsediaada,@cadanganjenishutan_jh_01_lcadangan,@cadanganjenishutan_jh_02_lcadangan,@cadanganjenishutan_jh_03_lcadangan,@cadanganjenishutan_jh_04_lcadangan,@cadanganjenishutan_jenisrekod);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_recordid", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_kodnegeri", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_koddaerah", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_koddaerah", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_tahun", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_tahun", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_kodsukutahun", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_kodsukutahun", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_cadanganhsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_cadanganhsk", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_tarikhmmkn", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_tarikhmmkn", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_norujukanmmkn", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_norujukanmmkn", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_catatan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_catatan", nilai009); }

                                    if (nilai010 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lsediaada", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lsediaada", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lsediaada", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lsediaada", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lsediaada", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lsediaada", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lsediaada", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lsediaada", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lcadangan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_01_lcadangan", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lcadangan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_02_lcadangan", nilai015); }
                                    if (nilai016 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lcadangan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_03_lcadangan", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lcadangan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jh_04_lcadangan", nilai017); }

                                    if (string.IsNullOrEmpty(nilai018)) { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jenisrekod", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@cadanganjenishutan_jenisrekod", nilai018); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[cadangan_jenis_hutan] SET [cadanganjenishutan_date_add] = @cadanganjenishutan_date_add,[cadanganjenishutan_date_view] = @cadanganjenishutan_date_view WHERE [cadanganjenishutan_recordid] = @cadanganjenishutan_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@cadanganjenishutan_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-cadangan_jenis_hutan-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("xxxeSmash-cadangan_jenis_hutan-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("xxxeSmash-cadangan_jenis_hutan-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-cadangan_jenis_hutan-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-cadangan_jenis_hutan-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-cadangan_jenis_hutan-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "cadangan_jenis_hutan", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-cadangan_jenis_hutan-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "cadangan_jenis_hutan", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan
            //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan //controller_eSmashCadanganJenisHutan

            await Task.Delay(1);

            //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan
            //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `daerah_hutan`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString();                                                           //string
                        string nilai001;
                        if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; }
                        else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NamaDaerah"].ToString();                                                          //string
                        string nilai002;
                        if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; }
                        else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodNegeri"].ToString();                                                          //string
                        string nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; }
                        else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodDaerah"].ToString();                                                              //string
                        string nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; }
                        else { try { nilai004 = nilai001x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KeluasanHutan"].ToString();                                                  //decimal
                        decimal nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = 0; }
                        else { try { nilai005 = decimal.Parse(nilai005x); } catch (Exception) { nilai005 = 0; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[daerah_hutan] WHERE [daerahhutan_recordid] = @daerahhutan_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@daerahhutan_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["daerahhutan_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["daerahhutan_namadaerah"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["daerahhutan_kodnegeri"].ToString();                                                          //string
                                    string checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; }
                                    else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["daerahhutan_koddaerah"].ToString();                                                              //string
                                    string checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; }
                                    else { try { checknilai004 = checknilai001x.ToString(); } catch (Exception) { checknilai004 = null; } }

                                    string checknilai005x = readerCheck01["daerahhutan_keluasanhutan"].ToString();                                                  //decimal
                                    decimal checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = 0; }
                                    else { try { checknilai005 = decimal.Parse(checknilai005x); } catch (Exception) { checknilai005 = 0; } }


                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[daerah_hutan] SET [daerahhutan_namadaerah] = @daerahhutan_namadaerah, [daerahhutan_kodnegeri] = @daerahhutan_kodnegeri, [daerahhutan_koddaerah] = @daerahhutan_koddaerah, [daerahhutan_keluasanhutan] = @daerahhutan_keluasanhutan WHERE [daerahhutan_recordid] = @daerahhutan_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_recordid", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_namadaerah", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_namadaerah", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_kodnegeri", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_kodnegeri", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_koddaerah", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_koddaerah", nilai004); }

                                            if (nilai005 == 0) { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_keluasanhutan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@daerahhutan_keluasanhutan", nilai005); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[daerah_hutan] SET [daerahhutan_date_update] = @daerahhutan_date_update,[daerahhutan_date_view] = @daerahhutan_date_view WHERE [daerahhutan_recordid] = @daerahhutan_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@daerahhutan_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@daerahhutan_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@daerahhutan_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                ///await syssts.StatusSystem_Integration("esmash", "daerah_hutan", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-daerah_hutan-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-daerah_hutan-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("SML-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            //MessageBox.Show("SML - Update - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[daerah_hutan] SET [daerahhutan_date_view] = @daerahhutan_date_view WHERE [daerahhutan_recordid] = @daerahhutan_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@daerahhutan_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@daerahhutan_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-daerah_hutan-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-daerah_hutan-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[daerah_hutan] ([daerahhutan_recordid],[daerahhutan_namadaerah],[daerahhutan_kodnegeri],[daerahhutan_koddaerah],[daerahhutan_keluasanhutan]) VALUES( @daerahhutan_recordid, @daerahhutan_namadaerah, @daerahhutan_kodnegeri, @daerahhutan_koddaerah, @daerahhutan_keluasanhutan);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@daerahhutan_recordid", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@daerahhutan_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@daerahhutan_namadaerah", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@daerahhutan_namadaerah", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@daerahhutan_kodnegeri", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@daerahhutan_kodnegeri", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@daerahhutan_koddaerah", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@daerahhutan_koddaerah", nilai004); }

                                    if (nilai005 == 0) { cmdInsert01.Parameters.AddWithValue("@daerahhutan_keluasanhutan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@daerahhutan_keluasanhutan", nilai005); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[daerah_hutan] SET [daerahhutan_date_add] = @daerahhutan_date_add,[daerahhutan_date_view] = @daerahhutan_date_view WHERE [daerahhutan_recordid] = @daerahhutan_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@daerahhutan_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@daerahhutan_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@daerahhutan_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-daerah_hutan-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-daerah_hutan-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-daerah_hutan-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-daerah_hutan-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-daerah_hutan-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-daerah_hutan-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "daerah_hutan", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-daerah_hutan-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "daerah_hutan", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            await Task.Delay(1);

            //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan
            //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan //controller_eSmashDaerahHutan

            await Task.Delay(1);

            //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015
            //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `hsk_dan_tanah_hutan_1960_2015`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString();                                                           //string
                        string nilai001;
                        if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; }
                        else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["Tahun"].ToString();                                                          //string
                        string nilai002;
                        if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; }
                        else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["HSK_Diwarta"].ToString();                                                  //decimal
                        decimal nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = 0; }
                        else { try { nilai003 = decimal.Parse(nilai003x); } catch (Exception) { nilai003 = 0; } }
                        string nilai004x = reader["HSK_Cadangan"].ToString();                                                  //decimal
                        decimal nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = 0; }
                        else { try { nilai004 = decimal.Parse(nilai004x); } catch (Exception) { nilai004 = 0; } }
                        string nilai005x = reader["RHL_DalamHSK"].ToString();                                                  //decimal
                        decimal nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = 0; }
                        else { try { nilai005 = decimal.Parse(nilai005x); } catch (Exception) { nilai005 = 0; } }
                        string nilai006x = reader["RHL_LuarHSK"].ToString();                                                  //decimal
                        decimal nilai006;
                        if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; }
                        else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["HutanTanahKerajaan"].ToString();                                                  //decimal
                        decimal nilai007;
                        if (string.IsNullOrEmpty(nilai007x)) { nilai007 = 0; }
                        else { try { nilai007 = decimal.Parse(nilai007x); } catch (Exception) { nilai007 = 0; } }
                        string nilai008x = reader["LainLainRizab"].ToString();                                                  //decimal
                        decimal nilai008;
                        if (string.IsNullOrEmpty(nilai008x)) { nilai008 = 0; }
                        else { try { nilai008 = decimal.Parse(nilai008x); } catch (Exception) { nilai008 = 0; } }
                        string nilai009x = reader["KodNegeri"].ToString();                                                          //string
                        string nilai009;
                        if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; }
                        else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[hsk_dan_tanah_hutan_1960_2015] WHERE [hskdantanahhutan19602015_recordid] = @hskdantanahhutan19602015_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["hskdantanahhutan19602015_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }

                                    string checknilai002x = readerCheck01["hskdantanahhutan19602015_tahun"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }

                                    string checknilai003x = readerCheck01["hskdantanahhutan19602015_hsk_diwarta"].ToString();                                                  //decimal
                                    decimal checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = 0; }
                                    else { try { checknilai003 = decimal.Parse(checknilai003x); } catch (Exception) { checknilai003 = 0; } }

                                    string checknilai004x = readerCheck01["hskdantanahhutan19602015_hsk_cadangan"].ToString();                                                  //decimal
                                    decimal checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = 0; }
                                    else { try { checknilai004 = decimal.Parse(checknilai004x); } catch (Exception) { checknilai004 = 0; } }

                                    string checknilai005x = readerCheck01["hskdantanahhutan19602015_rhl_dalamhsk"].ToString();                                                  //decimal
                                    decimal checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = 0; }
                                    else { try { checknilai005 = decimal.Parse(checknilai005x); } catch (Exception) { checknilai005 = 0; } }

                                    string checknilai006x = readerCheck01["hskdantanahhutan19602015_rhl_luarhsk"].ToString();                                                  //decimal
                                    decimal checknilai006;
                                    if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; }
                                    else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }

                                    string checknilai007x = readerCheck01["hskdantanahhutan19602015_hutantanahkerajaan"].ToString();                                                  //decimal
                                    decimal checknilai007;
                                    if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = 0; }
                                    else { try { checknilai007 = decimal.Parse(checknilai007x); } catch (Exception) { checknilai007 = 0; } }

                                    string checknilai008x = readerCheck01["hskdantanahhutan19602015_lainlainrizab"].ToString();                                                  //decimal
                                    decimal checknilai008;
                                    if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = 0; }
                                    else { try { checknilai008 = decimal.Parse(checknilai008x); } catch (Exception) { checknilai008 = 0; } }

                                    string checknilai009x = readerCheck01["hskdantanahhutan19602015_kodnegeri"].ToString();                                                          //string
                                    string checknilai009;
                                    if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; }
                                    else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }



                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[hsk_dan_tanah_hutan_1960_2015] SET [hskdantanahhutan19602015_tahun] = @hskdantanahhutan19602015_tahun, [hskdantanahhutan19602015_hsk_diwarta] = @hskdantanahhutan19602015_hsk_diwarta, [hskdantanahhutan19602015_hsk_cadangan] = @hskdantanahhutan19602015_hsk_cadangan, [hskdantanahhutan19602015_rhl_dalamhsk] = @hskdantanahhutan19602015_rhl_dalamhsk, [hskdantanahhutan19602015_rhl_luarhsk] = @hskdantanahhutan19602015_rhl_luarhsk, [hskdantanahhutan19602015_hutantanahkerajaan] = @hskdantanahhutan19602015_hutantanahkerajaan, [hskdantanahhutan19602015_lainlainrizab] = @hskdantanahhutan19602015_lainlainrizab, [hskdantanahhutan19602015_kodnegeri] = @hskdantanahhutan19602015_kodnegeri WHERE [hskdantanahhutan19602015_recordid] = @hskdantanahhutan19602015_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_tahun", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_tahun", nilai002); }
                                            if (nilai003 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_diwarta", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_diwarta", nilai003); }
                                            if (nilai004 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_cadangan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_cadangan", nilai004); }
                                            if (nilai005 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_dalamhsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_dalamhsk", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_luarhsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_luarhsk", nilai006); }
                                            if (nilai007 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_hutantanahkerajaan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_hutantanahkerajaan", nilai007); }
                                            if (nilai008 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_lainlainrizab", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_lainlainrizab", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_kodnegeri", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskdantanahhutan19602015_kodnegeri", nilai009); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[hsk_dan_tanah_hutan_1960_2015] SET [hskdantanahhutan19602015_date_update] = @hskdantanahhutan19602015_date_update,[hskdantanahhutan19602015_date_view] = @hskdantanahhutan19602015_date_view WHERE [hskdantanahhutan19602015_recordid] = @hskdantanahhutan19602015_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            //MessageBox.Show("SML - Update - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[hsk_dan_tanah_hutan_1960_2015] SET [hskdantanahhutan19602015_date_view] = @hskdantanahhutan19602015_date_view WHERE [hskdantanahhutan19602015_recordid] = @hskdantanahhutan19602015_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[hsk_dan_tanah_hutan_1960_2015] ([hskdantanahhutan19602015_recordid], [hskdantanahhutan19602015_tahun], [hskdantanahhutan19602015_hsk_diwarta], [hskdantanahhutan19602015_hsk_cadangan], [hskdantanahhutan19602015_rhl_dalamhsk], [hskdantanahhutan19602015_rhl_luarhsk], [hskdantanahhutan19602015_hutantanahkerajaan], [hskdantanahhutan19602015_lainlainrizab], [hskdantanahhutan19602015_kodnegeri] ) VALUES( @hskdantanahhutan19602015_recordid, @hskdantanahhutan19602015_tahun, @hskdantanahhutan19602015_hsk_diwarta, @hskdantanahhutan19602015_hsk_cadangan, @hskdantanahhutan19602015_rhl_dalamhsk, @hskdantanahhutan19602015_rhl_luarhsk, @hskdantanahhutan19602015_hutantanahkerajaan, @hskdantanahhutan19602015_lainlainrizab, @hskdantanahhutan19602015_kodnegeri);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_tahun", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_tahun", nilai002); }
                                    if (nilai003 == 0) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_diwarta", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_diwarta", nilai003); }
                                    if (nilai004 == 0) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_cadangan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_hsk_cadangan", nilai004); }
                                    if (nilai005 == 0) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_dalamhsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_dalamhsk", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_luarhsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_rhl_luarhsk", nilai006); }
                                    if (nilai007 == 0) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_hutantanahkerajaan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_hutantanahkerajaan", nilai007); }
                                    if (nilai008 == 0) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_lainlainrizab", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_lainlainrizab", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_kodnegeri", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskdantanahhutan19602015_kodnegeri", nilai009); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[hsk_dan_tanah_hutan_1960_2015] SET [hskdantanahhutan19602015_date_add] = @hskdantanahhutan19602015_date_add,[hskdantanahhutan19602015_date_view] = @hskdantanahhutan19602015_date_view WHERE [hskdantanahhutan19602015_recordid] = @hskdantanahhutan19602015_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@hskdantanahhutan19602015_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "hsk_dan_tanah_hutan_1960_2015", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-hsk_dan_tanah_hutan_1960_2015-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();

                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_dan_tanah_hutan_1960_2015", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015
            //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015 //controller_eSmashHSKDanTanahHutan19602015

            await Task.Delay(1);

            //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015
            //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `hsk_mengikut_jenis_hutan_1960_2015`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString();                                                           //string
                        string nilai001;
                        if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; }
                        else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["Tahun"].ToString();                                                          //string
                        string nilai002;
                        if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; }
                        else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["HutanDarat"].ToString();                                                  //decimal
                        decimal nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = 0; }
                        else { try { nilai003 = decimal.Parse(nilai003x); } catch (Exception) { nilai003 = 0; } }
                        string nilai004x = reader["HutanPayaGambut"].ToString();                                                  //decimal
                        decimal nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = 0; }
                        else { try { nilai004 = decimal.Parse(nilai004x); } catch (Exception) { nilai004 = 0; } }
                        string nilai005x = reader["HutanPayaLaut"].ToString();                                                  //decimal
                        decimal nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = 0; }
                        else { try { nilai005 = decimal.Parse(nilai005x); } catch (Exception) { nilai005 = 0; } }
                        string nilai006x = reader["LadangHutan"].ToString();                                                  //decimal
                        decimal nilai006;
                        if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; }
                        else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["KodNegeri"].ToString();                                                          //string
                        string nilai007;
                        if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; }
                        else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[hsk_mengikut_jenis_hutan_1960_2015] WHERE [hskmengikutjenishutan19602015_recordid] = @hskmengikutjenishutan19602015_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["hskmengikutjenishutan19602015_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["hskmengikutjenishutan19602015_tahun"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }

                                    string checknilai003x = readerCheck01["hskmengikutjenishutan19602015_hutandarat"].ToString();                                                  //decimal
                                    decimal checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = 0; }
                                    else { try { checknilai003 = decimal.Parse(checknilai003x); } catch (Exception) { checknilai003 = 0; } }
                                    string checknilai004x = readerCheck01["hskmengikutjenishutan19602015_hutanpayagambut"].ToString();                                                  //decimal
                                    decimal checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = 0; }
                                    else { try { checknilai004 = decimal.Parse(checknilai004x); } catch (Exception) { checknilai004 = 0; } }
                                    string checknilai005x = readerCheck01["hskmengikutjenishutan19602015_hutanpayalaut"].ToString();                                                  //decimal
                                    decimal checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = 0; }
                                    else { try { checknilai005 = decimal.Parse(checknilai005x); } catch (Exception) { checknilai005 = 0; } }
                                    string checknilai006x = readerCheck01["hskmengikutjenishutan19602015_ladanghutan"].ToString();                                                  //decimal
                                    decimal checknilai006;
                                    if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; }
                                    else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }

                                    string checknilai007x = readerCheck01["hskmengikutjenishutan19602015_kodnegeri"].ToString();                                                          //string
                                    string checknilai007;
                                    if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; }
                                    else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }



                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[hsk_mengikut_jenis_hutan_1960_2015] SET [hskmengikutjenishutan19602015_tahun] = @hskmengikutjenishutan19602015_tahun, [hskmengikutjenishutan19602015_hutandarat] = @hskmengikutjenishutan19602015_hutandarat, [hskmengikutjenishutan19602015_hutanpayagambut] = @hskmengikutjenishutan19602015_hutanpayagambut, [hskmengikutjenishutan19602015_hutanpayalaut] = @hskmengikutjenishutan19602015_hutanpayalaut, [hskmengikutjenishutan19602015_ladanghutan] = @hskmengikutjenishutan19602015_ladanghutan, [hskmengikutjenishutan19602015_kodnegeri] = @hskmengikutjenishutan19602015_kodnegeri WHERE [hskmengikutjenishutan19602015_recordid] = @hskmengikutjenishutan19602015_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_tahun", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_tahun", nilai002); }

                                            if (nilai003 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutandarat", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutandarat", nilai003); }
                                            if (nilai004 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayagambut", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayagambut", nilai004); }
                                            if (nilai005 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayalaut", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayalaut", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_ladanghutan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_ladanghutan", nilai006); }

                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_kodnegeri", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_kodnegeri", nilai007); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[hsk_mengikut_jenis_hutan_1960_2015] SET [hskmengikutjenishutan19602015_date_update] = @hskmengikutjenishutan19602015_date_update,[hskmengikutjenishutan19602015_date_view] = @hskmengikutjenishutan19602015_date_view WHERE [hskmengikutjenishutan19602015_recordid] = @hskmengikutjenishutan19602015_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            //MessageBox.Show("SML - Update - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[hsk_mengikut_jenis_hutan_1960_2015] SET [hskmengikutjenishutan19602015_date_view] = @hskmengikutjenishutan19602015_date_view WHERE [hskmengikutjenishutan19602015_recordid] = @hskmengikutjenishutan19602015_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[hsk_mengikut_jenis_hutan_1960_2015] ([hskmengikutjenishutan19602015_recordid], [hskmengikutjenishutan19602015_tahun], [hskmengikutjenishutan19602015_hutandarat], [hskmengikutjenishutan19602015_hutanpayagambut], [hskmengikutjenishutan19602015_hutanpayalaut], [hskmengikutjenishutan19602015_ladanghutan], [hskmengikutjenishutan19602015_kodnegeri]) VALUES( @hskmengikutjenishutan19602015_recordid, @hskmengikutjenishutan19602015_tahun, @hskmengikutjenishutan19602015_hutandarat, @hskmengikutjenishutan19602015_hutanpayagambut, @hskmengikutjenishutan19602015_hutanpayalaut, @hskmengikutjenishutan19602015_ladanghutan, @hskmengikutjenishutan19602015_kodnegeri);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_tahun", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_tahun", nilai002); }

                                    if (nilai003 == 0) { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutandarat", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutandarat", nilai003); }
                                    if (nilai004 == 0) { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayagambut", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayagambut", nilai004); }
                                    if (nilai005 == 0) { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayalaut", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_hutanpayalaut", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_ladanghutan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_ladanghutan", nilai006); }

                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_kodnegeri", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hskmengikutjenishutan19602015_kodnegeri", nilai007); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[hsk_mengikut_jenis_hutan_1960_2015] SET [hskmengikutjenishutan19602015_date_add] = @hskmengikutjenishutan19602015_date_add,[hskmengikutjenishutan19602015_date_view] = @hskmengikutjenishutan19602015_date_view WHERE [hskmengikutjenishutan19602015_recordid] = @hskmengikutjenishutan19602015_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@hskmengikutjenishutan19602015_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "hsk_mengikut_jenis_hutan_1960_2015", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-hsk_mengikut_jenis_hutan_1960_2015-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hsk_mengikut_jenis_hutan_1960_2015", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015
            //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015 //controller_eSmashHSKMengikutJenisHutan19602015

            await Task.Delay(1);

            //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal
            //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `hutan_simpanan_kekal`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString();                                                           //string
                        string nilai001;
                        if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; }
                        else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NamaHSK"].ToString();                                                          //string
                        string nilai002;
                        if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; }
                        else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }

                        string nilai003x = reader["KodNegeri"].ToString();                                                          //string
                        string nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; }
                        else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodDaerah"].ToString();                                                          //string
                        string nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; }
                        else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodHSK"].ToString();                                                          //string
                        string nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; }
                        else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }

                        string nilai006x = reader["KeluasanHutan"].ToString();                                                  //decimal
                        decimal nilai006;
                        if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; }
                        else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[hutan_simpanan_kekal] WHERE [hutansimpanankekal_recordid] = @hutansimpanankekal_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@hutansimpanankekal_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["hutansimpanankekal_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["hutansimpanankekal_namahsk"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }

                                    string checknilai003x = readerCheck01["hutansimpanankekal_kodnegeri"].ToString();                                                          //string
                                    string checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; }
                                    else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["hutansimpanankekal_koddaerah"].ToString();                                                          //string
                                    string checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; }
                                    else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["hutansimpanankekal_kodhsk"].ToString();                                                          //string
                                    string checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; }
                                    else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }

                                    string checknilai006x = readerCheck01["hutansimpanankekal_keluasanhutan"].ToString();                                                  //decimal
                                    decimal checknilai006;
                                    if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; }
                                    else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }



                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[hutan_simpanan_kekal] SET [hutansimpanankekal_namahsk] = @hutansimpanankekal_namahsk, [hutansimpanankekal_kodnegeri] = @hutansimpanankekal_kodnegeri, [hutansimpanankekal_koddaerah] = @hutansimpanankekal_koddaerah, [hutansimpanankekal_kodhsk] = @hutansimpanankekal_kodhsk, [hutansimpanankekal_keluasanhutan] = @hutansimpanankekal_keluasanhutan WHERE [hutansimpanankekal_recordid] = @hutansimpanankekal_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_recordid", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_namahsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_namahsk", nilai002); }

                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_kodnegeri", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_kodnegeri", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_koddaerah", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_koddaerah", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_kodhsk", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_kodhsk", nilai005); }

                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_keluasanhutan", DBNull.Value); }
                                            else { cmdUpdate01.Parameters.AddWithValue("@hutansimpanankekal_keluasanhutan", nilai006); }


                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[hutan_simpanan_kekal] SET [hutansimpanankekal_date_update] = @hutansimpanankekal_date_update,[hutansimpanankekal_date_view] = @hutansimpanankekal_date_view WHERE [hutansimpanankekal_recordid] = @hutansimpanankekal_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-hutan_simpanan_kekal-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-hutan_simpanan_kekal-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-hutan_simpanan_kekal-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            //MessageBox.Show("SML - Update - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[hutan_simpanan_kekal] SET [hutansimpanankekal_date_view] = @hutansimpanankekal_date_view WHERE [hutansimpanankekal_recordid] = @hutansimpanankekal_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-hutan_simpanan_kekal-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-hutan_simpanan_kekal-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[hutan_simpanan_kekal] ([hutansimpanankekal_recordid], [hutansimpanankekal_namahsk], [hutansimpanankekal_kodnegeri], [hutansimpanankekal_koddaerah], [hutansimpanankekal_kodhsk], [hutansimpanankekal_keluasanhutan]) VALUES( @hutansimpanankekal_recordid, @hutansimpanankekal_namahsk, @hutansimpanankekal_kodnegeri, @hutansimpanankekal_koddaerah, @hutansimpanankekal_kodhsk, @hutansimpanankekal_keluasanhutan);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_recordid", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_namahsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_namahsk", nilai002); }

                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_kodnegeri", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_kodnegeri", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_koddaerah", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_koddaerah", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_kodhsk", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_kodhsk", nilai005); }

                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_keluasanhutan", DBNull.Value); }
                                    else { cmdInsert01.Parameters.AddWithValue("@hutansimpanankekal_keluasanhutan", nilai006); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[hutan_simpanan_kekal] SET [hutansimpanankekal_date_add] = @hutansimpanankekal_date_add,[hutansimpanankekal_date_view] = @hutansimpanankekal_date_view WHERE [hutansimpanankekal_recordid] = @hutansimpanankekal_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@hutansimpanankekal_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-hutan_simpanan_kekal-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-hutan_simpanan_kekal-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-hutan_simpanan_kekal-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-hutan_simpanan_kekal-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-hutan_simpanan_kekal-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-hutan_simpanan_kekal-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "hutan_simpanan_kekal", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-hutan_simpanan_kekal-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "hutan_simpanan_kekal", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal
            //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal //controller_eSmashHutanSimpananKekal

            await Task.Delay(1);

            //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10
            //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_10`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString();                                                           //string
                        string nilai001;
                        if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; }
                        else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoKP"].ToString();                                                          //string
                        string nilai002;
                        if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; }
                        else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["IPAddress"].ToString();                                                          //string
                        string nilai003;
                        if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; }
                        else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["NamaNegeri"].ToString();                                                          //string
                        string nilai004;
                        if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; }
                        else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["Tarikh"].ToString();                                                          //string
                        string nilai005;
                        if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; }
                        else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }

                        string nilai006x = reader["HSK_Diwarta"].ToString();                                                  //decimal
                        decimal nilai006;
                        if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; }
                        else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["HSK_Cadangan"].ToString();                                                  //decimal
                        decimal nilai007;
                        if (string.IsNullOrEmpty(nilai007x)) { nilai007 = 0; }
                        else { try { nilai007 = decimal.Parse(nilai007x); } catch (Exception) { nilai007 = 0; } }
                        string nilai008x = reader["HTHL_RHL_LuarHSK"].ToString();                                                  //decimal
                        decimal nilai008;
                        if (string.IsNullOrEmpty(nilai008x)) { nilai008 = 0; }
                        else { try { nilai008 = decimal.Parse(nilai008x); } catch (Exception) { nilai008 = 0; } }
                        string nilai009x = reader["HTHL_RHL_DalamHSK"].ToString();                                                  //decimal
                        decimal nilai009;
                        if (string.IsNullOrEmpty(nilai009x)) { nilai009 = 0; }
                        else { try { nilai009 = decimal.Parse(nilai009x); } catch (Exception) { nilai009 = 0; } }
                        string nilai010x = reader["HTHL_TNas_LuarHSK"].ToString();                                                  //decimal
                        decimal nilai010;
                        if (string.IsNullOrEmpty(nilai010x)) { nilai010 = 0; }
                        else { try { nilai010 = decimal.Parse(nilai010x); } catch (Exception) { nilai010 = 0; } }
                        string nilai011x = reader["HTHL_TNas_DalamHSK"].ToString();                                                  //decimal
                        decimal nilai011;
                        if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; }
                        else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["TNeg_LuarHSK"].ToString();                                                  //decimal
                        decimal nilai012;
                        if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; }
                        else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["TNeg_DalamHSK"].ToString();                                                  //decimal
                        decimal nilai013;
                        if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; }
                        else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["HutanTanahKerajaan"].ToString();                                                  //decimal
                        decimal nilai014;
                        if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; }
                        else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["LainRezabBerhutan"].ToString();                                                  //decimal
                        decimal nilai015;
                        if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; }
                        else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }

                        string nilai016x = reader["KodNegeri"].ToString();                                                          //string
                        string nilai016;
                        if (string.IsNullOrEmpty(nilai016x)) { nilai016 = null; }
                        else { try { nilai016 = nilai016x.ToString(); } catch (Exception) { nilai016 = null; } }

                        string nilai017x = reader["KeluasanHutan"].ToString();                                                  //decimal
                        decimal nilai017;
                        if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; }
                        else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }

                        string nilai018x = reader["NoSusunan"].ToString();                                                          //string
                        string nilai018;
                        if (string.IsNullOrEmpty(nilai018x)) { nilai018 = null; }
                        else { try { nilai018 = nilai018x.ToString(); } catch (Exception) { nilai018 = null; } }


                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_10] WHERE [lapuh10_recordid] = @lapuh10_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh10_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {
                                    string checknilai001x = readerCheck01["lapuh10_recordid"].ToString();                                                           //string
                                    string checknilai001;
                                    if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; }
                                    else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh10_nokp"].ToString();                                                          //string
                                    string checknilai002;
                                    if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; }
                                    else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh10_ipaddress"].ToString();                                                          //string
                                    string checknilai003;
                                    if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; }
                                    else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh10_namanegeri"].ToString();                                                          //string
                                    string checknilai004;
                                    if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; }
                                    else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh10_tarikh"].ToString();                                                          //string
                                    string checknilai005;
                                    if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; }
                                    else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }

                                    string checknilai006x = readerCheck01["lapuh10_hsk_diwarta"].ToString();                                                  //decimal
                                    decimal checknilai006;
                                    if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; }
                                    else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }
                                    string checknilai007x = readerCheck01["lapuh10_hsk_cadangan"].ToString();                                                  //decimal
                                    decimal checknilai007;
                                    if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = 0; }
                                    else { try { checknilai007 = decimal.Parse(checknilai007x); } catch (Exception) { checknilai007 = 0; } }
                                    string checknilai008x = readerCheck01["lapuh10_hthl_rhl_luarhsk"].ToString();                                                  //decimal
                                    decimal checknilai008;
                                    if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = 0; }
                                    else { try { checknilai008 = decimal.Parse(checknilai008x); } catch (Exception) { checknilai008 = 0; } }
                                    string checknilai009x = readerCheck01["lapuh10_hthl_rhl_Dalamhsk"].ToString();                                                  //decimal
                                    decimal checknilai009;
                                    if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = 0; }
                                    else { try { checknilai009 = decimal.Parse(checknilai009x); } catch (Exception) { checknilai009 = 0; } }
                                    string checknilai0010x = readerCheck01["lapuh10_hthl_tnas_Luarhsk"].ToString();                                                  //decimal
                                    decimal checknilai0010;
                                    if (string.IsNullOrEmpty(checknilai0010x)) { checknilai0010 = 0; }
                                    else { try { checknilai0010 = decimal.Parse(checknilai0010x); } catch (Exception) { checknilai0010 = 0; } }
                                    string checknilai011x = readerCheck01["lapuh10_hthl_tnas_Dalamhsk"].ToString();                                                  //decimal
                                    decimal checknilai011;
                                    if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; }
                                    else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["lapuh10_tneg_luarhsk"].ToString();                                                  //decimal
                                    decimal checknilai012;
                                    if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; }
                                    else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["lapuh10_tneg_dalamhsk"].ToString();                                                  //decimal
                                    decimal checknilai013;
                                    if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; }
                                    else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["lapuh10_hutantanahkerajaan"].ToString();                                                  //decimal
                                    decimal checknilai014;
                                    if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; }
                                    else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["lapuh10_lainrezabberhutan"].ToString();                                                  //decimal
                                    decimal checknilai015;
                                    if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; }
                                    else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }

                                    string checknilai016x = readerCheck01["lapuh10_kodnegeri"].ToString();                                                          //string
                                    string checknilai016;
                                    if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = null; }
                                    else { try { checknilai016 = checknilai016x.ToString(); } catch (Exception) { checknilai016 = null; } }

                                    string checknilai017x = readerCheck01["lapuh10_keluasanhutan"].ToString();                                                  //decimal
                                    decimal checknilai017;
                                    if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; }
                                    else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }

                                    string checknilai018x = readerCheck01["lapuh10_nosusunan"].ToString();                                                          //string
                                    string checknilai018;
                                    if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = null; }
                                    else { try { checknilai018 = checknilai018x.ToString(); } catch (Exception) { checknilai018 = null; } }



                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_10] SET [lapuh10_nokp]=@lapuh10_nokp, [lapuh10_ipaddress]=@lapuh10_ipaddress, [lapuh10_namanegeri]=@lapuh10_namanegeri, [lapuh10_tarikh]=@lapuh10_tarikh, [lapuh10_hsk_diwarta]=@lapuh10_hsk_diwarta, [lapuh10_hsk_cadangan]=@lapuh10_hsk_cadangan, [lapuh10_hthl_rhl_luarhsk]=@lapuh10_hthl_rhl_luarhsk, [lapuh10_hthl_rhl_Dalamhsk]=@lapuh10_hthl_rhl_Dalamhsk, [lapuh10_hthl_tnas_Luarhsk]=@lapuh10_hthl_tnas_Luarhsk, [lapuh10_hthl_tnas_Dalamhsk]=@lapuh10_hthl_tnas_Dalamhsk, [lapuh10_tneg_luarhsk]=@lapuh10_tneg_luarhsk, [lapuh10_tneg_dalamhsk]=@lapuh10_tneg_dalamhsk, [lapuh10_hutantanahkerajaan]=@lapuh10_hutantanahkerajaan, [lapuh10_lainrezabberhutan]=@lapuh10_lainrezabberhutan, [lapuh10_kodnegeri]=@lapuh10_kodnegeri, [lapuh10_keluasanhutan]=@lapuh10_keluasanhutan, [lapuh10_nosusunan]=@lapuh10_nosusunan WHERE [lapuh10_recordid]=@lapuh10_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_nokp", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_ipaddress", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_namanegeri", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_tarikh", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_tarikh", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hsk_diwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hsk_diwarta", nilai006); }
                                            if (nilai007 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hsk_cadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hsk_cadangan", nilai007); }
                                            if (nilai008 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_rhl_luarhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_rhl_luarhsk", nilai008); }
                                            if (nilai009 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_rhl_Dalamhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_rhl_Dalamhsk", nilai009); }
                                            if (nilai010 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Luarhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Luarhsk", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Dalamhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Dalamhsk", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_tneg_luarhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_tneg_luarhsk", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_tneg_dalamhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_tneg_dalamhsk", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hutantanahkerajaan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_hutantanahkerajaan", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_lainrezabberhutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_lainrezabberhutan", nilai015); }
                                            if (string.IsNullOrEmpty(nilai016)) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_kodnegeri", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_keluasanhutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_keluasanhutan", nilai017); }
                                            if (string.IsNullOrEmpty(nilai018)) { cmdUpdate01.Parameters.AddWithValue("@lapuh10_nosusunan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh10_nosusunan", nilai018); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_10] SET [lapuh10_date_update] = @lapuh10_date_update,[lapuh10_date_view] = @lapuh10_date_view WHERE [lapuh10_recordid] = @lapuh10_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh10_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh10_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh10_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-maklumat_lesen-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_10-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_10-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_10] SET [lapuh10_date_view] = @lapuh10_date_view WHERE [lapuh10_recordid] = @lapuh10_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh10_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh10_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_10-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_10-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_10] ([lapuh10_recordid], [lapuh10_nokp], [lapuh10_ipaddress],[lapuh10_namanegeri], [lapuh10_tarikh], [lapuh10_hsk_diwarta], [lapuh10_hsk_cadangan], [lapuh10_hthl_rhl_luarhsk], [lapuh10_hthl_rhl_Dalamhsk], [lapuh10_hthl_tnas_Luarhsk], [lapuh10_hthl_tnas_Dalamhsk], [lapuh10_tneg_luarhsk], [lapuh10_tneg_dalamhsk], [lapuh10_hutantanahkerajaan], [lapuh10_lainrezabberhutan], [lapuh10_kodnegeri], [lapuh10_keluasanhutan], [lapuh10_nosusunan]) VALUES(@lapuh10_recordid, @lapuh10_nokp, @lapuh10_ipaddress, @lapuh10_namanegeri, @lapuh10_tarikh, @lapuh10_hsk_diwarta, @lapuh10_hsk_cadangan, @lapuh10_hthl_rhl_luarhsk, @lapuh10_hthl_rhl_Dalamhsk, @lapuh10_hthl_tnas_Luarhsk, @lapuh10_hthl_tnas_Dalamhsk, @lapuh10_tneg_luarhsk, @lapuh10_tneg_dalamhsk, @lapuh10_hutantanahkerajaan, @lapuh10_lainrezabberhutan, @lapuh10_kodnegeri, @lapuh10_keluasanhutan, @lapuh10_nosusunan);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh10_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh10_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_nokp", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh10_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_ipaddress", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh10_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_namanegeri", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh10_tarikh", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_tarikh", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_hsk_diwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_hsk_diwarta", nilai006); }
                                    if (nilai007 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_hsk_cadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_hsk_cadangan", nilai007); }
                                    if (nilai008 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_rhl_luarhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_rhl_luarhsk", nilai008); }
                                    if (nilai009 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_rhl_Dalamhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_rhl_Dalamhsk", nilai009); }
                                    if (nilai010 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Luarhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Luarhsk", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Dalamhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_hthl_tnas_Dalamhsk", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_tneg_luarhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_tneg_luarhsk", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_tneg_dalamhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_tneg_dalamhsk", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_hutantanahkerajaan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_hutantanahkerajaan", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_lainrezabberhutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_lainrezabberhutan", nilai015); }
                                    if (string.IsNullOrEmpty(nilai016)) { cmdInsert01.Parameters.AddWithValue("@lapuh10_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_kodnegeri", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh10_keluasanhutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_keluasanhutan", nilai017); }
                                    if (string.IsNullOrEmpty(nilai018)) { cmdInsert01.Parameters.AddWithValue("@lapuh10_nosusunan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh10_nosusunan", nilai018); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_10] SET [lapuh10_date_add] = @lapuh10_date_add,[lapuh10_date_view] = @lapuh10_date_view WHERE [lapuh10_recordid] = @lapuh10_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh10_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh10_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh10_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_10-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_10-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_10-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_10-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_10-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_10-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_10", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_10-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_10", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10
            //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10 //controller_eSmashLapUh10

            await Task.Delay(1);

            //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11
            //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_11`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoKP"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["IPAddress"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodNegeri"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodDaerah"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["Tahun"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["KodSukuTahun"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["KodHSK"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["TarikhWarta"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["NoPelanWarta"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["JH_01_LuasWarta"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["JH_01_LuasDigital"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["JH_02_LuasWarta"].ToString(); decimal nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; } else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["JH_02_LuasDigital"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["JH_03_LuasWarta"].ToString(); decimal nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; } else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }
                        string nilai016x = reader["JH_03_LuasDigital"].ToString(); decimal nilai016; if (string.IsNullOrEmpty(nilai016x)) { nilai016 = 0; } else { try { nilai016 = decimal.Parse(nilai016x); } catch (Exception) { nilai016 = 0; } }
                        string nilai017x = reader["JH_04_LuasWarta"].ToString(); decimal nilai017; if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; } else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }
                        string nilai018x = reader["JH_04_LuasDigital"].ToString(); decimal nilai018; if (string.IsNullOrEmpty(nilai018x)) { nilai018 = 0; } else { try { nilai018 = decimal.Parse(nilai018x); } catch (Exception) { nilai018 = 0; } }
                        string nilai019x = reader["NamaNegeri"].ToString(); string nilai019; if (string.IsNullOrEmpty(nilai019x)) { nilai019 = null; } else { try { nilai019 = nilai019x.ToString(); } catch (Exception) { nilai019 = null; } }
                        string nilai020x = reader["NoWarta"].ToString(); string nilai020; if (string.IsNullOrEmpty(nilai020x)) { nilai020 = null; } else { try { nilai020 = nilai020x.ToString(); } catch (Exception) { nilai020 = null; } }
                        string nilai021x = reader["NamaHSK"].ToString(); string nilai021; if (string.IsNullOrEmpty(nilai021x)) { nilai021 = null; } else { try { nilai021 = nilai021x.ToString(); } catch (Exception) { nilai021 = null; } }


                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_11] WHERE [lapuh11_recordid] = @lapuh11_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh11_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh11_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh11_nokp"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh11_ipaddress"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh11_kodnegeri"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh11_koddaerah"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh11_tahun"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["lapuh11_kodsukutahun"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["lapuh11_kodhsk"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["lapuh11_tarikhwarta"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["lapuh11_nopelanwarta"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["lapuh11_jh_01_luaswarta"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["lapuh11_jh_01_luasdigital"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["lapuh11_jh_02_luaswarta"].ToString(); decimal checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; } else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["lapuh11_jh_02_luasdigital"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["lapuh11_jh_03_luaswarta"].ToString(); decimal checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; } else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }
                                    string checknilai016x = readerCheck01["lapuh11_jh_03_luasdigital"].ToString(); decimal checknilai016; if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = 0; } else { try { checknilai016 = decimal.Parse(checknilai016x); } catch (Exception) { checknilai016 = 0; } }
                                    string checknilai017x = readerCheck01["lapuh11_jh_04_luaswarta"].ToString(); decimal checknilai017; if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; } else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }
                                    string checknilai018x = readerCheck01["lapuh11_jh_04_luasdigital"].ToString(); decimal checknilai018; if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = 0; } else { try { checknilai018 = decimal.Parse(checknilai018x); } catch (Exception) { checknilai018 = 0; } }
                                    string checknilai019x = readerCheck01["lapuh11_namanegeri"].ToString(); string checknilai019; if (string.IsNullOrEmpty(checknilai019x)) { checknilai019 = null; } else { try { checknilai019 = checknilai019x.ToString(); } catch (Exception) { checknilai019 = null; } }
                                    string checknilai020x = readerCheck01["lapuh11_nowarta"].ToString(); string checknilai020; if (string.IsNullOrEmpty(checknilai020x)) { checknilai020 = null; } else { try { checknilai020 = checknilai020x.ToString(); } catch (Exception) { checknilai020 = null; } }
                                    string checknilai021x = readerCheck01["lapuh11_namahsk"].ToString(); string checknilai021; if (string.IsNullOrEmpty(checknilai021x)) { checknilai021 = null; } else { try { checknilai021 = checknilai021x.ToString(); } catch (Exception) { checknilai021 = null; } }



                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai017 != nilai017 || checknilai018 != nilai018 || checknilai019 != nilai019 || checknilai020 != nilai020 || checknilai021 != nilai021)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_11] SET [lapuh11_nokp]=@lapuh11_nokp,[lapuh11_ipaddress]=@lapuh11_ipaddress,[lapuh11_kodnegeri]=@lapuh11_kodnegeri,[lapuh11_koddaerah]=@lapuh11_koddaerah,[lapuh11_tahun]=@lapuh11_tahun,[lapuh11_kodsukutahun]=@lapuh11_kodsukutahun,[lapuh11_kodhsk]=@lapuh11_kodhsk,[lapuh11_tarikhwarta]=@lapuh11_tarikhwarta,[lapuh11_nopelanwarta]=@lapuh11_nopelanwarta,[lapuh11_jh_01_luaswarta]=@lapuh11_jh_01_luaswarta,[lapuh11_jh_01_luasdigital]=@lapuh11_jh_01_luasdigital,[lapuh11_jh_02_luaswarta]=@lapuh11_jh_02_luaswarta,[lapuh11_jh_02_luasdigital]=@lapuh11_jh_02_luasdigital,[lapuh11_jh_03_luaswarta]=@lapuh11_jh_03_luaswarta,[lapuh11_jh_03_luasdigital]=@lapuh11_jh_03_luasdigital,[lapuh11_jh_04_luaswarta]=@lapuh11_jh_04_luaswarta,[lapuh11_jh_04_luasdigital]=@lapuh11_jh_04_luasdigital,[lapuh11_namanegeri]=@lapuh11_namanegeri,[lapuh11_nowarta]=@lapuh11_nowarta,[lapuh11_namahsk]=@lapuh11_namahsk WHERE [lapuh11_recordid]=@lapuh11_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_nokp", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_ipaddress", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_kodnegeri", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_koddaerah", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_tahun", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_kodsukutahun", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_kodhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_kodhsk", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_tarikhwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_tarikhwarta", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_nopelanwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_nopelanwarta", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_01_luaswarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_01_luaswarta", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_01_luasdigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_01_luasdigital", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_02_luaswarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_02_luaswarta", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_02_luasdigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_02_luasdigital", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_03_luaswarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_03_luaswarta", nilai015); }
                                            if (nilai016 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_03_luasdigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_03_luasdigital", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_04_luaswarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_04_luaswarta", nilai017); }
                                            if (nilai018 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_04_luasdigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_jh_04_luasdigital", nilai018); }
                                            if (string.IsNullOrEmpty(nilai019)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_namanegeri", nilai019); }
                                            if (string.IsNullOrEmpty(nilai020)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_nowarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_nowarta", nilai020); }
                                            if (string.IsNullOrEmpty(nilai021)) { cmdUpdate01.Parameters.AddWithValue("@lapuh11_namahsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh11_namahsk", nilai021); }


                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_11] SET [lapuh11_date_update] = @lapuh11_date_update,[lapuh11_date_view] = @lapuh11_date_view WHERE [lapuh11_recordid] = @lapuh11_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh11_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh11_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh11_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_11-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_11-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_11-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_11] SET [lapuh11_date_view] = @lapuh11_date_view WHERE [lapuh11_recordid] = @lapuh11_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh11_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh11_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_11-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_11-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_11] ([lapuh11_recordid],[lapuh11_nokp],[lapuh11_ipaddress],[lapuh11_kodnegeri],[lapuh11_koddaerah],[lapuh11_tahun],[lapuh11_kodsukutahun],[lapuh11_kodhsk],[lapuh11_tarikhwarta],[lapuh11_nopelanwarta],[lapuh11_jh_01_luaswarta],[lapuh11_jh_01_luasdigital],[lapuh11_jh_02_luaswarta],[lapuh11_jh_02_luasdigital],[lapuh11_jh_03_luaswarta],[lapuh11_jh_03_luasdigital],[lapuh11_jh_04_luaswarta],[lapuh11_jh_04_luasdigital],[lapuh11_namanegeri],[lapuh11_nowarta],[lapuh11_namahsk]) VALUES(@lapuh11_recordid,@lapuh11_nokp,@lapuh11_ipaddress,@lapuh11_kodnegeri,@lapuh11_koddaerah,@lapuh11_tahun,@lapuh11_kodsukutahun,@lapuh11_kodhsk,@lapuh11_tarikhwarta,@lapuh11_nopelanwarta,@lapuh11_jh_01_luaswarta,@lapuh11_jh_01_luasdigital,@lapuh11_jh_02_luaswarta,@lapuh11_jh_02_luasdigital,@lapuh11_jh_03_luaswarta,@lapuh11_jh_03_luasdigital,@lapuh11_jh_04_luaswarta,@lapuh11_jh_04_luasdigital,@lapuh11_namanegeri,@lapuh11_nowarta,@lapuh11_namahsk);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_nokp", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_ipaddress", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_kodnegeri", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_koddaerah", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_tahun", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_kodsukutahun", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_kodhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_kodhsk", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_tarikhwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_tarikhwarta", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_nopelanwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_nopelanwarta", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_01_luaswarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_01_luaswarta", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_01_luasdigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_01_luasdigital", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_02_luaswarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_02_luaswarta", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_02_luasdigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_02_luasdigital", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_03_luaswarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_03_luaswarta", nilai015); }
                                    if (nilai016 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_03_luasdigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_03_luasdigital", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_04_luaswarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_04_luaswarta", nilai017); }
                                    if (nilai018 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_04_luasdigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_jh_04_luasdigital", nilai018); }
                                    if (string.IsNullOrEmpty(nilai019)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_namanegeri", nilai019); }
                                    if (string.IsNullOrEmpty(nilai020)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_nowarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_nowarta", nilai020); }
                                    if (string.IsNullOrEmpty(nilai021)) { cmdInsert01.Parameters.AddWithValue("@lapuh11_namahsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh11_namahsk", nilai021); }


                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_11] SET [lapuh11_date_add] = @lapuh11_date_add,[lapuh11_date_view] = @lapuh11_date_view WHERE [lapuh11_recordid] = @lapuh11_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh11_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh11_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh11_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_11-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_11-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_11-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_11-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());
                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_11-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", null, null, "empty", "empty-nodata", "Tiada Data", null);
                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_11-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());
            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_11", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_11-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_11", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);
            }
            //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11
            //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11 //controller_eSmashLapUh11

            await Task.Delay(1);

            //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20
            //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_20`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoKP"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["IPAddress"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodNegeri"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["Tarikh"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["HSKSediaAda"].ToString(); decimal nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; } else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["CHSKDiwarta"].ToString(); decimal nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = 0; } else { try { nilai007 = decimal.Parse(nilai007x); } catch (Exception) { nilai007 = 0; } }
                        string nilai008x = reader["BakiCHSK"].ToString(); decimal nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = 0; } else { try { nilai008 = decimal.Parse(nilai008x); } catch (Exception) { nilai008 = 0; } }
                        string nilai009x = reader["HSKLulusMansuh"].ToString(); decimal nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = 0; } else { try { nilai009 = decimal.Parse(nilai009x); } catch (Exception) { nilai009 = 0; } }
                        string nilai010x = reader["HSKWartaKeluar"].ToString(); decimal nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = 0; } else { try { nilai010 = decimal.Parse(nilai010x); } catch (Exception) { nilai010 = 0; } }
                        string nilai011x = reader["LuasHSK"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["NamaNegeri"].ToString(); string nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = null; } else { try { nilai012 = nilai012x.ToString(); } catch (Exception) { nilai012 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_20] WHERE [lapuh20_recordid] = @lapuh20_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh20_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh20_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh20_nokp"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh20_ipaddress"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh20_kodnegeri"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh20_tarikh"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh20_hsksediaada"].ToString(); decimal checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; } else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }
                                    string checknilai007x = readerCheck01["lapuh20_chskdiwarta"].ToString(); decimal checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = 0; } else { try { checknilai007 = decimal.Parse(checknilai007x); } catch (Exception) { checknilai007 = 0; } }
                                    string checknilai008x = readerCheck01["lapuh20_bakichsk"].ToString(); decimal checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = 0; } else { try { checknilai008 = decimal.Parse(checknilai008x); } catch (Exception) { checknilai008 = 0; } }
                                    string checknilai009x = readerCheck01["lapuh20_hsklulusmansuh"].ToString(); decimal checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = 0; } else { try { checknilai009 = decimal.Parse(checknilai009x); } catch (Exception) { checknilai009 = 0; } }
                                    string checknilai010x = readerCheck01["lapuh20_hskwartakeluar"].ToString(); decimal checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = 0; } else { try { checknilai010 = decimal.Parse(checknilai010x); } catch (Exception) { checknilai010 = 0; } }
                                    string checknilai011x = readerCheck01["lapuh20_luashsk"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["lapuh20_namanegeri"].ToString(); string checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = null; } else { try { checknilai012 = checknilai012x.ToString(); } catch (Exception) { checknilai012 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_20] SET [lapuh20_nokp]=@lapuh20_nokp,[lapuh20_ipaddress]=@lapuh20_ipaddress,[lapuh20_kodnegeri]=@lapuh20_kodnegeri,[lapuh20_tarikh]=@lapuh20_tarikh,[lapuh20_hsksediaada]=@lapuh20_hsksediaada,[lapuh20_chskdiwarta]=@lapuh20_chskdiwarta,[lapuh20_bakichsk]=@lapuh20_bakichsk,[lapuh20_hsklulusmansuh]=@lapuh20_hsklulusmansuh,[lapuh20_hskwartakeluar]=@lapuh20_hskwartakeluar,[lapuh20_luashsk]=@lapuh20_luashsk,[lapuh20_namanegeri]=@lapuh20_namanegeri WHERE [lapuh20_recordid]=@lapuh20_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_nokp", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_ipaddress", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_kodnegeri", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_tarikh", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_tarikh", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_hsksediaada", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_hsksediaada", nilai006); }
                                            if (nilai007 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_chskdiwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_chskdiwarta", nilai007); }
                                            if (nilai008 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_bakichsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_bakichsk", nilai008); }
                                            if (nilai009 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_hsklulusmansuh", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_hsklulusmansuh", nilai009); }
                                            if (nilai010 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_hskwartakeluar", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_hskwartakeluar", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_luashsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_luashsk", nilai011); }
                                            if (string.IsNullOrEmpty(nilai012)) { cmdUpdate01.Parameters.AddWithValue("@lapuh20_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh20_namanegeri", nilai012); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_20] SET [lapuh20_date_update] = @lapuh20_date_update,[lapuh20_date_view] = @lapuh20_date_view WHERE [lapuh20_recordid] = @lapuh20_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh20_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh20_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh20_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_20-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());
                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_20-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_20-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_20] SET [lapuh20_date_view] = @lapuh20_date_view WHERE [lapuh20_recordid] = @lapuh20_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh20_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh20_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_20-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());
                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_20-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_20] ([lapuh20_recordid],[lapuh20_nokp],[lapuh20_ipaddress],[lapuh20_kodnegeri],[lapuh20_tarikh],[lapuh20_hsksediaada],[lapuh20_chskdiwarta],[lapuh20_bakichsk],[lapuh20_hsklulusmansuh],[lapuh20_hskwartakeluar],[lapuh20_luashsk],[lapuh20_namanegeri]) VALUES(@lapuh20_recordid,@lapuh20_nokp,@lapuh20_ipaddress,@lapuh20_kodnegeri,@lapuh20_tarikh,@lapuh20_hsksediaada,@lapuh20_chskdiwarta,@lapuh20_bakichsk,@lapuh20_hsklulusmansuh,@lapuh20_hskwartakeluar,@lapuh20_luashsk,@lapuh20_namanegeri);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh20_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh20_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_nokp", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh20_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_ipaddress", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh20_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_kodnegeri", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh20_tarikh", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_tarikh", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh20_hsksediaada", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_hsksediaada", nilai006); }
                                    if (nilai007 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh20_chskdiwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_chskdiwarta", nilai007); }
                                    if (nilai008 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh20_bakichsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_bakichsk", nilai008); }
                                    if (nilai009 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh20_hsklulusmansuh", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_hsklulusmansuh", nilai009); }
                                    if (nilai010 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh20_hskwartakeluar", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_hskwartakeluar", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh20_luashsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_luashsk", nilai011); }
                                    if (string.IsNullOrEmpty(nilai012)) { cmdInsert01.Parameters.AddWithValue("@lapuh20_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh20_namanegeri", nilai012); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_20] SET [lapuh20_date_add] = @lapuh20_date_add,[lapuh20_date_view] = @lapuh20_date_view WHERE [lapuh20_recordid] = @lapuh20_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh20_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh20_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh20_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_20-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());
                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    ///listBoxEsmashStatus.Items.Add("eSmash-lap_uh_20-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_20-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());
                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_20-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", null, "nodata", "nodata");
                    ///listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_20-Main-No Data!");
                    ///
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_20-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_20", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_20-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_20", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20
            //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20 //controller_eSmashLapUh20

            await Task.Delay(1);

            //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21
            //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_21`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodDaerah"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodHSK"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["NoKP"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["IPAddress"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["KodJenisHutan"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["Tahun"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["KodSukuTahun"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }


                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_21] WHERE [lapuh21_recordid] = @lapuh21_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh21_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh21_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh21_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh21_koddaerah"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh21_kodhsk"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh21_nokp"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh21_ipaddress"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["lapuh21_kodjenishutan"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["lapuh21_tahun"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["lapuh21_kodsukutahun"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_21] SET [lapuh21_kodnegeri]=@lapuh21_kodnegeri,[lapuh21_koddaerah]=@lapuh21_koddaerah,[lapuh21_kodhsk]=@lapuh21_kodhsk,[lapuh21_nokp]=@lapuh21_nokp,[lapuh21_ipaddress]=@lapuh21_ipaddress,[lapuh21_kodjenishutan]=@lapuh21_kodjenishutan,[lapuh21_tahun]=@lapuh21_tahun,[lapuh21_kodsukutahun]=@lapuh21_kodsukutahun WHERE [lapuh21_recordid]=@lapuh21_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_koddaerah", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodhsk", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_nokp", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_ipaddress", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodjenishutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodjenishutan", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_tahun", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21_kodsukutahun", nilai009); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_21] SET [lapuh21_date_update] = @lapuh21_date_update,[lapuh21_date_view] = @lapuh21_date_view WHERE [lapuh21_recordid] = @lapuh21_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_21-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_21] SET [lapuh21_date_view] = @lapuh21_date_view WHERE [lapuh21_recordid] = @lapuh21_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh21_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh21_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_21] ([lapuh21_recordid],[lapuh21_kodnegeri],[lapuh21_koddaerah],[lapuh21_kodhsk],[lapuh21_nokp],[lapuh21_ipaddress],[lapuh21_kodjenishutan],[lapuh21_tahun],[lapuh21_kodsukutahun]) VALUES(@lapuh21_recordid,@lapuh21_kodnegeri,@lapuh21_koddaerah,@lapuh21_kodhsk,@lapuh21_nokp,@lapuh21_ipaddress,@lapuh21_kodjenishutan,@lapuh21_tahun,@lapuh21_kodsukutahun);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_koddaerah", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodhsk", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_nokp", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_ipaddress", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodjenishutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodjenishutan", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_tahun", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21_kodsukutahun", nilai009); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_21] SET [lapuh21_date_add] = @lapuh21_date_add,[lapuh21_date_view] = @lapuh21_date_view WHERE [lapuh21_recordid] = @lapuh21_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_21-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_21-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_21-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_21-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_21-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21
            //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21 //controller_eSmashLapUh21

            await Task.Delay(1);

            //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a
            //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_21a`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["NamaNegeri"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodDaerah"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodHSK"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["Tahun"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["KodSukuTahun"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["KodJenisHutan"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["WARTA_Kompartment"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["WARTA_TarikhWarta"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["WARTA_NoWarta"].ToString(); string nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = null; } else { try { nilai011 = nilai011x.ToString(); } catch (Exception) { nilai011 = null; } }
                        string nilai012x = reader["WARTA_LuasDikeluarkan"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["WARTA_Tujuan"].ToString(); string nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = null; } else { try { nilai013 = nilai013x.ToString(); } catch (Exception) { nilai013 = null; } }
                        string nilai014x = reader["NoKP"].ToString(); string nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = null; } else { try { nilai014 = nilai014x.ToString(); } catch (Exception) { nilai014 = null; } }
                        string nilai015x = reader["IPAddress"].ToString(); string nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = null; } else { try { nilai015 = nilai015x.ToString(); } catch (Exception) { nilai015 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_21a] WHERE [lapuh21a_recordid] = @lapuh21a_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh21a_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh21a_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh21a_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh21a_namanegeri"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh21a_koddaerah"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh21a_kodhsk"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh21a_tahun"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["lapuh21a_kodsukutahun"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["lapuh21a_kodjenishutan"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["lapuh21a_warta_kompartment"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["lapuh21a_warta_tarikhwarta"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["lapuh21a_warta_nowarta"].ToString(); string checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = null; } else { try { checknilai011 = checknilai011x.ToString(); } catch (Exception) { checknilai011 = null; } }
                                    string checknilai012x = readerCheck01["lapuh21a_warta_luasdikeluarkan"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["lapuh21a_warta_tujuan"].ToString(); string checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = null; } else { try { checknilai013 = checknilai013x.ToString(); } catch (Exception) { checknilai013 = null; } }
                                    string checknilai014x = readerCheck01["lapuh21a_nokp"].ToString(); string checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = null; } else { try { checknilai014 = checknilai014x.ToString(); } catch (Exception) { checknilai014 = null; } }
                                    string checknilai015x = readerCheck01["lapuh21a_ipaddress"].ToString(); string checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = null; } else { try { checknilai015 = checknilai015x.ToString(); } catch (Exception) { checknilai015 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_21a] SET [lapuh21a_kodnegeri]=@lapuh21a_kodnegeri,[lapuh21a_namanegeri]=@lapuh21a_namanegeri,[lapuh21a_koddaerah]=@lapuh21a_koddaerah,[lapuh21a_kodhsk]=@lapuh21a_kodhsk,[lapuh21a_tahun]=@lapuh21a_tahun,[lapuh21a_kodsukutahun]=@lapuh21a_kodsukutahun,[lapuh21a_kodjenishutan]=@lapuh21a_kodjenishutan,[lapuh21a_warta_kompartment]=@lapuh21a_warta_kompartment,[lapuh21a_warta_tarikhwarta]=@lapuh21a_warta_tarikhwarta,[lapuh21a_warta_nowarta]=@lapuh21a_warta_nowarta,[lapuh21a_warta_luasdikeluarkan]=@lapuh21a_warta_luasdikeluarkan,[lapuh21a_warta_tujuan]=@lapuh21a_warta_tujuan,[lapuh21a_nokp]=@lapuh21a_nokp,[lapuh21a_ipaddress]=@lapuh21a_ipaddress WHERE [lapuh21a_recordid]=@lapuh21a_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_namanegeri", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_koddaerah", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodhsk", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_tahun", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodsukutahun", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodjenishutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_kodjenishutan", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_kompartment", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_kompartment", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_tarikhwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_tarikhwarta", nilai010); }
                                            if (string.IsNullOrEmpty(nilai011)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_nowarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_nowarta", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_luasdikeluarkan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_luasdikeluarkan", nilai012); }
                                            if (string.IsNullOrEmpty(nilai013)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_tujuan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_warta_tujuan", nilai013); }
                                            if (string.IsNullOrEmpty(nilai014)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_nokp", nilai014); }
                                            if (string.IsNullOrEmpty(nilai015)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21a_ipaddress", nilai015); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_21a] SET [lapuh21a_date_update] = @lapuh21a_date_update,[lapuh21a_date_view] = @lapuh21a_date_view WHERE [lapuh21a_recordid] = @lapuh21a_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21a_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21a_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21a_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_21a-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21a-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21a-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_21a] SET [lapuh21a_date_view] = @lapuh21a_date_view WHERE [lapuh21a_recordid] = @lapuh21a_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh21a_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh21a_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21a-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21a-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_21a] ([lapuh21a_recordid],[lapuh21a_kodnegeri],[lapuh21a_namanegeri],[lapuh21a_koddaerah],[lapuh21a_kodhsk],[lapuh21a_tahun],[lapuh21a_kodsukutahun],[lapuh21a_kodjenishutan],[lapuh21a_warta_kompartment],[lapuh21a_warta_tarikhwarta],[lapuh21a_warta_nowarta],[lapuh21a_warta_luasdikeluarkan],[lapuh21a_warta_tujuan],[lapuh21a_nokp],[lapuh21a_ipaddress]) VALUES(@lapuh21a_recordid,@lapuh21a_kodnegeri,@lapuh21a_namanegeri,@lapuh21a_koddaerah,@lapuh21a_kodhsk,@lapuh21a_tahun,@lapuh21a_kodsukutahun,@lapuh21a_kodjenishutan,@lapuh21a_warta_kompartment,@lapuh21a_warta_tarikhwarta,@lapuh21a_warta_nowarta,@lapuh21a_warta_luasdikeluarkan,@lapuh21a_warta_tujuan,@lapuh21a_nokp,@lapuh21a_ipaddress);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_namanegeri", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_koddaerah", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodhsk", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_tahun", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodsukutahun", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodjenishutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_kodjenishutan", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_kompartment", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_kompartment", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_tarikhwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_tarikhwarta", nilai010); }
                                    if (string.IsNullOrEmpty(nilai011)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_nowarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_nowarta", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_luasdikeluarkan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_luasdikeluarkan", nilai012); }
                                    if (string.IsNullOrEmpty(nilai013)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_tujuan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_warta_tujuan", nilai013); }
                                    if (string.IsNullOrEmpty(nilai014)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_nokp", nilai014); }
                                    if (string.IsNullOrEmpty(nilai015)) { cmdInsert01.Parameters.AddWithValue("@lapuh21a_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21a_ipaddress", nilai015); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_21a] SET [lapuh21a_date_add] = @lapuh21a_date_add,[lapuh21a_date_view] = @lapuh21a_date_view WHERE [lapuh21a_recordid] = @lapuh21a_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21a_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21a_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21a_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_21a-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21a-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_21a-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21a-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_21a-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_21a-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21a", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_21a-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21a", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a
            //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a //controller_eSmashLapUh21a

            await Task.Delay(1);

            //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b
            //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_21b`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["NamaNegeri"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodDaerah"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["Tahun"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["KodSukuTahun"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["CadanganHSK"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["TarikhMMKN"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["NoRujukanMMKN"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["Catatan"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["JH_01_LSediaAda"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["JH_02_LSediaAda"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["JH_03_LSediaAda"].ToString(); decimal nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; } else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["JH_04_LSediaAda"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["JH_01_LCadangan"].ToString(); decimal nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; } else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }
                        string nilai016x = reader["JH_02_LCadangan"].ToString(); decimal nilai016; if (string.IsNullOrEmpty(nilai016x)) { nilai016 = 0; } else { try { nilai016 = decimal.Parse(nilai016x); } catch (Exception) { nilai016 = 0; } }
                        string nilai017x = reader["JH_03_LCadangan"].ToString(); decimal nilai017; if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; } else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }
                        string nilai018x = reader["JH_04_Lcadangan"].ToString(); decimal nilai018; if (string.IsNullOrEmpty(nilai018x)) { nilai018 = 0; } else { try { nilai018 = decimal.Parse(nilai018x); } catch (Exception) { nilai018 = 0; } }
                        string nilai019x = reader["JenisRekod"].ToString(); string nilai019; if (string.IsNullOrEmpty(nilai019x)) { nilai019 = null; } else { try { nilai019 = nilai019x.ToString(); } catch (Exception) { nilai019 = null; } }
                        string nilai020x = reader["NoKP"].ToString(); string nilai020; if (string.IsNullOrEmpty(nilai020x)) { nilai020 = null; } else { try { nilai020 = nilai020x.ToString(); } catch (Exception) { nilai020 = null; } }
                        string nilai021x = reader["IPAddress"].ToString(); string nilai021; if (string.IsNullOrEmpty(nilai021x)) { nilai021 = null; } else { try { nilai021 = nilai021x.ToString(); } catch (Exception) { nilai021 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_21b] WHERE [lapuh21b_recordid] = @lapuh21b_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh21b_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh21b_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh21b_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh21b_namanegeri"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh21b_koddaerah"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh21b_tahun"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh21b_kodsukutahun"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["lapuh21b_cadanganhsk"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["lapuh21b_tarikhmmkn"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["lapuh21b_norujukanmmkn"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["lapuh21b_catatan"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["lapuh21b_jh_01_lsediaada"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["lapuh21b_jh_02_lsediaada"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["lapuh21b_jh_03_lsediaada"].ToString(); decimal checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; } else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["lapuh21b_jh_04_lsediaada"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["lapuh21b_jh_01_lcadangan"].ToString(); decimal checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; } else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }
                                    string checknilai016x = readerCheck01["lapuh21b_jh_02_lcadangan"].ToString(); decimal checknilai016; if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = 0; } else { try { checknilai016 = decimal.Parse(checknilai016x); } catch (Exception) { checknilai016 = 0; } }
                                    string checknilai017x = readerCheck01["lapuh21b_jh_03_lcadangan"].ToString(); decimal checknilai017; if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; } else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }
                                    string checknilai018x = readerCheck01["lapuh21b_jh_04_lcadangan"].ToString(); decimal checknilai018; if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = 0; } else { try { checknilai018 = decimal.Parse(checknilai018x); } catch (Exception) { checknilai018 = 0; } }
                                    string checknilai019x = readerCheck01["lapuh21b_jenisrekod"].ToString(); string checknilai019; if (string.IsNullOrEmpty(checknilai019x)) { checknilai019 = null; } else { try { checknilai019 = checknilai019x.ToString(); } catch (Exception) { checknilai019 = null; } }
                                    string checknilai020x = readerCheck01["lapuh21b_nokp"].ToString(); string checknilai020; if (string.IsNullOrEmpty(checknilai020x)) { checknilai020 = null; } else { try { checknilai020 = checknilai020x.ToString(); } catch (Exception) { checknilai020 = null; } }
                                    string checknilai021x = readerCheck01["lapuh21b_ipaddress"].ToString(); string checknilai021; if (string.IsNullOrEmpty(checknilai021x)) { checknilai021 = null; } else { try { checknilai021 = checknilai021x.ToString(); } catch (Exception) { checknilai021 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai017 != nilai017 || checknilai018 != nilai018 || checknilai019 != nilai019 || checknilai020 != nilai020 || checknilai021 != nilai021)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_21b] SET [lapuh21b_kodnegeri]=@lapuh21b_kodnegeri,[lapuh21b_namanegeri]=@lapuh21b_namanegeri,[lapuh21b_koddaerah]=@lapuh21b_koddaerah,[lapuh21b_tahun]=@lapuh21b_tahun,[lapuh21b_kodsukutahun]=@lapuh21b_kodsukutahun,[lapuh21b_cadanganhsk]=@lapuh21b_cadanganhsk,[lapuh21b_tarikhmmkn]=@lapuh21b_tarikhmmkn,[lapuh21b_norujukanmmkn]=@lapuh21b_norujukanmmkn,[lapuh21b_catatan]=@lapuh21b_catatan,[lapuh21b_jh_01_lsediaada]=@lapuh21b_jh_01_lsediaada,[lapuh21b_jh_02_lsediaada]=@lapuh21b_jh_02_lsediaada,[lapuh21b_jh_03_lsediaada]=@lapuh21b_jh_03_lsediaada,[lapuh21b_jh_04_lsediaada]=@lapuh21b_jh_04_lsediaada,[lapuh21b_jh_01_lcadangan]=@lapuh21b_jh_01_lcadangan,[lapuh21b_jh_02_lcadangan]=@lapuh21b_jh_02_lcadangan,[lapuh21b_jh_03_lcadangan]=@lapuh21b_jh_03_lcadangan,[lapuh21b_jh_04_lcadangan]=@lapuh21b_jh_04_lcadangan,[lapuh21b_jenisrekod]=@lapuh21b_jenisrekod,[lapuh21b_nokp]=@lapuh21b_nokp,[lapuh21b_ipaddress]=@lapuh21b_ipaddress WHERE [lapuh21b_recordid]=@lapuh21b_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_namanegeri", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_koddaerah", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_tahun", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_kodsukutahun", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_cadanganhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_cadanganhsk", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_tarikhmmkn", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_tarikhmmkn", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_norujukanmmkn", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_norujukanmmkn", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_catatan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_catatan", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_01_lsediaada", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_01_lsediaada", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_02_lsediaada", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_02_lsediaada", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_03_lsediaada", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_03_lsediaada", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_04_lsediaada", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_04_lsediaada", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_01_lcadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_01_lcadangan", nilai015); }
                                            if (nilai016 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_02_lcadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_02_lcadangan", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_03_lcadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_03_lcadangan", nilai017); }
                                            if (nilai018 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_04_lcadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jh_04_lcadangan", nilai018); }
                                            if (string.IsNullOrEmpty(nilai019)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jenisrekod", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_jenisrekod", nilai019); }
                                            if (string.IsNullOrEmpty(nilai020)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_nokp", nilai020); }
                                            if (string.IsNullOrEmpty(nilai021)) { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh21b_ipaddress", nilai021); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_21b] SET [lapuh21b_date_update] = @lapuh21b_date_update,[lapuh21b_date_view] = @lapuh21b_date_view WHERE [lapuh21b_recordid] = @lapuh21b_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21b_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21b_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh21b_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_21b-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21b-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21b-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_21b] SET [lapuh21b_date_view] = @lapuh21b_date_view WHERE [lapuh21b_recordid] = @lapuh21b_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh21b_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh21b_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21b-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21b-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_21b] ([lapuh21b_recordid],[lapuh21b_kodnegeri],[lapuh21b_namanegeri],[lapuh21b_koddaerah],[lapuh21b_tahun],[lapuh21b_kodsukutahun],[lapuh21b_cadanganhsk],[lapuh21b_tarikhmmkn],[lapuh21b_norujukanmmkn],[lapuh21b_catatan],[lapuh21b_jh_01_lsediaada],[lapuh21b_jh_02_lsediaada],[lapuh21b_jh_03_lsediaada],[lapuh21b_jh_04_lsediaada],[lapuh21b_jh_01_lcadangan],[lapuh21b_jh_02_lcadangan],[lapuh21b_jh_03_lcadangan],[lapuh21b_jh_04_lcadangan],[lapuh21b_jenisrekod],[lapuh21b_nokp],[lapuh21b_ipaddress]) VALUES(@lapuh21b_recordid,@lapuh21b_kodnegeri,@lapuh21b_namanegeri,@lapuh21b_koddaerah,@lapuh21b_tahun,@lapuh21b_kodsukutahun,@lapuh21b_cadanganhsk,@lapuh21b_tarikhmmkn,@lapuh21b_norujukanmmkn,@lapuh21b_catatan,@lapuh21b_jh_01_lsediaada,@lapuh21b_jh_02_lsediaada,@lapuh21b_jh_03_lsediaada,@lapuh21b_jh_04_lsediaada,@lapuh21b_jh_01_lcadangan,@lapuh21b_jh_02_lcadangan,@lapuh21b_jh_03_lcadangan,@lapuh21b_jh_04_lcadangan,@lapuh21b_jenisrekod,@lapuh21b_nokp,@lapuh21b_ipaddress);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_namanegeri", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_koddaerah", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_tahun", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_kodsukutahun", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_cadanganhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_cadanganhsk", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_tarikhmmkn", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_tarikhmmkn", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_norujukanmmkn", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_norujukanmmkn", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_catatan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_catatan", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_01_lsediaada", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_01_lsediaada", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_02_lsediaada", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_02_lsediaada", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_03_lsediaada", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_03_lsediaada", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_04_lsediaada", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_04_lsediaada", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_01_lcadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_01_lcadangan", nilai015); }
                                    if (nilai016 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_02_lcadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_02_lcadangan", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_03_lcadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_03_lcadangan", nilai017); }
                                    if (nilai018 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_04_lcadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jh_04_lcadangan", nilai018); }
                                    if (string.IsNullOrEmpty(nilai019)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jenisrekod", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_jenisrekod", nilai019); }
                                    if (string.IsNullOrEmpty(nilai020)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_nokp", nilai020); }
                                    if (string.IsNullOrEmpty(nilai021)) { cmdInsert01.Parameters.AddWithValue("@lapuh21b_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh21b_ipaddress", nilai021); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_21b] SET [lapuh21b_date_add] = @lapuh21b_date_add,[lapuh21b_date_view] = @lapuh21b_date_view WHERE [lapuh21b_recordid] = @lapuh21b_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21b_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21b_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh21b_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_21b-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_21b-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_21b-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_21b-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_21b-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_21b-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_21b", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_21b-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_21b", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b
            //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b //controller_eSmashLapUh21b

            await Task.Delay(1);

            //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22
            //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_22`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["CadanganHSK"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["NoKP"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["IPAddress"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["NamaNegeri"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["TarikhMMKN"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["NoRujukanMMKN"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["KodDaerah"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["LuasSediaAda_01"].ToString(); decimal nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = 0; } else { try { nilai010 = decimal.Parse(nilai010x); } catch (Exception) { nilai010 = 0; } }
                        string nilai011x = reader["LuasSediaAda_02"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["LuasSediaAda_03"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["LuasSediaAda_04"].ToString(); decimal nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; } else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["LuasCadangan_01"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["LuasCadangan_02"].ToString(); decimal nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; } else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }
                        string nilai016x = reader["LuasCadangan_03"].ToString(); decimal nilai016; if (string.IsNullOrEmpty(nilai016x)) { nilai016 = 0; } else { try { nilai016 = decimal.Parse(nilai016x); } catch (Exception) { nilai016 = 0; } }
                        string nilai017x = reader["LuasCadangan_04"].ToString(); decimal nilai017; if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; } else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }
                        string nilai018x = reader["Catatan"].ToString(); string nilai018; if (string.IsNullOrEmpty(nilai018x)) { nilai018 = null; } else { try { nilai018 = nilai018x.ToString(); } catch (Exception) { nilai018 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_22] WHERE [lapuh22_recordid] = @lapuh22_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh22_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh22_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh22_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh22_cadanganhsk"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh22_nokp"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh22_ipaddress"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh22_namanegeri"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["lapuh22_tarikhmmkn"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["lapuh22_norujukanmmkn"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["lapuh22_koddaerah"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["lapuh22_luassediaada_01"].ToString(); decimal checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = 0; } else { try { checknilai010 = decimal.Parse(checknilai010x); } catch (Exception) { checknilai010 = 0; } }
                                    string checknilai011x = readerCheck01["lapuh22_luassediaada_02"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["lapuh22_luassediaada_03"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["lapuh22_luassediaada_04"].ToString(); decimal checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; } else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["lapuh22_luascadangan_01"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["lapuh22_luascadangan_02"].ToString(); decimal checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; } else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }
                                    string checknilai016x = readerCheck01["lapuh22_luascadangan_03"].ToString(); decimal checknilai016; if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = 0; } else { try { checknilai016 = decimal.Parse(checknilai016x); } catch (Exception) { checknilai016 = 0; } }
                                    string checknilai017x = readerCheck01["lapuh22_luascadangan_04"].ToString(); decimal checknilai017; if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; } else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }
                                    string checknilai018x = readerCheck01["lapuh22_catatan"].ToString(); string checknilai018; if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = null; } else { try { checknilai018 = checknilai018x.ToString(); } catch (Exception) { checknilai018 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai017 != nilai017 || checknilai018 != nilai018)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_22] SET [lapuh22_kodnegeri]=@lapuh22_kodnegeri,[lapuh22_cadanganhsk]=@lapuh22_cadanganhsk,[lapuh22_nokp]=@lapuh22_nokp,[lapuh22_ipaddress]=@lapuh22_ipaddress,[lapuh22_namanegeri]=@lapuh22_namanegeri,[lapuh22_tarikhmmkn]=@lapuh22_tarikhmmkn,[lapuh22_norujukanmmkn]=@lapuh22_norujukanmmkn,[lapuh22_koddaerah]=@lapuh22_koddaerah,[lapuh22_luassediaada_01]=@lapuh22_luassediaada_01,[lapuh22_luassediaada_02]=@lapuh22_luassediaada_02,[lapuh22_luassediaada_03]=@lapuh22_luassediaada_03,[lapuh22_luassediaada_04]=@lapuh22_luassediaada_04,[lapuh22_luascadangan_01]=@lapuh22_luascadangan_01,[lapuh22_luascadangan_02]=@lapuh22_luascadangan_02,[lapuh22_luascadangan_03]=@lapuh22_luascadangan_03,[lapuh22_luascadangan_04]=@lapuh22_luascadangan_04,[lapuh22_catatan]=@lapuh22_catatan WHERE [lapuh22_recordid]=@lapuh22_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_cadanganhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_cadanganhsk", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_nokp", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_ipaddress", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_namanegeri", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_tarikhmmkn", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_tarikhmmkn", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_norujukanmmkn", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_norujukanmmkn", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_koddaerah", nilai009); }
                                            if (nilai010 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_01", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_01", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_02", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_02", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_03", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_03", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_04", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luassediaada_04", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_01", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_01", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_02", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_02", nilai015); }
                                            if (nilai016 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_03", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_03", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_04", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_luascadangan_04", nilai017); }
                                            if (string.IsNullOrEmpty(nilai018)) { cmdUpdate01.Parameters.AddWithValue("@lapuh22_catatan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh22_catatan", nilai018); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_22] SET [lapuh22_date_update] = @lapuh22_date_update,[lapuh22_date_view] = @lapuh22_date_view WHERE [lapuh22_recordid] = @lapuh22_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh22_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh22_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh22_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_22-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_22-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_22-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_22] SET [lapuh22_date_view] = @lapuh22_date_view WHERE [lapuh22_recordid] = @lapuh22_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh22_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh22_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_22-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_22-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_22] ([lapuh22_recordid],[lapuh22_kodnegeri],[lapuh22_cadanganhsk],[lapuh22_nokp],[lapuh22_ipaddress],[lapuh22_namanegeri],[lapuh22_tarikhmmkn],[lapuh22_norujukanmmkn],[lapuh22_koddaerah],[lapuh22_luassediaada_01],[lapuh22_luassediaada_02],[lapuh22_luassediaada_03],[lapuh22_luassediaada_04],[lapuh22_luascadangan_01],[lapuh22_luascadangan_02],[lapuh22_luascadangan_03],[lapuh22_luascadangan_04],[lapuh22_catatan]) VALUES(@lapuh22_recordid,@lapuh22_kodnegeri,@lapuh22_cadanganhsk,@lapuh22_nokp,@lapuh22_ipaddress,@lapuh22_namanegeri,@lapuh22_tarikhmmkn,@lapuh22_norujukanmmkn,@lapuh22_koddaerah,@lapuh22_luassediaada_01,@lapuh22_luassediaada_02,@lapuh22_luassediaada_03,@lapuh22_luassediaada_04,@lapuh22_luascadangan_01,@lapuh22_luascadangan_02,@lapuh22_luascadangan_03,@lapuh22_luascadangan_04,@lapuh22_catatan);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_cadanganhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_cadanganhsk", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_nokp", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_ipaddress", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_namanegeri", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_tarikhmmkn", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_tarikhmmkn", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_norujukanmmkn", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_norujukanmmkn", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_koddaerah", nilai009); }
                                    if (nilai010 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_01", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_01", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_02", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_02", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_03", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_03", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_04", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luassediaada_04", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_01", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_01", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_02", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_02", nilai015); }
                                    if (nilai016 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_03", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_03", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_04", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_luascadangan_04", nilai017); }
                                    if (string.IsNullOrEmpty(nilai018)) { cmdInsert01.Parameters.AddWithValue("@lapuh22_catatan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh22_catatan", nilai018); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_22] SET [lapuh22_date_add] = @lapuh22_date_add,[lapuh22_date_view] = @lapuh22_date_view WHERE [lapuh22_recordid] = @lapuh22_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh22_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh22_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh22_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_22-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_22-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_22-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_22-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_22-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_22-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_22", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_22-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_22", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22
            //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22 //controller_eSmashLapUh22

            await Task.Delay(1);

            //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23
            //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_23`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodHSK"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["NamaHSK"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["NoKP"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["IPAddress"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["NamaNegeri"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["TarikhWarta"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["NoPelanWarta"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["KodDaerah"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["Luas_JH_01"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["Luas_JH_02"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["Luas_JH_03"].ToString(); decimal nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; } else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["Luas_JH_04"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["NoWarta"].ToString(); string nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = null; } else { try { nilai015 = nilai015x.ToString(); } catch (Exception) { nilai015 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_23] WHERE [lapuh23_recordid] = @lapuh23_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh23_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh23_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh23_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh23_kodhsk"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh23_namahsk"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh23_nokp"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh23_ipaddress"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["lapuh23_namanegeri"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["lapuh23_tarikhwarta"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["lapuh23_nopelanwarta"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["lapuh23_koddaerah"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["lapuh23_luas_jh_01"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["lapuh23_luas_jh_02"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["lapuh23_luas_jh_03"].ToString(); decimal checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; } else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["lapuh23_luas_jh_04"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["lapuh23_nowarta"].ToString(); string checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = null; } else { try { checknilai015 = checknilai015x.ToString(); } catch (Exception) { checknilai015 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_23] SET [lapuh23_kodnegeri]=@lapuh23_kodnegeri,[lapuh23_kodhsk]=@lapuh23_kodhsk,[lapuh23_namahsk]=@lapuh23_namahsk,[lapuh23_nokp]=@lapuh23_nokp,[lapuh23_ipaddress]=@lapuh23_ipaddress,[lapuh23_namanegeri]=@lapuh23_namanegeri,[lapuh23_tarikhwarta]=@lapuh23_tarikhwarta,[lapuh23_nopelanwarta]=@lapuh23_nopelanwarta,[lapuh23_koddaerah]=@lapuh23_koddaerah,[lapuh23_luas_jh_01]=@lapuh23_luas_jh_01,[lapuh23_luas_jh_02]=@lapuh23_luas_jh_02,[lapuh23_luas_jh_03]=@lapuh23_luas_jh_03,[lapuh23_luas_jh_04]=@lapuh23_luas_jh_04,[lapuh23_nowarta]=@lapuh23_nowarta WHERE [lapuh23_recordid]=@lapuh23_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_kodhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_kodhsk", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_namahsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_namahsk", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_nokp", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_ipaddress", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_namanegeri", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_tarikhwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_tarikhwarta", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_nopelanwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_nopelanwarta", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_koddaerah", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_01", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_01", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_02", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_02", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_03", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_03", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_04", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_luas_jh_04", nilai014); }
                                            if (string.IsNullOrEmpty(nilai015)) { cmdUpdate01.Parameters.AddWithValue("@lapuh23_nowarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh23_nowarta", nilai015); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_23] SET [lapuh23_date_update] = @lapuh23_date_update,[lapuh23_date_view] = @lapuh23_date_view WHERE [lapuh23_recordid] = @lapuh23_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh23_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh23_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh23_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_23-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_23-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_23-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_23] SET [lapuh23_date_view] = @lapuh23_date_view WHERE [lapuh23_recordid] = @lapuh23_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh23_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh23_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_23-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_23-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_23] ([lapuh23_recordid],[lapuh23_kodnegeri],[lapuh23_kodhsk],[lapuh23_namahsk],[lapuh23_nokp],[lapuh23_ipaddress],[lapuh23_namanegeri],[lapuh23_tarikhwarta],[lapuh23_nopelanwarta],[lapuh23_koddaerah],[lapuh23_luas_jh_01],[lapuh23_luas_jh_02],[lapuh23_luas_jh_03],[lapuh23_luas_jh_04],[lapuh23_nowarta]) VALUES(@lapuh23_recordid,@lapuh23_kodnegeri,@lapuh23_kodhsk,@lapuh23_namahsk,@lapuh23_nokp,@lapuh23_ipaddress,@lapuh23_namanegeri,@lapuh23_tarikhwarta,@lapuh23_nopelanwarta,@lapuh23_koddaerah,@lapuh23_luas_jh_01,@lapuh23_luas_jh_02,@lapuh23_luas_jh_03,@lapuh23_luas_jh_04,@lapuh23_nowarta);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_kodhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_kodhsk", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_namahsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_namahsk", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_nokp", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_ipaddress", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_namanegeri", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_tarikhwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_tarikhwarta", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_nopelanwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_nopelanwarta", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_koddaerah", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_01", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_01", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_02", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_02", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_03", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_03", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_04", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_luas_jh_04", nilai014); }
                                    if (string.IsNullOrEmpty(nilai015)) { cmdInsert01.Parameters.AddWithValue("@lapuh23_nowarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh23_nowarta", nilai015); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_23] SET [lapuh23_date_add] = @lapuh23_date_add,[lapuh23_date_view] = @lapuh23_date_view WHERE [lapuh23_recordid] = @lapuh23_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh23_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh23_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh23_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_23-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_23-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_23-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_23-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_23-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_23-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {

                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_23", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_23-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "lap_uh_23", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23
            //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23 //controller_eSmashLapUh23

            await Task.Delay(1);

            //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30
            //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_30`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoKP"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["IPAddress"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["NamaNegeri"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["InfoTarikh"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["LH01"].ToString(); decimal nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; } else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["LH02"].ToString(); decimal nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = 0; } else { try { nilai007 = decimal.Parse(nilai007x); } catch (Exception) { nilai007 = 0; } }
                        string nilai008x = reader["LH03"].ToString(); decimal nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = 0; } else { try { nilai008 = decimal.Parse(nilai008x); } catch (Exception) { nilai008 = 0; } }
                        string nilai009x = reader["LH04"].ToString(); decimal nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = 0; } else { try { nilai009 = decimal.Parse(nilai009x); } catch (Exception) { nilai009 = 0; } }
                        string nilai010x = reader["KodNegeri"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_30] WHERE [lapuh30_recordid] = @lapuh30_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh30_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh30_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh30_nokp"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh30_ipaddress"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh30_namanegeri"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh30_infotarikh"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh30_lh01"].ToString(); decimal checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; } else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }
                                    string checknilai007x = readerCheck01["lapuh30_lh02"].ToString(); decimal checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = 0; } else { try { checknilai007 = decimal.Parse(checknilai007x); } catch (Exception) { checknilai007 = 0; } }
                                    string checknilai008x = readerCheck01["lapuh30_lh03"].ToString(); decimal checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = 0; } else { try { checknilai008 = decimal.Parse(checknilai008x); } catch (Exception) { checknilai008 = 0; } }
                                    string checknilai009x = readerCheck01["lapuh30_lh04"].ToString(); decimal checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = 0; } else { try { checknilai009 = decimal.Parse(checknilai009x); } catch (Exception) { checknilai009 = 0; } }
                                    string checknilai010x = readerCheck01["lapuh30_kodnegeri"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_30] SET [lapuh30_nokp]=@lapuh30_nokp,[lapuh30_ipaddress]=@lapuh30_ipaddress,[lapuh30_namanegeri]=@lapuh30_namanegeri,[lapuh30_infotarikh]=@lapuh30_infotarikh,[lapuh30_lh01]=@lapuh30_lh01,[lapuh30_lh02]=@lapuh30_lh02,[lapuh30_lh03]=@lapuh30_lh03,[lapuh30_lh04]=@lapuh30_lh04,[lapuh30_kodnegeri]=@lapuh30_kodnegeri WHERE [lapuh30_recordid]=@lapuh30_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_nokp", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_ipaddress", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_namanegeri", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_infotarikh", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_infotarikh", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh01", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh01", nilai006); }
                                            if (nilai007 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh02", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh02", nilai007); }
                                            if (nilai008 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh03", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh03", nilai008); }
                                            if (nilai009 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh04", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_lh04", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@lapuh30_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh30_kodnegeri", nilai010); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_30] SET [lapuh30_date_update] = @lapuh30_date_update,[lapuh30_date_view] = @lapuh30_date_view WHERE [lapuh30_recordid] = @lapuh30_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh30_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh30_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh30_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_30-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_30-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_30-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_30] SET [lapuh30_date_view] = @lapuh30_date_view WHERE [lapuh30_recordid] = @lapuh30_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh30_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh30_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_30-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        ////listBoxEsmashStatus.Items.Add("eSmash-lap_uh_30-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_30] ([lapuh30_recordid],[lapuh30_nokp],[lapuh30_ipaddress],[lapuh30_namanegeri],[lapuh30_infotarikh],[lapuh30_lh01],[lapuh30_lh02],[lapuh30_lh03],[lapuh30_lh04],[lapuh30_kodnegeri]) VALUES(@lapuh30_recordid,@lapuh30_nokp,@lapuh30_ipaddress,@lapuh30_namanegeri,@lapuh30_infotarikh,@lapuh30_lh01,@lapuh30_lh02,@lapuh30_lh03,@lapuh30_lh04,@lapuh30_kodnegeri);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh30_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh30_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_nokp", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh30_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_ipaddress", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh30_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_namanegeri", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh30_infotarikh", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_infotarikh", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh01", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh01", nilai006); }
                                    if (nilai007 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh02", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh02", nilai007); }
                                    if (nilai008 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh03", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh03", nilai008); }
                                    if (nilai009 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh04", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_lh04", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@lapuh30_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh30_kodnegeri", nilai010); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_30] SET [lapuh30_date_add] = @lapuh30_date_add,[lapuh30_date_view] = @lapuh30_date_view WHERE [lapuh30_recordid] = @lapuh30_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh30_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh30_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh30_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_30-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_30-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_30-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_30-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_30-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_30-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_30", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_30-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_30", "esmash", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30
            //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30 //controller_eSmashLapUh30

            await Task.Delay(1);

            //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40
            //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_40`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {
                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoKP"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["IPAddress"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["NamaNegeri"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["InfoTarikh"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["LH01"].ToString(); decimal nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; } else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["LH02"].ToString(); decimal nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = 0; } else { try { nilai007 = decimal.Parse(nilai007x); } catch (Exception) { nilai007 = 0; } }
                        string nilai008x = reader["LH03"].ToString(); decimal nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = 0; } else { try { nilai008 = decimal.Parse(nilai008x); } catch (Exception) { nilai008 = 0; } }
                        string nilai009x = reader["LH04"].ToString(); decimal nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = 0; } else { try { nilai009 = decimal.Parse(nilai009x); } catch (Exception) { nilai009 = 0; } }
                        string nilai010x = reader["LH05"].ToString(); decimal nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = 0; } else { try { nilai010 = decimal.Parse(nilai010x); } catch (Exception) { nilai010 = 0; } }
                        string nilai011x = reader["LH06"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["LH07"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["LH08"].ToString(); decimal nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; } else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["LH09"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["LH10"].ToString(); decimal nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; } else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }
                        string nilai016x = reader["LH11"].ToString(); decimal nilai016; if (string.IsNullOrEmpty(nilai016x)) { nilai016 = 0; } else { try { nilai016 = decimal.Parse(nilai016x); } catch (Exception) { nilai016 = 0; } }
                        string nilai017x = reader["LH12"].ToString(); decimal nilai017; if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; } else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }
                        string nilai018x = reader["KodNegeri"].ToString(); string nilai018; if (string.IsNullOrEmpty(nilai018x)) { nilai018 = null; } else { try { nilai018 = nilai018x.ToString(); } catch (Exception) { nilai018 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_40] WHERE [lapuh40_recordid] = @lapuh40_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh40_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh40_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh40_nokp"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh40_ipaddress"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh40_namanegeri"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh40_infotarikh"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh40_lh01"].ToString(); decimal checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; } else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }
                                    string checknilai007x = readerCheck01["lapuh40_lh02"].ToString(); decimal checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = 0; } else { try { checknilai007 = decimal.Parse(checknilai007x); } catch (Exception) { checknilai007 = 0; } }
                                    string checknilai008x = readerCheck01["lapuh40_lh03"].ToString(); decimal checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = 0; } else { try { checknilai008 = decimal.Parse(checknilai008x); } catch (Exception) { checknilai008 = 0; } }
                                    string checknilai009x = readerCheck01["lapuh40_lh04"].ToString(); decimal checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = 0; } else { try { checknilai009 = decimal.Parse(checknilai009x); } catch (Exception) { checknilai009 = 0; } }
                                    string checknilai010x = readerCheck01["lapuh40_lh05"].ToString(); decimal checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = 0; } else { try { checknilai010 = decimal.Parse(checknilai010x); } catch (Exception) { checknilai010 = 0; } }
                                    string checknilai011x = readerCheck01["lapuh40_lh06"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["lapuh40_lh07"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["lapuh40_lh08"].ToString(); decimal checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; } else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["lapuh40_lh09"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["lapuh40_lh10"].ToString(); decimal checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; } else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }
                                    string checknilai016x = readerCheck01["lapuh40_lh11"].ToString(); decimal checknilai016; if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = 0; } else { try { checknilai016 = decimal.Parse(checknilai016x); } catch (Exception) { checknilai016 = 0; } }
                                    string checknilai017x = readerCheck01["lapuh40_lh12"].ToString(); decimal checknilai017; if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; } else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }
                                    string checknilai018x = readerCheck01["lapuh40_kodnegeri"].ToString(); string checknilai018; if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = null; } else { try { checknilai018 = checknilai018x.ToString(); } catch (Exception) { checknilai018 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai017 != nilai017 || checknilai018 != nilai018)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_40] SET [lapuh40_nokp]=@lapuh40_nokp,[lapuh40_ipaddress]=@lapuh40_ipaddress,[lapuh40_namanegeri]=@lapuh40_namanegeri,[lapuh40_infotarikh]=@lapuh40_infotarikh,[lapuh40_lh01]=@lapuh40_lh01,[lapuh40_lh02]=@lapuh40_lh02,[lapuh40_lh03]=@lapuh40_lh03,[lapuh40_lh04]=@lapuh40_lh04,[lapuh40_lh05]=@lapuh40_lh05,[lapuh40_lh06]=@lapuh40_lh06,[lapuh40_lh07]=@lapuh40_lh07,[lapuh40_lh08]=@lapuh40_lh08,[lapuh40_lh09]=@lapuh40_lh09,[lapuh40_lh10]=@lapuh40_lh10,[lapuh40_lh11]=@lapuh40_lh11,[lapuh40_lh12]=@lapuh40_lh12,[lapuh40_kodnegeri]=@lapuh40_kodnegeri WHERE [lapuh40_recordid]=@lapuh40_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_nokp", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_ipaddress", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_namanegeri", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_infotarikh", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_infotarikh", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh01", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh01", nilai006); }
                                            if (nilai007 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh02", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh02", nilai007); }
                                            if (nilai008 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh03", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh03", nilai008); }
                                            if (nilai009 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh04", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh04", nilai009); }
                                            if (nilai010 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh05", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh05", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh06", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh06", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh07", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh07", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh08", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh08", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh09", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh09", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh10", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh10", nilai015); }
                                            if (nilai016 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh11", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh11", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh12", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_lh12", nilai017); }
                                            if (string.IsNullOrEmpty(nilai018)) { cmdUpdate01.Parameters.AddWithValue("@lapuh40_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh40_kodnegeri", nilai018); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_40] SET [lapuh40_date_update] = @lapuh40_date_update,[lapuh40_date_view] = @lapuh40_date_view WHERE [lapuh40_recordid] = @lapuh40_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh40_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh40_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh40_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_40-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_40-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_40-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_40] SET [lapuh40_date_view] = @lapuh40_date_view WHERE [lapuh40_recordid] = @lapuh40_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh40_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh40_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_40-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        ////listBoxEsmashStatus.Items.Add("eSmash-lap_uh_40-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_40] ([lapuh40_recordid],[lapuh40_nokp],[lapuh40_ipaddress],[lapuh40_namanegeri],[lapuh40_infotarikh],[lapuh40_lh01],[lapuh40_lh02],[lapuh40_lh03],[lapuh40_lh04],[lapuh40_lh05],[lapuh40_lh06],[lapuh40_lh07],[lapuh40_lh08],[lapuh40_lh09],[lapuh40_lh10],[lapuh40_lh11],[lapuh40_lh12],[lapuh40_kodnegeri]) VALUES(@lapuh40_recordid,@lapuh40_nokp,@lapuh40_ipaddress,@lapuh40_namanegeri,@lapuh40_infotarikh,@lapuh40_lh01,@lapuh40_lh02,@lapuh40_lh03,@lapuh40_lh04,@lapuh40_lh05,@lapuh40_lh06,@lapuh40_lh07,@lapuh40_lh08,@lapuh40_lh09,@lapuh40_lh10,@lapuh40_lh11,@lapuh40_lh12,@lapuh40_kodnegeri);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh40_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh40_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_nokp", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh40_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_ipaddress", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh40_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_namanegeri", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh40_infotarikh", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_infotarikh", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh01", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh01", nilai006); }
                                    if (nilai007 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh02", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh02", nilai007); }
                                    if (nilai008 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh03", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh03", nilai008); }
                                    if (nilai009 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh04", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh04", nilai009); }
                                    if (nilai010 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh05", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh05", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh06", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh06", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh07", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh07", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh08", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh08", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh09", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh09", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh10", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh10", nilai015); }
                                    if (nilai016 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh11", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh11", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh12", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_lh12", nilai017); }
                                    if (string.IsNullOrEmpty(nilai018)) { cmdInsert01.Parameters.AddWithValue("@lapuh40_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh40_kodnegeri", nilai018); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_40] SET [lapuh40_date_add] = @lapuh40_date_add,[lapuh40_date_view] = @lapuh40_date_view WHERE [lapuh40_recordid] = @lapuh40_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh40_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh40_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh40_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lap_uh_40-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_40-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_40-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_40-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_40-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_40-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_40", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_40-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_40", "esmash", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40
            //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40 //controller_eSmashLapUh40

            await Task.Delay(1);

            //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50
            //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `lap_uh_50`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoKP"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["IPAddress"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["NamaNegeri"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodFungsiHutan"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["LCTSebelum"].ToString(); decimal nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; } else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["LWTSebelum"].ToString(); decimal nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = 0; } else { try { nilai007 = decimal.Parse(nilai007x); } catch (Exception) { nilai007 = 0; } }
                        string nilai008x = reader["LCTPilihan"].ToString(); decimal nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = 0; } else { try { nilai008 = decimal.Parse(nilai008x); } catch (Exception) { nilai008 = 0; } }
                        string nilai009x = reader["LWTPilihan"].ToString(); decimal nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = 0; } else { try { nilai009 = decimal.Parse(nilai009x); } catch (Exception) { nilai009 = 0; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[lap_uh_50] WHERE [lapuh50_recordid] = @lapuh50_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@lapuh50_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["lapuh50_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["lapuh50_nokp"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["lapuh50_ipaddress"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["lapuh50_namanegeri"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["lapuh50_kodfungsihutan"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["lapuh50_lctsebelum"].ToString(); decimal checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; } else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }
                                    string checknilai007x = readerCheck01["lapuh50_lwtsebelum"].ToString(); decimal checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = 0; } else { try { checknilai007 = decimal.Parse(checknilai007x); } catch (Exception) { checknilai007 = 0; } }
                                    string checknilai008x = readerCheck01["lapuh50_lctpilihan"].ToString(); decimal checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = 0; } else { try { checknilai008 = decimal.Parse(checknilai008x); } catch (Exception) { checknilai008 = 0; } }
                                    string checknilai009x = readerCheck01["lapuh50_lwtpilihan"].ToString(); decimal checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = 0; } else { try { checknilai009 = decimal.Parse(checknilai009x); } catch (Exception) { checknilai009 = 0; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[lap_uh_50] SET [lapuh50_nokp]=@lapuh50_nokp,[lapuh50_ipaddress]=@lapuh50_ipaddress,[lapuh50_namanegeri]=@lapuh50_namanegeri,[lapuh50_kodfungsihutan]=@lapuh50_kodfungsihutan,[lapuh50_lctsebelum]=@lapuh50_lctsebelum,[lapuh50_lwtsebelum]=@lapuh50_lwtsebelum,[lapuh50_lctpilihan]=@lapuh50_lctpilihan,[lapuh50_lwtpilihan]=@lapuh50_lwtpilihan WHERE [lapuh50_recordid]=@lapuh50_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_nokp", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_ipaddress", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_namanegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_namanegeri", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_kodfungsihutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_kodfungsihutan", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lctsebelum", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lctsebelum", nilai006); }
                                            if (nilai007 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lwtsebelum", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lwtsebelum", nilai007); }
                                            if (nilai008 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lctpilihan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lctpilihan", nilai008); }
                                            if (nilai009 == 0) { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lwtpilihan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@lapuh50_lwtpilihan", nilai009); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[lap_uh_50] SET [lapuh50_date_update] = @lapuh50_date_update,[lapuh50_date_view] = @lapuh50_date_view WHERE [lapuh50_recordid] = @lapuh50_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh50_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh50_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@lapuh50_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-lap_uh_50-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_50-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_50-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[lap_uh_50] SET [lapuh50_date_view] = @lapuh50_date_view WHERE [lapuh50_recordid] = @lapuh50_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh50_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@lapuh50_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_50-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        ////listBoxEsmashStatus.Items.Add("eSmash-lap_uh_50-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[lap_uh_50] ([lapuh50_recordid],[lapuh50_nokp],[lapuh50_ipaddress],[lapuh50_namanegeri],[lapuh50_kodfungsihutan],[lapuh50_lctsebelum],[lapuh50_lwtsebelum],[lapuh50_lctpilihan],[lapuh50_lwtpilihan]) VALUES(@lapuh50_recordid,@lapuh50_nokp,@lapuh50_ipaddress,@lapuh50_namanegeri,@lapuh50_kodfungsihutan,@lapuh50_lctsebelum,@lapuh50_lwtsebelum,@lapuh50_lctpilihan,@lapuh50_lwtpilihan);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@lapuh50_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@lapuh50_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_nokp", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@lapuh50_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_ipaddress", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@lapuh50_namanegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_namanegeri", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@lapuh50_kodfungsihutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_kodfungsihutan", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh50_lctsebelum", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_lctsebelum", nilai006); }
                                    if (nilai007 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh50_lwtsebelum", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_lwtsebelum", nilai007); }
                                    if (nilai008 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh50_lctpilihan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_lctpilihan", nilai008); }
                                    if (nilai009 == 0) { cmdInsert01.Parameters.AddWithValue("@lapuh50_lwtpilihan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@lapuh50_lwtpilihan", nilai009); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[lap_uh_50] SET [lapuh50_date_add] = @lapuh50_date_add,[lapuh50_date_view] = @lapuh50_date_view WHERE [lapuh50_recordid] = @lapuh50_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh50_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh50_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@lapuh50_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-lapuh50_date_add-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-lap_uh_50-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-lap_uh_50-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                            //if (finalvalue == 1) {
                            //    MessageBox.Show("check !");
                            //}
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-lap_uh_50-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_50-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-lap_uh_50-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "lap_uh_50", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-lap_uh_50-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "lap_uh_50", "esmash", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50
            //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50 //controller_eSmashLapUh50

            await Task.Delay(1);

            //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan
            //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `ops_fungsi_hutan`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["NoKP"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["IPAddress"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodNegeri"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodDaerah"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["Tahun"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["KodSukuTahun"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["KodHSK"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["KodFungsiHutan"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["KodJenisHutan"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["NoWarta"].ToString(); string nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = null; } else { try { nilai011 = nilai011x.ToString(); } catch (Exception) { nilai011 = null; } }
                        string nilai012x = reader["TarikhWarta"].ToString(); string nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = null; } else { try { nilai012 = nilai012x.ToString(); } catch (Exception) { nilai012 = null; } }
                        string nilai013x = reader["NoPelanWarta"].ToString(); string nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = null; } else { try { nilai013 = nilai013x.ToString(); } catch (Exception) { nilai013 = null; } }
                        string nilai014x = reader["LuasWartaCadangan"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["LuasWartaDiwarta"].ToString(); decimal nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; } else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }
                        string nilai016x = reader["LuasDigitalCadangan"].ToString(); decimal nilai016; if (string.IsNullOrEmpty(nilai016x)) { nilai016 = 0; } else { try { nilai016 = decimal.Parse(nilai016x); } catch (Exception) { nilai016 = 0; } }
                        string nilai017x = reader["LuasDigitalDiwarta"].ToString(); decimal nilai017; if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; } else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[ops_fungsi_hutan] WHERE [opsfungsihutan_recordid] = @opsfungsihutan_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@opsfungsihutan_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["opsfungsihutan_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["opsfungsihutan_nokp"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["opsfungsihutan_ipaddress"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["opsfungsihutan_kodnegeri"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["opsfungsihutan_koddaerah"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["opsfungsihutan_tahun"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["opsfungsihutan_kodsukutahun"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["opsfungsihutan_kodhsk"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["opsfungsihutan_kodfungsihutan"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["opsfungsihutan_kodjenishutan"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["opsfungsihutan_nowarta"].ToString(); string checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = null; } else { try { checknilai011 = checknilai011x.ToString(); } catch (Exception) { checknilai011 = null; } }
                                    string checknilai012x = readerCheck01["opsfungsihutan_tarikhwarta"].ToString(); string checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = null; } else { try { checknilai012 = checknilai012x.ToString(); } catch (Exception) { checknilai012 = null; } }
                                    string checknilai013x = readerCheck01["opsfungsihutan_nopelanwarta"].ToString(); string checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = null; } else { try { checknilai013 = checknilai013x.ToString(); } catch (Exception) { checknilai013 = null; } }
                                    string checknilai014x = readerCheck01["opsfungsihutan_luaswartacadangan"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["opsfungsihutan_luaswartadiwarta"].ToString(); decimal checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; } else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }
                                    string checknilai016x = readerCheck01["opsfungsihutan_luasdigitalcadangan"].ToString(); decimal checknilai016; if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = 0; } else { try { checknilai016 = decimal.Parse(checknilai016x); } catch (Exception) { checknilai016 = 0; } }
                                    string checknilai017x = readerCheck01["opsfungsihutan_luasdigitaldiwarta"].ToString(); decimal checknilai017; if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; } else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai017 != nilai017)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[ops_fungsi_hutan] SET [opsfungsihutan_nokp]=@opsfungsihutan_nokp,[opsfungsihutan_ipaddress]=@opsfungsihutan_ipaddress,[opsfungsihutan_kodnegeri]=@opsfungsihutan_kodnegeri,[opsfungsihutan_koddaerah]=@opsfungsihutan_koddaerah,[opsfungsihutan_tahun]=@opsfungsihutan_tahun,[opsfungsihutan_kodsukutahun]=@opsfungsihutan_kodsukutahun,[opsfungsihutan_kodhsk]=@opsfungsihutan_kodhsk,[opsfungsihutan_kodfungsihutan]=@opsfungsihutan_kodfungsihutan,[opsfungsihutan_kodjenishutan]=@opsfungsihutan_kodjenishutan,[opsfungsihutan_nowarta]=@opsfungsihutan_nowarta,[opsfungsihutan_tarikhwarta]=@opsfungsihutan_tarikhwarta,[opsfungsihutan_nopelanwarta]=@opsfungsihutan_nopelanwarta,[opsfungsihutan_luaswartacadangan]=@opsfungsihutan_luaswartacadangan,[opsfungsihutan_luaswartadiwarta]=@opsfungsihutan_luaswartadiwarta,[opsfungsihutan_luasdigitalcadangan]=@opsfungsihutan_luasdigitalcadangan,[opsfungsihutan_luasdigitaldiwarta]=@opsfungsihutan_luasdigitaldiwarta WHERE [opsfungsihutan_recordid]=@opsfungsihutan_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_nokp", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_nokp", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_ipaddress", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_ipaddress", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodnegeri", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_koddaerah", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_tahun", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodsukutahun", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodhsk", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodfungsihutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodfungsihutan", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodjenishutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_kodjenishutan", nilai010); }
                                            if (string.IsNullOrEmpty(nilai011)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_nowarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_nowarta", nilai011); }
                                            if (string.IsNullOrEmpty(nilai012)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_tarikhwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_tarikhwarta", nilai012); }
                                            if (string.IsNullOrEmpty(nilai013)) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_nopelanwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_nopelanwarta", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luaswartacadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luaswartacadangan", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luaswartadiwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luaswartadiwarta", nilai015); }
                                            if (nilai016 == 0) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luasdigitalcadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luasdigitalcadangan", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luasdigitaldiwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@opsfungsihutan_luasdigitaldiwarta", nilai017); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[ops_fungsi_hutan] SET [opsfungsihutan_date_update] = @opsfungsihutan_date_update,[opsfungsihutan_date_view] = @opsfungsihutan_date_view WHERE [opsfungsihutan_recordid] = @opsfungsihutan_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-ops_fungsi_hutan-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-ops_fungsi_hutan-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-ops_fungsi_hutan-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[ops_fungsi_hutan] SET [opsfungsihutan_date_view] = @opsfungsihutan_date_view WHERE [opsfungsihutan_recordid] = @opsfungsihutan_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-ops_fungsi_hutan-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        ////listBoxEsmashStatus.Items.Add("eSmash-ops_fungsi_hutan-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[ops_fungsi_hutan] ([opsfungsihutan_recordid],[opsfungsihutan_nokp],[opsfungsihutan_ipaddress],[opsfungsihutan_kodnegeri],[opsfungsihutan_koddaerah],[opsfungsihutan_tahun],[opsfungsihutan_kodsukutahun],[opsfungsihutan_kodhsk],[opsfungsihutan_kodfungsihutan],[opsfungsihutan_kodjenishutan],[opsfungsihutan_nowarta],[opsfungsihutan_tarikhwarta],[opsfungsihutan_nopelanwarta],[opsfungsihutan_luaswartacadangan],[opsfungsihutan_luaswartadiwarta],[opsfungsihutan_luasdigitalcadangan],[opsfungsihutan_luasdigitaldiwarta]) VALUES(@opsfungsihutan_recordid,@opsfungsihutan_nokp,@opsfungsihutan_ipaddress,@opsfungsihutan_kodnegeri,@opsfungsihutan_koddaerah,@opsfungsihutan_tahun,@opsfungsihutan_kodsukutahun,@opsfungsihutan_kodhsk,@opsfungsihutan_kodfungsihutan,@opsfungsihutan_kodjenishutan,@opsfungsihutan_nowarta,@opsfungsihutan_tarikhwarta,@opsfungsihutan_nopelanwarta,@opsfungsihutan_luaswartacadangan,@opsfungsihutan_luaswartadiwarta,@opsfungsihutan_luasdigitalcadangan,@opsfungsihutan_luasdigitaldiwarta);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_nokp", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_nokp", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_ipaddress", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_ipaddress", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodnegeri", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_koddaerah", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_tahun", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodsukutahun", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodhsk", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodfungsihutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodfungsihutan", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodjenishutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_kodjenishutan", nilai010); }
                                    if (string.IsNullOrEmpty(nilai011)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_nowarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_nowarta", nilai011); }
                                    if (string.IsNullOrEmpty(nilai012)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_tarikhwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_tarikhwarta", nilai012); }
                                    if (string.IsNullOrEmpty(nilai013)) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_nopelanwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_nopelanwarta", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luaswartacadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luaswartacadangan", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luaswartadiwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luaswartadiwarta", nilai015); }
                                    if (nilai016 == 0) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luasdigitalcadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luasdigitalcadangan", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luasdigitaldiwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@opsfungsihutan_luasdigitaldiwarta", nilai017); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[ops_fungsi_hutan] SET [opsfungsihutan_date_add] = @opsfungsihutan_date_add,[opsfungsihutan_date_view] = @opsfungsihutan_date_view WHERE [opsfungsihutan_recordid] = @opsfungsihutan_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@opsfungsihutan_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-ops_fungsi_hutan-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-ops_fungsi_hutan-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-ops_fungsi_hutan-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-ops_fungsi_hutan-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-ops_fungsi_hutan-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-ops_fungsi_hutan-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "ops_fungsi_hutan", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-ops_fungsi_hutan-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "ops_fungsi_hutan", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan
            //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan //controller_eSmashOpsFungsiHutan

            await Task.Delay(1);

            //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan
            //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `pewartaan_fungsi_hutan`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodDaerah"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["Tahun"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodSukuTahun"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["KodHSK"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["KodFungsiHutan"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["TarikhWarta"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["NoWarta"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["NoPelanWarta"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["LuasWartaCadangan"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["LuasWartaDiwarta"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["LuasDigitalCadangan"].ToString(); decimal nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; } else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["LuasDigitalDiwarta"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["IndikatorBertindih"].ToString(); string nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = null; } else { try { nilai015 = nilai015x.ToString(); } catch (Exception) { nilai015 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[pewartaan_fungsi_hutan] WHERE [pewartaanfungsihutan_recordid] = @pewartaanfungsihutan_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["pewartaanfungsihutan_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["pewartaanfungsihutan_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["pewartaanfungsihutan_koddaerah"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["pewartaanfungsihutan_tahun"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["pewartaanfungsihutan_kodsukutahun"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["pewartaanfungsihutan_kodhsk"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["pewartaanfungsihutan_kodfungsihutan"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["pewartaanfungsihutan_tarikhwarta"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["pewartaanfungsihutan_nowarta"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["pewartaanfungsihutan_nopelanwarta"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["pewartaanfungsihutan_luaswartacadangan"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["pewartaanfungsihutan_luaswartadiwarta"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["pewartaanfungsihutan_luasdigitalcadangan"].ToString(); decimal checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; } else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["pewartaanfungsihutan_luasdigitaldiwarta"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["pewartaanfungsihutan_indikatorbertindih"].ToString(); string checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = null; } else { try { checknilai015 = checknilai015x.ToString(); } catch (Exception) { checknilai015 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[pewartaan_fungsi_hutan] SET [pewartaanfungsihutan_kodnegeri]=@pewartaanfungsihutan_kodnegeri,[pewartaanfungsihutan_koddaerah]=@pewartaanfungsihutan_koddaerah,[pewartaanfungsihutan_tahun]=@pewartaanfungsihutan_tahun,[pewartaanfungsihutan_kodsukutahun]=@pewartaanfungsihutan_kodsukutahun,[pewartaanfungsihutan_kodhsk]=@pewartaanfungsihutan_kodhsk,[pewartaanfungsihutan_kodfungsihutan]=@pewartaanfungsihutan_kodfungsihutan,[pewartaanfungsihutan_tarikhwarta]=@pewartaanfungsihutan_tarikhwarta,[pewartaanfungsihutan_nowarta]=@pewartaanfungsihutan_nowarta,[pewartaanfungsihutan_nopelanwarta]=@pewartaanfungsihutan_nopelanwarta,[pewartaanfungsihutan_luaswartacadangan]=@pewartaanfungsihutan_luaswartacadangan,[pewartaanfungsihutan_luaswartadiwarta]=@pewartaanfungsihutan_luaswartadiwarta,[pewartaanfungsihutan_luasdigitalcadangan]=@pewartaanfungsihutan_luasdigitalcadangan,[pewartaanfungsihutan_luasdigitaldiwarta]=@pewartaanfungsihutan_luasdigitaldiwarta,[pewartaanfungsihutan_indikatorbertindih]=@pewartaanfungsihutan_indikatorbertindih WHERE [pewartaanfungsihutan_recordid]=@pewartaanfungsihutan_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_koddaerah", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_tahun", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodsukutahun", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodhsk", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodfungsihutan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_kodfungsihutan", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_tarikhwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_tarikhwarta", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_nowarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_nowarta", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_nopelanwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_nopelanwarta", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartacadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartacadangan", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartadiwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartadiwarta", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitalcadangan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitalcadangan", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitaldiwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitaldiwarta", nilai014); }
                                            if (string.IsNullOrEmpty(nilai015)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_indikatorbertindih", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanfungsihutan_indikatorbertindih", nilai015); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[pewartaan_fungsi_hutan] SET [pewartaanfungsihutan_date_update] = @pewartaanfungsihutan_date_update,[pewartaanfungsihutan_date_view] = @pewartaanfungsihutan_date_view WHERE [pewartaanfungsihutan_recordid] = @pewartaanfungsihutan_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-pewartaan_fungsi_hutan-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-pewartaan_fungsi_hutan-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-pewartaan_fungsi_hutan-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[pewartaan_fungsi_hutan] SET [pewartaanfungsihutan_date_view] = @pewartaanfungsihutan_date_view WHERE [pewartaanfungsihutan_recordid] = @pewartaanfungsihutan_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-pewartaan_fungsi_hutan-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        ////listBoxEsmashStatus.Items.Add("eSmash-pewartaan_fungsi_hutan-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[pewartaan_fungsi_hutan] ([pewartaanfungsihutan_recordid],[pewartaanfungsihutan_kodnegeri],[pewartaanfungsihutan_koddaerah],[pewartaanfungsihutan_tahun],[pewartaanfungsihutan_kodsukutahun],[pewartaanfungsihutan_kodhsk],[pewartaanfungsihutan_kodfungsihutan],[pewartaanfungsihutan_tarikhwarta],[pewartaanfungsihutan_nowarta],[pewartaanfungsihutan_nopelanwarta],[pewartaanfungsihutan_luaswartacadangan],[pewartaanfungsihutan_luaswartadiwarta],[pewartaanfungsihutan_luasdigitalcadangan],[pewartaanfungsihutan_luasdigitaldiwarta],[pewartaanfungsihutan_indikatorbertindih]) VALUES(@pewartaanfungsihutan_recordid,@pewartaanfungsihutan_kodnegeri,@pewartaanfungsihutan_koddaerah,@pewartaanfungsihutan_tahun,@pewartaanfungsihutan_kodsukutahun,@pewartaanfungsihutan_kodhsk,@pewartaanfungsihutan_kodfungsihutan,@pewartaanfungsihutan_tarikhwarta,@pewartaanfungsihutan_nowarta,@pewartaanfungsihutan_nopelanwarta,@pewartaanfungsihutan_luaswartacadangan,@pewartaanfungsihutan_luaswartadiwarta,@pewartaanfungsihutan_luasdigitalcadangan,@pewartaanfungsihutan_luasdigitaldiwarta,@pewartaanfungsihutan_indikatorbertindih);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_koddaerah", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_tahun", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodsukutahun", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodhsk", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodfungsihutan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_kodfungsihutan", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_tarikhwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_tarikhwarta", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_nowarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_nowarta", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_nopelanwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_nopelanwarta", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartacadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartacadangan", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartadiwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luaswartadiwarta", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitalcadangan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitalcadangan", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitaldiwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_luasdigitaldiwarta", nilai014); }
                                    if (string.IsNullOrEmpty(nilai015)) { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_indikatorbertindih", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanfungsihutan_indikatorbertindih", nilai015); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[pewartaan_fungsi_hutan] SET [pewartaanfungsihutan_date_add] = @pewartaanfungsihutan_date_add,[pewartaanfungsihutan_date_view] = @pewartaanfungsihutan_date_view WHERE [pewartaanfungsihutan_recordid] = @pewartaanfungsihutan_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@pewartaanfungsihutan_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-pewartaan_fungsi_hutan-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-pewartaan_fungsi_hutan-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-pewartaan_fungsi_hutan-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-pewartaan_fungsi_hutan-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-pewartaan_fungsi_hutan-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //listBoxEsmashError.Items.Add("eSmash-pewartaan_fungsi_hutan-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "pewartaan_fungsi_hutan", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-pewartaan_fungsi_hutan-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_fungsi_hutan", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan
            //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan //controller_eSmashPewartaanFungsiHutan

            await Task.Delay(1);

            //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan
            //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `pewartaan_jenis_hutan`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["KodDaerah"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["Tahun"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodSukuTahun"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["KodHSK"].ToString(); string nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = null; } else { try { nilai006 = nilai006x.ToString(); } catch (Exception) { nilai006 = null; } }
                        string nilai007x = reader["TarikhWarta"].ToString(); string nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = null; } else { try { nilai007 = nilai007x.ToString(); } catch (Exception) { nilai007 = null; } }
                        string nilai008x = reader["NoWarta"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["NoPelanWarta"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["Catatan"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }
                        string nilai011x = reader["JH_01_LWarta"].ToString(); decimal nilai011; if (string.IsNullOrEmpty(nilai011x)) { nilai011 = 0; } else { try { nilai011 = decimal.Parse(nilai011x); } catch (Exception) { nilai011 = 0; } }
                        string nilai012x = reader["JH_01_LDigital"].ToString(); decimal nilai012; if (string.IsNullOrEmpty(nilai012x)) { nilai012 = 0; } else { try { nilai012 = decimal.Parse(nilai012x); } catch (Exception) { nilai012 = 0; } }
                        string nilai013x = reader["JH_02_LWarta"].ToString(); decimal nilai013; if (string.IsNullOrEmpty(nilai013x)) { nilai013 = 0; } else { try { nilai013 = decimal.Parse(nilai013x); } catch (Exception) { nilai013 = 0; } }
                        string nilai014x = reader["JH_02_LDigital"].ToString(); decimal nilai014; if (string.IsNullOrEmpty(nilai014x)) { nilai014 = 0; } else { try { nilai014 = decimal.Parse(nilai014x); } catch (Exception) { nilai014 = 0; } }
                        string nilai015x = reader["JH_03_LWarta"].ToString(); decimal nilai015; if (string.IsNullOrEmpty(nilai015x)) { nilai015 = 0; } else { try { nilai015 = decimal.Parse(nilai015x); } catch (Exception) { nilai015 = 0; } }
                        string nilai016x = reader["JH_03_LDigital"].ToString(); decimal nilai016; if (string.IsNullOrEmpty(nilai016x)) { nilai016 = 0; } else { try { nilai016 = decimal.Parse(nilai016x); } catch (Exception) { nilai016 = 0; } }
                        string nilai017x = reader["JH_04_LWarta"].ToString(); decimal nilai017; if (string.IsNullOrEmpty(nilai017x)) { nilai017 = 0; } else { try { nilai017 = decimal.Parse(nilai017x); } catch (Exception) { nilai017 = 0; } }
                        string nilai018x = reader["JH_04_LDigital"].ToString(); decimal nilai018; if (string.IsNullOrEmpty(nilai018x)) { nilai018 = 0; } else { try { nilai018 = decimal.Parse(nilai018x); } catch (Exception) { nilai018 = 0; } }
                        string nilai019x = reader["JenisRekod"].ToString(); string nilai019; if (string.IsNullOrEmpty(nilai019x)) { nilai019 = null; } else { try { nilai019 = nilai019x.ToString(); } catch (Exception) { nilai019 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[pewartaan_jenis_hutan] WHERE [pewartaanjenishutan_recordid] = @pewartaanjenishutan_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@pewartaanjenishutan_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["pewartaanjenishutan_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["pewartaanjenishutan_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["pewartaanjenishutan_koddaerah"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["pewartaanjenishutan_tahun"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["pewartaanjenishutan_kodsukutahun"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["pewartaanjenishutan_kodhsk"].ToString(); string checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = null; } else { try { checknilai006 = checknilai006x.ToString(); } catch (Exception) { checknilai006 = null; } }
                                    string checknilai007x = readerCheck01["pewartaanjenishutan_tarikhwarta"].ToString(); string checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = null; } else { try { checknilai007 = checknilai007x.ToString(); } catch (Exception) { checknilai007 = null; } }
                                    string checknilai008x = readerCheck01["pewartaanjenishutan_nowarta"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["pewartaanjenishutan_nopelanwarta"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["pewartaanjenishutan_catatan"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }
                                    string checknilai011x = readerCheck01["pewartaanjenishutan_jh_01_lwarta"].ToString(); decimal checknilai011; if (string.IsNullOrEmpty(checknilai011x)) { checknilai011 = 0; } else { try { checknilai011 = decimal.Parse(checknilai011x); } catch (Exception) { checknilai011 = 0; } }
                                    string checknilai012x = readerCheck01["pewartaanjenishutan_jh_01_ldigital"].ToString(); decimal checknilai012; if (string.IsNullOrEmpty(checknilai012x)) { checknilai012 = 0; } else { try { checknilai012 = decimal.Parse(checknilai012x); } catch (Exception) { checknilai012 = 0; } }
                                    string checknilai013x = readerCheck01["pewartaanjenishutan_jh_02_lwarta"].ToString(); decimal checknilai013; if (string.IsNullOrEmpty(checknilai013x)) { checknilai013 = 0; } else { try { checknilai013 = decimal.Parse(checknilai013x); } catch (Exception) { checknilai013 = 0; } }
                                    string checknilai014x = readerCheck01["pewartaanjenishutan_jh_02_ldigital"].ToString(); decimal checknilai014; if (string.IsNullOrEmpty(checknilai014x)) { checknilai014 = 0; } else { try { checknilai014 = decimal.Parse(checknilai014x); } catch (Exception) { checknilai014 = 0; } }
                                    string checknilai015x = readerCheck01["pewartaanjenishutan_jh_03_lwarta"].ToString(); decimal checknilai015; if (string.IsNullOrEmpty(checknilai015x)) { checknilai015 = 0; } else { try { checknilai015 = decimal.Parse(checknilai015x); } catch (Exception) { checknilai015 = 0; } }
                                    string checknilai016x = readerCheck01["pewartaanjenishutan_jh_03_ldigital"].ToString(); decimal checknilai016; if (string.IsNullOrEmpty(checknilai016x)) { checknilai016 = 0; } else { try { checknilai016 = decimal.Parse(checknilai016x); } catch (Exception) { checknilai016 = 0; } }
                                    string checknilai017x = readerCheck01["pewartaanjenishutan_jh_04_lwarta"].ToString(); decimal checknilai017; if (string.IsNullOrEmpty(checknilai017x)) { checknilai017 = 0; } else { try { checknilai017 = decimal.Parse(checknilai017x); } catch (Exception) { checknilai017 = 0; } }
                                    string checknilai018x = readerCheck01["pewartaanjenishutan_jh_04_ldigital"].ToString(); decimal checknilai018; if (string.IsNullOrEmpty(checknilai018x)) { checknilai018 = 0; } else { try { checknilai018 = decimal.Parse(checknilai018x); } catch (Exception) { checknilai018 = 0; } }
                                    string checknilai019x = readerCheck01["pewartaanjenishutan_jenisrekod"].ToString(); string checknilai019; if (string.IsNullOrEmpty(checknilai019x)) { checknilai019 = null; } else { try { checknilai019 = checknilai019x.ToString(); } catch (Exception) { checknilai019 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010 || checknilai011 != nilai011 || checknilai012 != nilai012 || checknilai013 != nilai013 || checknilai014 != nilai014 || checknilai015 != nilai015 || checknilai016 != nilai016 || checknilai017 != nilai017 || checknilai018 != nilai018 || checknilai019 != nilai019)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[pewartaan_jenis_hutan] SET [pewartaanjenishutan_kodnegeri]=@pewartaanjenishutan_kodnegeri,[pewartaanjenishutan_koddaerah]=@pewartaanjenishutan_koddaerah,[pewartaanjenishutan_tahun]=@pewartaanjenishutan_tahun,[pewartaanjenishutan_kodsukutahun]=@pewartaanjenishutan_kodsukutahun,[pewartaanjenishutan_kodhsk]=@pewartaanjenishutan_kodhsk,[pewartaanjenishutan_tarikhwarta]=@pewartaanjenishutan_tarikhwarta,[pewartaanjenishutan_nowarta]=@pewartaanjenishutan_nowarta,[pewartaanjenishutan_nopelanwarta]=@pewartaanjenishutan_nopelanwarta,[pewartaanjenishutan_catatan]=@pewartaanjenishutan_catatan,[pewartaanjenishutan_jh_01_lwarta]=@pewartaanjenishutan_jh_01_lwarta,[pewartaanjenishutan_jh_01_ldigital]=@pewartaanjenishutan_jh_01_ldigital,[pewartaanjenishutan_jh_02_lwarta]=@pewartaanjenishutan_jh_02_lwarta,[pewartaanjenishutan_jh_02_ldigital]=@pewartaanjenishutan_jh_02_ldigital,[pewartaanjenishutan_jh_03_lwarta]=@pewartaanjenishutan_jh_03_lwarta,[pewartaanjenishutan_jh_03_ldigital]=@pewartaanjenishutan_jh_03_ldigital,[pewartaanjenishutan_jh_04_lwarta]=@pewartaanjenishutan_jh_04_lwarta,[pewartaanjenishutan_jh_04_ldigital]=@pewartaanjenishutan_jh_04_ldigital,[pewartaanjenishutan_jenisrekod]=@pewartaanjenishutan_jenisrekod WHERE [pewartaanjenishutan_recordid]=@pewartaanjenishutan_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_koddaerah", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_koddaerah", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_tahun", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_kodsukutahun", nilai005); }
                                            if (string.IsNullOrEmpty(nilai006)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_kodhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_kodhsk", nilai006); }
                                            if (string.IsNullOrEmpty(nilai007)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_tarikhwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_tarikhwarta", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_nowarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_nowarta", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_nopelanwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_nopelanwarta", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_catatan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_catatan", nilai010); }
                                            if (nilai011 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_lwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_lwarta", nilai011); }
                                            if (nilai012 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_ldigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_ldigital", nilai012); }
                                            if (nilai013 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_lwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_lwarta", nilai013); }
                                            if (nilai014 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_ldigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_ldigital", nilai014); }
                                            if (nilai015 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_lwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_lwarta", nilai015); }
                                            if (nilai016 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_ldigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_ldigital", nilai016); }
                                            if (nilai017 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_lwarta", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_lwarta", nilai017); }
                                            if (nilai018 == 0) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_ldigital", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_ldigital", nilai018); }
                                            if (string.IsNullOrEmpty(nilai019)) { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jenisrekod", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@pewartaanjenishutan_jenisrekod", nilai019); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[pewartaan_jenis_hutan] SET [pewartaanjenishutan_date_update] = @pewartaanjenishutan_date_update,[pewartaanjenishutan_date_view] = @date_view WHERE [pewartaanjenishutan_recordid] = @pewartaanjenishutan_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-pewartaan_jenis_hutan-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }

                                            //listBoxEsmashStatus.Items.Add("eSmash-pewartaan_jenis_hutan-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-pewartaan_jenis_hutan-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[pewartaan_jenis_hutan] SET [pewartaanjenishutan_date_view] = @pewartaanjenishutan_date_view WHERE [pewartaanjenishutan_recordid] = @pewartaanjenishutan_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-pewartaan_jenis_hutan-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        ////listBoxEsmashStatus.Items.Add("eSmash-pewartaan_jenis_hutan-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[pewartaan_jenis_hutan] ([pewartaanjenishutan_recordid],[pewartaanjenishutan_kodnegeri],[pewartaanjenishutan_koddaerah],[pewartaanjenishutan_tahun],[pewartaanjenishutan_kodsukutahun],[pewartaanjenishutan_kodhsk],[pewartaanjenishutan_tarikhwarta],[pewartaanjenishutan_nowarta],[pewartaanjenishutan_nopelanwarta],[pewartaanjenishutan_catatan],[pewartaanjenishutan_jh_01_lwarta],[pewartaanjenishutan_jh_01_ldigital],[pewartaanjenishutan_jh_02_lwarta],[pewartaanjenishutan_jh_02_ldigital],[pewartaanjenishutan_jh_03_lwarta],[pewartaanjenishutan_jh_03_ldigital],[pewartaanjenishutan_jh_04_lwarta],[pewartaanjenishutan_jh_04_ldigital],[pewartaanjenishutan_jenisrekod]) VALUES(@pewartaanjenishutan_recordid,@pewartaanjenishutan_kodnegeri,@pewartaanjenishutan_koddaerah,@pewartaanjenishutan_tahun,@pewartaanjenishutan_kodsukutahun,@pewartaanjenishutan_kodhsk,@pewartaanjenishutan_tarikhwarta,@pewartaanjenishutan_nowarta,@pewartaanjenishutan_nopelanwarta,@pewartaanjenishutan_catatan,@pewartaanjenishutan_jh_01_lwarta,@pewartaanjenishutan_jh_01_ldigital,@pewartaanjenishutan_jh_02_lwarta,@pewartaanjenishutan_jh_02_ldigital,@pewartaanjenishutan_jh_03_lwarta,@pewartaanjenishutan_jh_03_ldigital,@pewartaanjenishutan_jh_04_lwarta,@pewartaanjenishutan_jh_04_ldigital,@pewartaanjenishutan_jenisrekod);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_koddaerah", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_koddaerah", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_tahun", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_kodsukutahun", nilai005); }
                                    if (string.IsNullOrEmpty(nilai006)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_kodhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_kodhsk", nilai006); }
                                    if (string.IsNullOrEmpty(nilai007)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_tarikhwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_tarikhwarta", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_nowarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_nowarta", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_nopelanwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_nopelanwarta", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_catatan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_catatan", nilai010); }
                                    if (nilai011 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_lwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_lwarta", nilai011); }
                                    if (nilai012 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_ldigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_01_ldigital", nilai012); }
                                    if (nilai013 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_lwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_lwarta", nilai013); }
                                    if (nilai014 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_ldigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_02_ldigital", nilai014); }
                                    if (nilai015 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_lwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_lwarta", nilai015); }
                                    if (nilai016 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_ldigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_03_ldigital", nilai016); }
                                    if (nilai017 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_lwarta", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_lwarta", nilai017); }
                                    if (nilai018 == 0) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_ldigital", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jh_04_ldigital", nilai018); }
                                    if (string.IsNullOrEmpty(nilai019)) { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jenisrekod", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@pewartaanjenishutan_jenisrekod", nilai019); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[pewartaan_jenis_hutan] SET [pewartaanjenishutan_date_add] = @pewartaanjenishutan_date_add,[pewartaanjenishutan_date_view] = @pewartaanjenishutan_date_view WHERE [pewartaanjenishutan_recordid] = @pewartaanjenishutan_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@pewartaanjenishutan_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-pewartaan_jenis_hutan-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-pewartaan_jenis_hutan-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-pewartaan_jenis_hutan-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-pewartaan_jenis_hutan-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-pewartaan_jenis_hutan-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-pewartaan_jenis_hutan-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "pewartaan_jenis_hutan", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-pewartaan_jenis_hutan-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "pewartaan_jenis_hutan", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan
            //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan //controller_eSmashPewartaanJenisHutan

            await Task.Delay(1);

            //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas
            //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas
            try
            {
                connMYSQLDB_eSmashSDB01 = new MySqlConnection(dis_mysql_esmashdb.ToString());
                string cmdText = "SELECT * FROM `tambahan_maklumat_asas`;";
                MySqlCommand cmd = new MySqlCommand(cmdText, connMYSQLDB_eSmashSDB01);
                connMYSQLDB_eSmashSDB01.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    int x = 1;
                    while (reader.Read())
                    {

                        string nilai001x = reader["RecordID"].ToString(); string nilai001; if (string.IsNullOrEmpty(nilai001x)) { nilai001 = null; } else { try { nilai001 = nilai001x.ToString(); } catch (Exception) { nilai001 = null; } }
                        string nilai002x = reader["KodNegeri"].ToString(); string nilai002; if (string.IsNullOrEmpty(nilai002x)) { nilai002 = null; } else { try { nilai002 = nilai002x.ToString(); } catch (Exception) { nilai002 = null; } }
                        string nilai003x = reader["Tahun"].ToString(); string nilai003; if (string.IsNullOrEmpty(nilai003x)) { nilai003 = null; } else { try { nilai003 = nilai003x.ToString(); } catch (Exception) { nilai003 = null; } }
                        string nilai004x = reader["KodSukuTahun"].ToString(); string nilai004; if (string.IsNullOrEmpty(nilai004x)) { nilai004 = null; } else { try { nilai004 = nilai004x.ToString(); } catch (Exception) { nilai004 = null; } }
                        string nilai005x = reader["KodJenisMaklumat"].ToString(); string nilai005; if (string.IsNullOrEmpty(nilai005x)) { nilai005 = null; } else { try { nilai005 = nilai005x.ToString(); } catch (Exception) { nilai005 = null; } }
                        string nilai006x = reader["LuasLuarHSK"].ToString(); decimal nilai006; if (string.IsNullOrEmpty(nilai006x)) { nilai006 = 0; } else { try { nilai006 = decimal.Parse(nilai006x); } catch (Exception) { nilai006 = 0; } }
                        string nilai007x = reader["LuasDalamHSK"].ToString(); decimal nilai007; if (string.IsNullOrEmpty(nilai007x)) { nilai007 = 0; } else { try { nilai007 = decimal.Parse(nilai007x); } catch (Exception) { nilai007 = 0; } }
                        string nilai008x = reader["Catatan"].ToString(); string nilai008; if (string.IsNullOrEmpty(nilai008x)) { nilai008 = null; } else { try { nilai008 = nilai008x.ToString(); } catch (Exception) { nilai008 = null; } }
                        string nilai009x = reader["KodMaklumatTambahan"].ToString(); string nilai009; if (string.IsNullOrEmpty(nilai009x)) { nilai009 = null; } else { try { nilai009 = nilai009x.ToString(); } catch (Exception) { nilai009 = null; } }
                        string nilai010x = reader["JenisRekod"].ToString(); string nilai010; if (string.IsNullOrEmpty(nilai010x)) { nilai010 = null; } else { try { nilai010 = nilai010x.ToString(); } catch (Exception) { nilai010 = null; } }

                        try
                        {
                            connMSSQLDB_eSmashSDB02 = new SqlConnection(dis_mssql_esmashdb.ToString());
                            string queryCheck01 = "SELECT * FROM [dbo].[tambahan_maklumat_asas] WHERE [tambahanmaklumatasas_recordid] = @tambahanmaklumatasas_recordid;";
                            SqlCommand cmdCheck01 = new SqlCommand(queryCheck01, connMSSQLDB_eSmashSDB02);
                            cmdCheck01.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", nilai001.ToString());

                            connMSSQLDB_eSmashSDB02.Open();
                            SqlDataReader readerCheck01 = cmdCheck01.ExecuteReader();
                            if (readerCheck01.HasRows)
                            {
                                while (readerCheck01.Read())
                                {

                                    string checknilai001x = readerCheck01["tambahanmaklumatasas_recordid"].ToString(); string checknilai001; if (string.IsNullOrEmpty(checknilai001x)) { checknilai001 = null; } else { try { checknilai001 = checknilai001x.ToString(); } catch (Exception) { checknilai001 = null; } }
                                    string checknilai002x = readerCheck01["tambahanmaklumatasas_kodnegeri"].ToString(); string checknilai002; if (string.IsNullOrEmpty(checknilai002x)) { checknilai002 = null; } else { try { checknilai002 = checknilai002x.ToString(); } catch (Exception) { checknilai002 = null; } }
                                    string checknilai003x = readerCheck01["tambahanmaklumatasas_tahun"].ToString(); string checknilai003; if (string.IsNullOrEmpty(checknilai003x)) { checknilai003 = null; } else { try { checknilai003 = checknilai003x.ToString(); } catch (Exception) { checknilai003 = null; } }
                                    string checknilai004x = readerCheck01["tambahanmaklumatasas_kodsukutahun"].ToString(); string checknilai004; if (string.IsNullOrEmpty(checknilai004x)) { checknilai004 = null; } else { try { checknilai004 = checknilai004x.ToString(); } catch (Exception) { checknilai004 = null; } }
                                    string checknilai005x = readerCheck01["tambahanmaklumatasas_kodjenismaklumat"].ToString(); string checknilai005; if (string.IsNullOrEmpty(checknilai005x)) { checknilai005 = null; } else { try { checknilai005 = checknilai005x.ToString(); } catch (Exception) { checknilai005 = null; } }
                                    string checknilai006x = readerCheck01["tambahanmaklumatasas_luasluarhsk"].ToString(); decimal checknilai006; if (string.IsNullOrEmpty(checknilai006x)) { checknilai006 = 0; } else { try { checknilai006 = decimal.Parse(checknilai006x); } catch (Exception) { checknilai006 = 0; } }
                                    string checknilai007x = readerCheck01["tambahanmaklumatasas_luasdalamhsk"].ToString(); decimal checknilai007; if (string.IsNullOrEmpty(checknilai007x)) { checknilai007 = 0; } else { try { checknilai007 = decimal.Parse(checknilai007x); } catch (Exception) { checknilai007 = 0; } }
                                    string checknilai008x = readerCheck01["tambahanmaklumatasas_catatan"].ToString(); string checknilai008; if (string.IsNullOrEmpty(checknilai008x)) { checknilai008 = null; } else { try { checknilai008 = checknilai008x.ToString(); } catch (Exception) { checknilai008 = null; } }
                                    string checknilai009x = readerCheck01["tambahanmaklumatasas_kodmaklumattambahan"].ToString(); string checknilai009; if (string.IsNullOrEmpty(checknilai009x)) { checknilai009 = null; } else { try { checknilai009 = checknilai009x.ToString(); } catch (Exception) { checknilai009 = null; } }
                                    string checknilai010x = readerCheck01["tambahanmaklumatasas_jenisrekod"].ToString(); string checknilai010; if (string.IsNullOrEmpty(checknilai010x)) { checknilai010 = null; } else { try { checknilai010 = checknilai010x.ToString(); } catch (Exception) { checknilai010 = null; } }

                                    if (checknilai001 != nilai001 || checknilai002 != nilai002 || checknilai003 != nilai003 || checknilai004 != nilai004 || checknilai005 != nilai005 || checknilai006 != nilai006 || checknilai007 != nilai007 || checknilai008 != nilai008 || checknilai009 != nilai009 || checknilai010 != nilai010)
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryUpdate01 = "UPDATE [dbo].[tambahan_maklumat_asas] SET [tambahanmaklumatasas_kodnegeri]=@tambahanmaklumatasas_kodnegeri,[tambahanmaklumatasas_tahun]=@tambahanmaklumatasas_tahun,[tambahanmaklumatasas_kodsukutahun]=@tambahanmaklumatasas_kodsukutahun,[tambahanmaklumatasas_kodjenismaklumat]=@tambahanmaklumatasas_kodjenismaklumat,[tambahanmaklumatasas_luasluarhsk]=@tambahanmaklumatasas_luasluarhsk,[tambahanmaklumatasas_luasdalamhsk]=@tambahanmaklumatasas_luasdalamhsk,[tambahanmaklumatasas_catatan]=@tambahanmaklumatasas_catatan,[tambahanmaklumatasas_kodmaklumattambahan]=@tambahanmaklumatasas_kodmaklumattambahan,[tambahanmaklumatasas_jenisrekod]=@tambahanmaklumatasas_jenisrekod WHERE [tambahanmaklumatasas_recordid]=@tambahanmaklumatasas_recordid;";
                                            SqlCommand cmdUpdate01 = new SqlCommand(queryUpdate01, connMSSQLDB_eSmashSDB01);

                                            if (string.IsNullOrEmpty(nilai001)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", nilai001); }
                                            if (string.IsNullOrEmpty(nilai002)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodnegeri", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodnegeri", nilai002); }
                                            if (string.IsNullOrEmpty(nilai003)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_tahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_tahun", nilai003); }
                                            if (string.IsNullOrEmpty(nilai004)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodsukutahun", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodsukutahun", nilai004); }
                                            if (string.IsNullOrEmpty(nilai005)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodjenismaklumat", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodjenismaklumat", nilai005); }
                                            if (nilai006 == 0) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_luasluarhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_luasluarhsk", nilai006); }
                                            if (nilai007 == 0) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_luasdalamhsk", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_luasdalamhsk", nilai007); }
                                            if (string.IsNullOrEmpty(nilai008)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_catatan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_catatan", nilai008); }
                                            if (string.IsNullOrEmpty(nilai009)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodmaklumattambahan", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_kodmaklumattambahan", nilai009); }
                                            if (string.IsNullOrEmpty(nilai010)) { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_jenisrekod", DBNull.Value); } else { cmdUpdate01.Parameters.AddWithValue("@tambahanmaklumatasas_jenisrekod", nilai010); }

                                            connMSSQLDB_eSmashSDB01.Open();
                                            cmdUpdate01.ExecuteNonQuery();

                                            try
                                            {
                                                connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                                string queryInsert0X = "UPDATE [dbo].[tambahan_maklumat_asas] SET [tambahanmaklumatasas_date_update] = @tambahanmaklumatasas_date_update,[tambahanmaklumatasas_date_view] = @tambahanmaklumatasas_date_view WHERE [tambahanmaklumatasas_recordid] = @tambahanmaklumatasas_recordid;";
                                                SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                                cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", nilai001);
                                                cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_date_update", DateTime.Now);
                                                cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_date_view", DateTime.Now);
                                                connMSSQLDB_eSmashDB0X.Open();
                                                cmdInsert0X.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", nilai001, "error-date-update", ex.ToString());
                                                //listBoxEsmashError.Items.Add("eSmash-tambahan_maklumat_asas-DateUpdate-" + ex.ToString());
                                                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", nilai001, null, "error", "error-date-update", ex.Message.ToString(), ex.ToString());

                                            }
                                            finally
                                            {
                                                connMSSQLDB_eSmashDB0X.Close();
                                            }
                                            //listBoxEsmashStatus.Items.Add("eSmash-tambahan_maklumat_asas-Update-Done-" + nilai001.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", nilai001, "error-update-update", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-tambahan_maklumat_asas-ErrorUpdate-" + nilai001 + " - " + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", nilai001, null, "error", "error-update-update", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashSDB01.Close();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                            string queryInsert0X = "UPDATE [dbo].[tambahan_maklumat_asas] SET [tambahanmaklumatasas_date_view] = @tambahanmaklumatasas_date_view WHERE [tambahanmaklumatasas_recordid] = @tambahanmaklumatasas_recordid;";
                                            SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                            cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", nilai001);
                                            cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_date_view", DateTime.Now);

                                            connMSSQLDB_eSmashDB0X.Open();
                                            cmdInsert0X.ExecuteNonQuery();
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", nilai001, "error-date-view", ex.ToString());
                                            //listBoxEsmashError.Items.Add("eSmash-tambahan_maklumat_asas-DateView-" + ex.ToString());
                                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", nilai001, null, "error", "error-date-view", ex.Message.ToString(), ex.ToString());

                                        }
                                        finally
                                        {
                                            connMSSQLDB_eSmashDB0X.Close();
                                        }
                                        ////listBoxEsmashStatus.Items.Add("eSmash-tambahan_maklumat_asas-Same-" + nilai001);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    connMSSQLDB_eSmashSDB01 = new SqlConnection(dis_mssql_esmashdb.ToString());
                                    string queryInsert01 = "INSERT INTO [dbo].[tambahan_maklumat_asas] ([tambahanmaklumatasas_recordid],[tambahanmaklumatasas_kodnegeri],[tambahanmaklumatasas_tahun],[tambahanmaklumatasas_kodsukutahun],[tambahanmaklumatasas_kodjenismaklumat],[tambahanmaklumatasas_luasluarhsk],[tambahanmaklumatasas_luasdalamhsk],[tambahanmaklumatasas_catatan],[tambahanmaklumatasas_kodmaklumattambahan],[tambahanmaklumatasas_jenisrekod]) VALUES(@tambahanmaklumatasas_recordid,@tambahanmaklumatasas_kodnegeri,@tambahanmaklumatasas_tahun,@tambahanmaklumatasas_kodsukutahun,@tambahanmaklumatasas_kodjenismaklumat,@tambahanmaklumatasas_luasluarhsk,@tambahanmaklumatasas_luasdalamhsk,@tambahanmaklumatasas_catatan,@tambahanmaklumatasas_kodmaklumattambahan,@tambahanmaklumatasas_jenisrekod);";
                                    SqlCommand cmdInsert01 = new SqlCommand(queryInsert01, connMSSQLDB_eSmashSDB01);

                                    if (string.IsNullOrEmpty(nilai001)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", nilai001); }
                                    if (string.IsNullOrEmpty(nilai002)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodnegeri", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodnegeri", nilai002); }
                                    if (string.IsNullOrEmpty(nilai003)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_tahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_tahun", nilai003); }
                                    if (string.IsNullOrEmpty(nilai004)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodsukutahun", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodsukutahun", nilai004); }
                                    if (string.IsNullOrEmpty(nilai005)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodjenismaklumat", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodjenismaklumat", nilai005); }
                                    if (nilai006 == 0) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_luasluarhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_luasluarhsk", nilai006); }
                                    if (nilai007 == 0) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_luasdalamhsk", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_luasdalamhsk", nilai007); }
                                    if (string.IsNullOrEmpty(nilai008)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_catatan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_catatan", nilai008); }
                                    if (string.IsNullOrEmpty(nilai009)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodmaklumattambahan", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_kodmaklumattambahan", nilai009); }
                                    if (string.IsNullOrEmpty(nilai010)) { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_jenisrekod", DBNull.Value); } else { cmdInsert01.Parameters.AddWithValue("@tambahanmaklumatasas_jenisrekod", nilai010); }

                                    connMSSQLDB_eSmashSDB01.Open();
                                    cmdInsert01.ExecuteNonQuery();

                                    try
                                    {
                                        connMSSQLDB_eSmashDB0X = new SqlConnection(dis_mssql_esmashdb.ToString());
                                        string queryInsert0X = "UPDATE [dbo].[tambahan_maklumat_asas] SET [tambahanmaklumatasas_date_add] = @tambahanmaklumatasas_date_add,[tambahanmaklumatasas_date_view] = @tambahanmaklumatasas_date_view WHERE [tambahanmaklumatasas_recordid] = @tambahanmaklumatasas_recordid;";
                                        SqlCommand cmdInsert0X = new SqlCommand(queryInsert0X, connMSSQLDB_eSmashDB0X);
                                        cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_recordid", nilai001);
                                        cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_date_add", DateTime.Now);
                                        cmdInsert0X.Parameters.AddWithValue("@tambahanmaklumatasas_date_view", DateTime.Now);

                                        connMSSQLDB_eSmashDB0X.Open();
                                        cmdInsert0X.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        // syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", nilai001, "error-date-add", ex.ToString());
                                        //listBoxEsmashError.Items.Add("eSmash-tambahan_maklumat_asas-DateAdd-" + ex.ToString());
                                        await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", nilai001, null, "error", "error-date-add", ex.Message.ToString(), ex.ToString());

                                    }
                                    finally
                                    {
                                        connMSSQLDB_eSmashDB0X.Close();
                                    }
                                    //listBoxEsmashStatus.Items.Add("eSmash-tambahan_maklumat_asas-Insert-Done-" + nilai001.ToString());
                                }

                                catch (Exception ex)
                                {
                                    //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", nilai001, "error-insert-insert-", ex.ToString());
                                    //listBoxEsmashError.Items.Add("eSmash-tambahan_maklumat_asas-Insert-Error-" + ex.ToString());
                                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", nilai001, null, "error", "error-insert-insert", ex.Message.ToString(), ex.ToString());

                                }
                                finally
                                {
                                    connMSSQLDB_eSmashSDB01.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", nilai001, "error-second-second-", ex.ToString());
                            //listBoxEsmashError.Items.Add("eSmash-tambahan_maklumat_asas-Second-Error-" + ex.ToString());
                            await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", nilai001, null, "error", "error-second-second", ex.Message.ToString(), ex.ToString());

                        }
                        finally
                        {
                            connMSSQLDB_eSmashSDB02.Close();
                        }
                    }
                }
                else
                {
                    //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", null, "nodata", "nodata");
                    //listBoxEsmashFinalStatus.Items.Add("eSmash-tambahan_maklumat_asas-Main-No Data!");
                    await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", null, null, "empty", "empty-nodata", "Tiada Data", null);

                }
            }
            catch (Exception ex)
            {
                //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", null, "error-first-first-", ex.ToString());
                //listBoxEsmashError.Items.Add("eSmash-tambahan_maklumat_asas-Main-Error-" + ex.ToString());
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", null, null, "error", "error-first-first", ex.Message.ToString(), ex.ToString());

            }
            finally
            {
                connMYSQLDB_eSmashSDB01.Close();
                //await syssts.StatusSystem_Integration("esmash", "tambahan_maklumat_asas", null, "done", "done");
                //listBoxEsmashFinalStatus.Items.Add("eSmash-tambahan_maklumat_asas-Main-Done");
                await syssts.StatusSystem_Integration2(sistem_servername, "esmash", "tambahan_maklumat_asas", null, null, "done", "DONE_SYSTEMINTEGRATION_ESMASH", null, null);

            }
            //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas
            //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas //controller_eSmashTambahanMaklumatAsas

            await Task.Delay(10);
        }
    }
}
