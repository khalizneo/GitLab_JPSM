﻿namespace DISJPSM_Integration
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlConnection = new System.Windows.Forms.TabControl();
            this.tabPageProduction = new System.Windows.Forms.TabPage();
            this.tabPageDevelopment = new System.Windows.Forms.TabPage();
            this.txtProdServer = new System.Windows.Forms.TextBox();
            this.txtProdDatabase = new System.Windows.Forms.TextBox();
            this.txtProdUserName = new System.Windows.Forms.TextBox();
            this.txtProdUserPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProdServerFull = new System.Windows.Forms.TextBox();
            this.txtDevServerFull = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDevUserPassword = new System.Windows.Forms.TextBox();
            this.txtDevUserName = new System.Windows.Forms.TextBox();
            this.txtDevDatabase = new System.Windows.Forms.TextBox();
            this.txtDevServer = new System.Windows.Forms.TextBox();
            this.btnConnSave = new System.Windows.Forms.Button();
            this.btnConnBack = new System.Windows.Forms.Button();
            this.tabControlConnection.SuspendLayout();
            this.tabPageProduction.SuspendLayout();
            this.tabPageDevelopment.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlConnection
            // 
            this.tabControlConnection.Controls.Add(this.tabPageProduction);
            this.tabControlConnection.Controls.Add(this.tabPageDevelopment);
            this.tabControlConnection.Location = new System.Drawing.Point(12, 12);
            this.tabControlConnection.Name = "tabControlConnection";
            this.tabControlConnection.SelectedIndex = 0;
            this.tabControlConnection.Size = new System.Drawing.Size(605, 214);
            this.tabControlConnection.TabIndex = 8;
            // 
            // tabPageProduction
            // 
            this.tabPageProduction.Controls.Add(this.txtProdServerFull);
            this.tabPageProduction.Controls.Add(this.label5);
            this.tabPageProduction.Controls.Add(this.label4);
            this.tabPageProduction.Controls.Add(this.label3);
            this.tabPageProduction.Controls.Add(this.label2);
            this.tabPageProduction.Controls.Add(this.label1);
            this.tabPageProduction.Controls.Add(this.txtProdUserPassword);
            this.tabPageProduction.Controls.Add(this.txtProdUserName);
            this.tabPageProduction.Controls.Add(this.txtProdDatabase);
            this.tabPageProduction.Controls.Add(this.txtProdServer);
            this.tabPageProduction.Location = new System.Drawing.Point(4, 22);
            this.tabPageProduction.Name = "tabPageProduction";
            this.tabPageProduction.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageProduction.Size = new System.Drawing.Size(597, 188);
            this.tabPageProduction.TabIndex = 0;
            this.tabPageProduction.Text = "Production";
            this.tabPageProduction.UseVisualStyleBackColor = true;
            // 
            // tabPageDevelopment
            // 
            this.tabPageDevelopment.Controls.Add(this.txtDevServerFull);
            this.tabPageDevelopment.Controls.Add(this.label6);
            this.tabPageDevelopment.Controls.Add(this.label7);
            this.tabPageDevelopment.Controls.Add(this.label8);
            this.tabPageDevelopment.Controls.Add(this.label9);
            this.tabPageDevelopment.Controls.Add(this.label10);
            this.tabPageDevelopment.Controls.Add(this.txtDevUserPassword);
            this.tabPageDevelopment.Controls.Add(this.txtDevUserName);
            this.tabPageDevelopment.Controls.Add(this.txtDevDatabase);
            this.tabPageDevelopment.Controls.Add(this.txtDevServer);
            this.tabPageDevelopment.Location = new System.Drawing.Point(4, 22);
            this.tabPageDevelopment.Name = "tabPageDevelopment";
            this.tabPageDevelopment.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDevelopment.Size = new System.Drawing.Size(597, 188);
            this.tabPageDevelopment.TabIndex = 1;
            this.tabPageDevelopment.Text = "Development";
            this.tabPageDevelopment.UseVisualStyleBackColor = true;
            // 
            // txtProdServer
            // 
            this.txtProdServer.Location = new System.Drawing.Point(87, 17);
            this.txtProdServer.Name = "txtProdServer";
            this.txtProdServer.Size = new System.Drawing.Size(500, 20);
            this.txtProdServer.TabIndex = 0;
            // 
            // txtProdDatabase
            // 
            this.txtProdDatabase.Location = new System.Drawing.Point(87, 43);
            this.txtProdDatabase.Name = "txtProdDatabase";
            this.txtProdDatabase.Size = new System.Drawing.Size(500, 20);
            this.txtProdDatabase.TabIndex = 1;
            // 
            // txtProdUserName
            // 
            this.txtProdUserName.Location = new System.Drawing.Point(87, 69);
            this.txtProdUserName.Name = "txtProdUserName";
            this.txtProdUserName.Size = new System.Drawing.Size(500, 20);
            this.txtProdUserName.TabIndex = 2;
            // 
            // txtProdUserPassword
            // 
            this.txtProdUserPassword.Location = new System.Drawing.Point(87, 95);
            this.txtProdUserPassword.Name = "txtProdUserPassword";
            this.txtProdUserPassword.PasswordChar = '●';
            this.txtProdUserPassword.Size = new System.Drawing.Size(500, 20);
            this.txtProdUserPassword.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Database";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "UserName";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "UserPassword";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Full Connection String";
            // 
            // txtProdServerFull
            // 
            this.txtProdServerFull.Location = new System.Drawing.Point(9, 152);
            this.txtProdServerFull.Name = "txtProdServerFull";
            this.txtProdServerFull.Size = new System.Drawing.Size(578, 20);
            this.txtProdServerFull.TabIndex = 9;
            // 
            // txtDevServerFull
            // 
            this.txtDevServerFull.Location = new System.Drawing.Point(9, 152);
            this.txtDevServerFull.Name = "txtDevServerFull";
            this.txtDevServerFull.Size = new System.Drawing.Size(578, 20);
            this.txtDevServerFull.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Full Connection String";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "UserPassword";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "UserName";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Database";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Server";
            // 
            // txtDevUserPassword
            // 
            this.txtDevUserPassword.Location = new System.Drawing.Point(87, 95);
            this.txtDevUserPassword.Name = "txtDevUserPassword";
            this.txtDevUserPassword.PasswordChar = '●';
            this.txtDevUserPassword.Size = new System.Drawing.Size(500, 20);
            this.txtDevUserPassword.TabIndex = 13;
            // 
            // txtDevUserName
            // 
            this.txtDevUserName.Location = new System.Drawing.Point(87, 69);
            this.txtDevUserName.Name = "txtDevUserName";
            this.txtDevUserName.Size = new System.Drawing.Size(500, 20);
            this.txtDevUserName.TabIndex = 12;
            // 
            // txtDevDatabase
            // 
            this.txtDevDatabase.Location = new System.Drawing.Point(87, 43);
            this.txtDevDatabase.Name = "txtDevDatabase";
            this.txtDevDatabase.Size = new System.Drawing.Size(500, 20);
            this.txtDevDatabase.TabIndex = 11;
            // 
            // txtDevServer
            // 
            this.txtDevServer.Location = new System.Drawing.Point(87, 17);
            this.txtDevServer.Name = "txtDevServer";
            this.txtDevServer.Size = new System.Drawing.Size(500, 20);
            this.txtDevServer.TabIndex = 10;
            // 
            // btnConnSave
            // 
            this.btnConnSave.Location = new System.Drawing.Point(12, 232);
            this.btnConnSave.Name = "btnConnSave";
            this.btnConnSave.Size = new System.Drawing.Size(280, 40);
            this.btnConnSave.TabIndex = 9;
            this.btnConnSave.Text = "Save";
            this.btnConnSave.UseVisualStyleBackColor = true;
            this.btnConnSave.Click += new System.EventHandler(this.btnConnSave_Click);
            // 
            // btnConnBack
            // 
            this.btnConnBack.Location = new System.Drawing.Point(336, 232);
            this.btnConnBack.Name = "btnConnBack";
            this.btnConnBack.Size = new System.Drawing.Size(280, 40);
            this.btnConnBack.TabIndex = 10;
            this.btnConnBack.Text = "Back";
            this.btnConnBack.UseVisualStyleBackColor = true;
            this.btnConnBack.Click += new System.EventHandler(this.btnConnBack_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 285);
            this.Controls.Add(this.btnConnBack);
            this.Controls.Add(this.btnConnSave);
            this.Controls.Add(this.tabControlConnection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Connection Configuration";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tabControlConnection.ResumeLayout(false);
            this.tabPageProduction.ResumeLayout(false);
            this.tabPageProduction.PerformLayout();
            this.tabPageDevelopment.ResumeLayout(false);
            this.tabPageDevelopment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlConnection;
        private System.Windows.Forms.TabPage tabPageProduction;
        private System.Windows.Forms.TabPage tabPageDevelopment;
        private System.Windows.Forms.TextBox txtProdServerFull;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProdUserPassword;
        private System.Windows.Forms.TextBox txtProdUserName;
        private System.Windows.Forms.TextBox txtProdDatabase;
        private System.Windows.Forms.TextBox txtProdServer;
        private System.Windows.Forms.TextBox txtDevServerFull;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDevUserPassword;
        private System.Windows.Forms.TextBox txtDevUserName;
        private System.Windows.Forms.TextBox txtDevDatabase;
        private System.Windows.Forms.TextBox txtDevServer;
        private System.Windows.Forms.Button btnConnSave;
        private System.Windows.Forms.Button btnConnBack;
    }
}