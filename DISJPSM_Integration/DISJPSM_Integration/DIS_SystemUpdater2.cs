﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISJPSM_Integration
{
    class DIS_SystemUpdater2
    {
        public string sistem_suasana = Properties.Settings.Default.sistem_suasana;
        public string sistem_servername;
        public string sistem_database;
        public string sistem_username;
        public string sistem_password;
        public string sistem_suasana2;

        public int totalvalue_sml01;
        public int totalvalue_sml02;

        public int totalvalue_esmash01;
        public int totalvalue_esmash02;

        public int totalvalue_pgdp01;
        public int totalvalue_pgdp02;

        public string nilaiserver;
        public string nilaidatabase;
        public string nilaiusername;
        public string nilaipassword;

        public string nilaidatabase01;
        public string nilaiscema01;
        public string nilaitable01;
        public string nilaimain01;
        public string nilaimaintype01;
        public string nilaisec01;
        public string nilaisectype01;

        public string nilaidatabase02;
        public string nilaiscema02;
        public string nilaitable02;
        public string nilaimain02;
        public string nilaimaintype02;
        public string nilaisec02;
        public string nilaisectype02;

        public string nilaiupdaterselect_01;
        public string nilaiupdaterselect_02;

        public string error_updatef = null; public string error_updatef1 = null; public string error_updatef2 = null; public string error_updatef3 = null; public string error_updatef4 = null;
        public string error_selectf = null; public string error_selectf1 = null; public string error_selectf2 = null; public string error_selectf3 = null; public string error_selectf4 = null;
        public string error_selecttbl = null; public string error_selecttbl1 = null; public string error_selecttbl2 = null; public string error_selecttbl3 = null; public string error_selecttbl4 = null;
        public string error_selectdb = null; public string error_selectdb1 = null; public string error_selectdb2 = null; public string error_selectdb3 = null; public string error_selectdb4 = null;

        public SqlConnectionStringBuilder build = new SqlConnectionStringBuilder();
        public SqlConnection conn = new SqlConnection();

        public MySqlConnectionStringBuilder dis_mysql_smldb = new MySqlConnectionStringBuilder();
        public SqlConnectionStringBuilder dis_mssql_smldb = new SqlConnectionStringBuilder();

        public MySqlConnectionStringBuilder dis_mysql_esmashdb = new MySqlConnectionStringBuilder();
        public SqlConnectionStringBuilder dis_mssql_esmashdb = new SqlConnectionStringBuilder();

        public SqlConnection conndis_sdb2 = new SqlConnection();

        public SqlConnectionStringBuilder dis_pgdp01 = new SqlConnectionStringBuilder();
        public SqlConnection disconn_pgdp01 = new SqlConnection();

        public SqlConnectionStringBuilder dis_updaterselect = new SqlConnectionStringBuilder();
        public SqlConnection conndis_updaterselect = new SqlConnection();

        public SqlConnectionStringBuilder dis_updaterupdate = new SqlConnectionStringBuilder();
        public SqlConnection conndis_updaterupdate = new SqlConnection();
        


        private async Task initialconnection()
        {
            sistem_suasana = Properties.Settings.Default.sistem_suasana;

            if (sistem_suasana == "prod")
            {
                sistem_servername = Properties.Settings.Default.prodmssql_sdb_server;
                sistem_database = Properties.Settings.Default.prodmssql_sdb_database;
                sistem_username = Properties.Settings.Default.prodmssql_sdb_username;
                sistem_password = Properties.Settings.Default.prodmssql_sdb_userpassword;
                sistem_suasana2 = "production";
            }
            else if (sistem_suasana == "dev")
            {
                sistem_servername = Properties.Settings.Default.devmssql_sdb_server;
                sistem_database = Properties.Settings.Default.devmssql_sdb_database;
                sistem_username = Properties.Settings.Default.devmssql_sdb_username;
                sistem_password = Properties.Settings.Default.devmssql_sdb_userpassword;
                sistem_suasana2 = "development";
            }
            else if (sistem_suasana == "dev2")
            {
                sistem_servername = Properties.Settings.Default.dev2mssql_sdb_server;
                sistem_database = Properties.Settings.Default.dev2mssql_sdb_database;
                sistem_username = Properties.Settings.Default.dev2mssql_sdb_username;
                sistem_password = Properties.Settings.Default.dev2mssql_sdb_userpassword;
                sistem_suasana2 = "development2";
            }

            build.DataSource = sistem_servername;
            build.InitialCatalog = sistem_database;
            build.UserID = sistem_username;
            build.Password = sistem_password;
            build.PersistSecurityInfo = true;

            conn = new SqlConnection(build.ToString());
            string queryCheckSDB = "SELECT * FROM [dis_database_connection];";
            SqlCommand cmdCheckSDB = new SqlCommand(queryCheckSDB, conn);

            conn.Open();
            SqlDataReader readerCheckSDB = cmdCheckSDB.ExecuteReader();
            if (readerCheckSDB.HasRows)
            {
                while (readerCheckSDB.Read())
                {
                    if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mysql" && readerCheckSDB["dbconn_servername"].ToString() == "sml")
                    {
                        dis_mysql_smldb.Server = readerCheckSDB["dbconn_server"].ToString();
                        dis_mysql_smldb.Database = readerCheckSDB["dbconn_database"].ToString();
                        dis_mysql_smldb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mysql_smldb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mysql_smldb.PersistSecurityInfo = true;
                        dis_mysql_smldb.ConvertZeroDateTime = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mssql" && readerCheckSDB["dbconn_servername"].ToString() == "sml")
                    {
                        dis_mssql_smldb.DataSource = readerCheckSDB["dbconn_server"].ToString();
                        dis_mssql_smldb.InitialCatalog = readerCheckSDB["dbconn_database"].ToString();
                        dis_mssql_smldb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mssql_smldb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mssql_smldb.PersistSecurityInfo = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mysql" && readerCheckSDB["dbconn_servername"].ToString() == "esmash")
                    {
                        dis_mysql_esmashdb.Server = readerCheckSDB["dbconn_server"].ToString();
                        dis_mysql_esmashdb.Database = readerCheckSDB["dbconn_database"].ToString();
                        dis_mysql_esmashdb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mysql_esmashdb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mysql_esmashdb.PersistSecurityInfo = true;
                        dis_mysql_esmashdb.ConvertZeroDateTime = true;
                    }
                    else if (readerCheckSDB["dbconn_serverenvironment"].ToString() == sistem_suasana2 && readerCheckSDB["dbconn_servertype"].ToString() == "mssql" && readerCheckSDB["dbconn_servername"].ToString() == "esmash")
                    {
                        dis_mssql_esmashdb.DataSource = readerCheckSDB["dbconn_server"].ToString();
                        dis_mssql_esmashdb.InitialCatalog = readerCheckSDB["dbconn_database"].ToString();
                        dis_mssql_esmashdb.UserID = readerCheckSDB["dbconn_username"].ToString();
                        dis_mssql_esmashdb.Password = readerCheckSDB["dbconn_userpassword"].ToString();
                        dis_mssql_esmashdb.PersistSecurityInfo = true;
                    }
                }
            }
            else
            {
            }
            await Task.Delay(1);
        }
        public async Task updater_pgdp() {
            var syssts = new DIS_StatusSystem();
            totalvalue_pgdp01 = 0;

            await initialconnection();
            try
            {
                //get database
                conndis_sdb2 = new SqlConnection(build.ToString());
                string cmdTextdis_sdb2 = "SELECT [dbconn_server],[dbconn_database],[dbconn_username],[dbconn_userpassword] FROM [dis_database_connection] WHERE [dbconn_servername] = @dbconn_servername AND [dbconn_serverenvironment] = @dbconn_serverenvironment GROUP BY [dbconn_server],[dbconn_database],[dbconn_username],[dbconn_userpassword];";
                SqlCommand cmddis_sdb2 = new SqlCommand(cmdTextdis_sdb2, conndis_sdb2);
                cmddis_sdb2.Parameters.AddWithValue("@dbconn_servername", "pdgp");
                cmddis_sdb2.Parameters.AddWithValue("@dbconn_serverenvironment", sistem_suasana2);
                conndis_sdb2.Open();
                SqlDataReader reader_sdb2 = cmddis_sdb2.ExecuteReader();
                if (reader_sdb2.HasRows)
                {
                    while (reader_sdb2.Read())
                    {
                        nilaiserver = reader_sdb2["dbconn_server"].ToString();
                        nilaidatabase = reader_sdb2["dbconn_database"].ToString();
                        nilaiusername = reader_sdb2["dbconn_username"].ToString();
                        nilaipassword = reader_sdb2["dbconn_userpassword"].ToString();

                        try
                        {
                            disconn_pgdp01 = new SqlConnection(build.ToString());
                            string cmdTextdis_sdb = "SELECT * FROM [dbo].[dis_system_updater_all] WHERE [updaterall_database_02] = @updaterall_database_02;";
                            SqlCommand cmddis_sdb = new SqlCommand(cmdTextdis_sdb, disconn_pgdp01);
                            cmddis_sdb.Parameters.AddWithValue("@updaterall_database_02", nilaidatabase);
                            disconn_pgdp01.Open();
                            SqlDataReader reader_sdb = cmddis_sdb.ExecuteReader();
                            if (reader_sdb.HasRows)
                            {
                                while (reader_sdb.Read())
                                {
                                    nilaidatabase01 = reader_sdb["updaterall_database_01"].ToString();
                                    nilaiscema01 = reader_sdb["updaterall_scema_01"].ToString();
                                    nilaitable01 = reader_sdb["updaterall_table_01"].ToString();
                                    nilaimain01 = reader_sdb["updaterall_fieldkey_01"].ToString();
                                    nilaimaintype01 = reader_sdb["updaterall_fieldkeytype_01"].ToString();
                                    nilaisec01 = reader_sdb["updaterall_fieldname_01"].ToString();
                                    nilaisectype01 = reader_sdb["updaterall_fieldtype_01"].ToString();

                                    nilaidatabase02 = reader_sdb["updaterall_database_02"].ToString();
                                    nilaiscema02 = reader_sdb["updaterall_scema_02"].ToString();
                                    nilaitable02 = reader_sdb["updaterall_table_02"].ToString();
                                    nilaimain02 = reader_sdb["updaterall_fieldkey_02"].ToString();
                                    nilaimaintype02 = reader_sdb["updaterall_fieldkeytype_02"].ToString();
                                    nilaisec02 = reader_sdb["updaterall_fieldname_02"].ToString();
                                    nilaisectype02 = reader_sdb["updaterall_fieldtype_02"].ToString();


                                    try
                                    {

                                        dis_updaterselect.DataSource = sistem_servername;
                                        dis_updaterselect.InitialCatalog = nilaidatabase01;
                                        dis_updaterselect.UserID = sistem_username;
                                        dis_updaterselect.Password = sistem_password;
                                        dis_updaterselect.PersistSecurityInfo = true;

                                        conndis_updaterselect = new SqlConnection(dis_updaterselect.ToString());
                                        string cmdTextdis_updaterselect = "SELECT [" + nilaimain01 + "],[" + nilaisec01 + "] FROM [" + nilaiscema01 + "].[" + nilaitable01 + "];";
                                        SqlCommand cmddis_updaterselect = new SqlCommand(cmdTextdis_updaterselect, conndis_updaterselect);
                                        conndis_updaterselect.Open();
                                        SqlDataReader reader_updaterselect = cmddis_updaterselect.ExecuteReader();
                                        if (reader_updaterselect.HasRows)
                                        {
                                            while (reader_updaterselect.Read())
                                            {
                                                nilaiupdaterselect_01 = reader_updaterselect[nilaimain01].ToString();
                                                nilaiupdaterselect_02 = reader_updaterselect[nilaisec01].ToString();
                                                if (string.IsNullOrEmpty(nilaiupdaterselect_02) || nilaiupdaterselect_02 == "-" || nilaiupdaterselect_02 == " " || nilaiupdaterselect_02 == "")
                                                {

                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        dis_updaterupdate.DataSource = nilaiserver;
                                                        dis_updaterupdate.InitialCatalog = nilaidatabase;
                                                        dis_updaterupdate.UserID = nilaiusername;
                                                        dis_updaterupdate.Password = nilaipassword;
                                                        dis_updaterupdate.PersistSecurityInfo = true;

                                                        conndis_updaterupdate = new SqlConnection(dis_updaterupdate.ToString());
                                                        string query_updaterupdate = "UPDATE [" + nilaiscema02 + "].[" + nilaitable02 + "] SET [" + nilaisec02 + "]=@" + nilaisec02.ToLower() + " WHERE [" + nilaimain02 + "]=@" + nilaimain02.ToLower() + ";";
                                                        SqlCommand cmddis_updaterupdate = new SqlCommand(query_updaterupdate, conndis_updaterupdate);

                                                        string field_main = "@" + nilaimain02.ToLower();
                                                        string field_sec = "@" + nilaisec02.ToLower();

                                                        if (nilaimaintype02 == "string")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_01.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001))
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    string nilaiupdater001 = nilaiupdaterx001.ToString();
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaimaintype02 == "string_shortdate")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_01.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001) || nilaiupdaterx001 == "0/0/0000" || nilaiupdaterx001 == "1/01/0001 12:00:00 AM" || nilaiupdaterx001 == "1/01/0001 12:00:00 PG")
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    DateTime a1 = DateTime.ParseExact(nilaiupdaterx001, "d/M/yyyy hh:mm:ss tt", null);

                                                                    string a2 = a1.ToString();
                                                                    string nilaiupdater001 = a1.ToShortDateString();

                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaimaintype02 == "string_longdate")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_01.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001) || nilaiupdaterx001 == "0/0/0000" || nilaiupdaterx001 == "1/01/0001 12:00:00 AM" || nilaiupdaterx001 == "1/01/0001 12:00:00 PG")
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    DateTime a1 = DateTime.ParseExact(nilaiupdaterx001, "d/M/yyyy hh:mm:ss tt", null);

                                                                    string nilaiupdater001 = a1.ToString();
                                                                    string b2 = a1.ToShortDateString();

                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaimaintype02 == "numeric")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_01.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001))
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    decimal nilaiupdater001 = decimal.Parse(nilaiupdaterx001);
                                                                    string b2 = decimal.Parse(nilaiupdaterx001).ToString();
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaimaintype02 == "decimal")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_01.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001))
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    decimal nilaiupdater001 = decimal.Parse(nilaiupdaterx001);
                                                                    string b2 = decimal.Parse(nilaiupdaterx001).ToString();
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_01.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001))
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    string nilaiupdater001 = nilaiupdaterx001.ToString();
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_main, DBNull.Value);
                                                                }
                                                            }
                                                        }




                                                        if (nilaisectype02 == "string")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_02.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001))
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    string nilaiupdater001 = nilaiupdaterx001.ToString();
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaisectype02 == "string_shortdate")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_02.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001) || nilaiupdaterx001 == "0/0/0000" || nilaiupdaterx001 == "1/01/0001 12:00:00 AM" || nilaiupdaterx001 == "1/01/0001 12:00:00 PG")
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    DateTime a1 = DateTime.ParseExact(nilaiupdaterx001, "d/M/yyyy hh:mm:ss tt", null);

                                                                    string a2 = a1.ToString();
                                                                    string nilaiupdater001 = a1.ToShortDateString();

                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaisectype02 == "string_longdate")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_02.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001) || nilaiupdaterx001 == "0/0/0000" || nilaiupdaterx001 == "1/01/0001 12:00:00 AM" || nilaiupdaterx001 == "1/01/0001 12:00:00 PG")
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    DateTime a1 = DateTime.ParseExact(nilaiupdaterx001, "d/M/yyyy hh:mm:ss tt", null);

                                                                    string nilaiupdater001 = a1.ToString();
                                                                    string b2 = a1.ToShortDateString();

                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaisectype02 == "numeric")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_02.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001))
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    decimal nilaiupdater001 = decimal.Parse(nilaiupdaterx001);
                                                                    string b2 = decimal.Parse(nilaiupdaterx001).ToString();
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else if (nilaisectype02 == "decimal")
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_02.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001))
                                                            {
                                                                cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    decimal nilaiupdater001 = decimal.Parse(nilaiupdaterx001);
                                                                    string b2 = decimal.Parse(nilaiupdaterx001).ToString();
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, nilaiupdater001);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string nilaiupdaterx001 = nilaiupdaterselect_02.ToString();
                                                            if (string.IsNullOrEmpty(nilaiupdaterx001)) { cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value); }
                                                            else { try { string nilaiupdater001 = nilaiupdaterx001.ToString(); cmddis_updaterupdate.Parameters.AddWithValue(field_sec, nilaiupdater001); } catch (Exception) { cmddis_updaterupdate.Parameters.AddWithValue(field_sec, DBNull.Value); } }
                                                        }
                                                        conndis_updaterupdate.Open();
                                                        int rowsAffected = cmddis_updaterupdate.ExecuteNonQuery();

                                                        await Task.Delay(0);

                                                        if (rowsAffected == 0)
                                                        {
                                                            //khalis tiada update
                                                            //await syssts.StatusSystem_Updater(server_pdgb, nilai_database02, nilai_table02, null, null, "no_update", "no_update");
                                                        }
                                                        else
                                                        {
                                                            //khalis ada update
                                                            //await syssts.StatusSystem_Updater(server_pdgb, nilai_database02, nilai_table02, null, null, "done_update", "done_update");
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //khalis error 1
                                                        string short_error = ex.Message.ToString();
                                                        if (error_updatef1 == nilaiserver && error_updatef2 == nilaidatabase && error_updatef3 == nilaiscema02 + "." + nilaitable02 && error_updatef == short_error)
                                                        {

                                                        }
                                                        else
                                                        {
                                                            error_updatef = ex.Message.ToString();
                                                            error_updatef1 = nilaiserver;
                                                            error_updatef2 = nilaidatabase;
                                                            error_updatef3 = nilaiscema02 + "." + nilaitable02;

                                                            string combine_error1 = "~|nilai1:"+nilaiupdaterselect_01.ToString()+"~|";
                                                            string combine_error2 = "~|nilai1:" + nilaiupdaterselect_02.ToString() + "~|";
                                                            string combine_error3 = "~|detail error:"+ex.ToString()+"~|";
                                                            string combine_error = combine_error1 + combine_error2 + combine_error3;

                                                            await syssts.StatusSystem_Updater2(error_updatef1, error_updatef2, error_updatef3, nilaimain02, nilaisec02, "error", "error_update", short_error, combine_error);
                                                            await Task.Delay(0);
                                                        }
                                                    }
                                                    finally
                                                    {
                                                        conndis_updaterupdate.Close();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //khalis empty 1
                                            await syssts.StatusSystem_Updater2(sistem_servername, nilaidatabase01, nilaiscema01+"."+ nilaitable01, nilaimain01, nilaisec01, "empty", "empty_select",null,null);
                                            await Task.Delay(0);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //khalis error 2
                                        string short_error = ex.Message.ToString();
                                        if (error_selectf1 == sistem_servername && error_selectf2 == nilaidatabase01 && error_selectf3 == nilaiscema02 + "." + nilaitable02 && error_selectf == short_error)
                                        {

                                        }
                                        else
                                        {
                                            error_selectf = ex.Message.ToString();
                                            error_selectf1 = sistem_servername;
                                            error_selectf2 = nilaidatabase01;
                                            error_selectf3 = nilaiscema01 + "." + nilaitable01;

                                            string combine_error1 = "~|nilai1:" + nilaiupdaterselect_01.ToString() + "~|";
                                            string combine_error2 = "~|nilai1:" + nilaiupdaterselect_02.ToString() + "~|";
                                            string combine_error3 = "~|detail error:" + ex.ToString() + "~|";
                                            string combine_error = combine_error1 + combine_error2 + combine_error3;

                                            await syssts.StatusSystem_Updater2(error_selectf1, error_selectf2, error_selectf3, nilaimain02, nilaisec02, "error", "error_update", short_error, combine_error);
                                            await Task.Delay(0);
                                        }
                                    }
                                    finally
                                    {
                                        conndis_updaterselect.Close();
                                    }
                                }
                                //901025145214
                                await syssts.StatusSystem_Updater2(sistem_servername, nilaidatabase, nilaidatabase, null, null, "done", "DONE_SYSTEMUPDATE_PART", "Kemaskini Siap Pada Pangkalan Data" + nilaidatabase, null);
                                await Task.Delay(0);
                            }
                            else
                            {
                                //khalis empty 2
                                await syssts.StatusSystem_Updater2(sistem_servername, sistem_database, "dbo.dis_system_updater_all", "updaterall_database_02", null, "empty", "empty_select_table",null,null);
                                await Task.Delay(0);
                            }
                            await syssts.StatusSystem_Updater2(sistem_servername, sistem_database, "dbo.dis_system_updater_all", null, null, "done" , "DONE_SYSTEMUPDATE_" + error_updatef2, "Kemaskini Siap Pada Table " + error_updatef2, null);
                            await Task.Delay(0);
                        }
                        catch (Exception ex)
                        {
                            //khalis error 2
                            string short_error = ex.Message.ToString();
                            if (error_selecttbl1 == sistem_servername && error_selecttbl2 == sistem_database && error_selecttbl3 == "dbo.dis_system_updater_all" && error_selecttbl == short_error)
                            {

                            }
                            else
                            {
                                error_selecttbl = ex.Message.ToString();
                                error_selecttbl1 = sistem_servername;
                                error_selecttbl2 = sistem_database;
                                error_selecttbl3 = "dbo.dis_system_updater_all";

                                string combine_error1 = "~|nilai1:-~|";
                                string combine_error2 = "~|nilai1:-~|";
                                string combine_error3 = "~|detail error:" + ex.ToString() + "~|";
                                string combine_error = combine_error1 + combine_error2 + combine_error3;

                                await syssts.StatusSystem_Updater2(error_selecttbl1, error_selecttbl2, error_selecttbl3, nilaimain02, nilaisec02, "error", "error_select_table", short_error, combine_error);
                                await Task.Delay(0);
                            }
                        }
                        finally
                        {
                            disconn_pgdp01.Close();
                        }
                    }
                }
                else
                {
                    //khalis empty 2
                    await syssts.StatusSystem_Updater2(sistem_servername, sistem_database, "dbo.dis_database_connection", "dbconn_servername", "dbconn_serverenvironment", "empty", "empty_select_database",null,null);
                    await Task.Delay(0);
                }
                await syssts.StatusSystem_Updater2(sistem_servername, sistem_database, "dbo.dis_database_connection", null, null, "done", "DONE_SYSTEMINTEGRATION_PGDP", null, null);
            }
            catch (Exception ex)
            {
                //khalis error 2
                string short_error = ex.Message.ToString();
                if (error_selectdb1 == sistem_servername && error_selectdb2 == sistem_database && error_selectdb3 == "dbo.dis_system_updater_all" && error_selectdb == short_error)
                {

                }
                else
                {
                    error_selectdb = ex.Message.ToString();
                    error_selectdb1 = sistem_servername;
                    error_selectdb2 = sistem_database;
                    error_selectdb3 = "dbo.dis_database_connection";

                    string combine_error1 = "~|nilai1:-~|";
                    string combine_error2 = "~|nilai1:-~|";
                    string combine_error3 = "~|detail error:" + ex.ToString() + "~|";
                    string combine_error = combine_error1 + combine_error2 + combine_error3;

                    await syssts.StatusSystem_Updater2(error_selectdb1, error_selectdb2, error_selectdb3, nilaimain02, nilaisec02, "error", "error_select_database", short_error, combine_error);
                    await Task.Delay(0);
                }
            }
            finally
            {
                conndis_sdb2.Close();
            }
        }
    }
}
