﻿namespace DISJPSM_Integration
{
    partial class FormDatabaseCleaning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTotalcountStatusupdater = new System.Windows.Forms.Label();
            this.lblTotalcountStatusintegration = new System.Windows.Forms.Label();
            this.lblTotalcountStatusupdater2 = new System.Windows.Forms.Label();
            this.lblTotalcountStatusintegration2 = new System.Windows.Forms.Label();
            this.btnEmptyStatusintegration = new System.Windows.Forms.Button();
            this.btnEmptyStatusupdater = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTotalcountStatusupdater
            // 
            this.lblTotalcountStatusupdater.AutoSize = true;
            this.lblTotalcountStatusupdater.Location = new System.Drawing.Point(12, 61);
            this.lblTotalcountStatusupdater.Name = "lblTotalcountStatusupdater";
            this.lblTotalcountStatusupdater.Size = new System.Drawing.Size(130, 13);
            this.lblTotalcountStatusupdater.TabIndex = 0;
            this.lblTotalcountStatusupdater.Text = "[totalcount_statusupdater]";
            // 
            // lblTotalcountStatusintegration
            // 
            this.lblTotalcountStatusintegration.AutoSize = true;
            this.lblTotalcountStatusintegration.Location = new System.Drawing.Point(12, 9);
            this.lblTotalcountStatusintegration.Name = "lblTotalcountStatusintegration";
            this.lblTotalcountStatusintegration.Size = new System.Drawing.Size(143, 13);
            this.lblTotalcountStatusintegration.TabIndex = 1;
            this.lblTotalcountStatusintegration.Text = "[totalcount_statusintegration]";
            // 
            // lblTotalcountStatusupdater2
            // 
            this.lblTotalcountStatusupdater2.AutoSize = true;
            this.lblTotalcountStatusupdater2.Location = new System.Drawing.Point(161, 61);
            this.lblTotalcountStatusupdater2.Name = "lblTotalcountStatusupdater2";
            this.lblTotalcountStatusupdater2.Size = new System.Drawing.Size(136, 13);
            this.lblTotalcountStatusupdater2.TabIndex = 3;
            this.lblTotalcountStatusupdater2.Text = "[totalcount_statusupdater2]";
            // 
            // lblTotalcountStatusintegration2
            // 
            this.lblTotalcountStatusintegration2.AutoSize = true;
            this.lblTotalcountStatusintegration2.Location = new System.Drawing.Point(161, 9);
            this.lblTotalcountStatusintegration2.Name = "lblTotalcountStatusintegration2";
            this.lblTotalcountStatusintegration2.Size = new System.Drawing.Size(149, 13);
            this.lblTotalcountStatusintegration2.TabIndex = 2;
            this.lblTotalcountStatusintegration2.Text = "[totalcount_statusintegration2]";
            // 
            // btnEmptyStatusintegration
            // 
            this.btnEmptyStatusintegration.Location = new System.Drawing.Point(15, 25);
            this.btnEmptyStatusintegration.Name = "btnEmptyStatusintegration";
            this.btnEmptyStatusintegration.Size = new System.Drawing.Size(300, 23);
            this.btnEmptyStatusintegration.TabIndex = 4;
            this.btnEmptyStatusintegration.Text = "Empty Status Integration";
            this.btnEmptyStatusintegration.UseVisualStyleBackColor = true;
            this.btnEmptyStatusintegration.Click += new System.EventHandler(this.btnEmptyStatusintegration_Click);
            // 
            // btnEmptyStatusupdater
            // 
            this.btnEmptyStatusupdater.Location = new System.Drawing.Point(15, 77);
            this.btnEmptyStatusupdater.Name = "btnEmptyStatusupdater";
            this.btnEmptyStatusupdater.Size = new System.Drawing.Size(300, 23);
            this.btnEmptyStatusupdater.TabIndex = 5;
            this.btnEmptyStatusupdater.Text = "Empty Status Updater";
            this.btnEmptyStatusupdater.UseVisualStyleBackColor = true;
            this.btnEmptyStatusupdater.Click += new System.EventHandler(this.btnEmptyStatusupdater_Click);
            // 
            // FormDatabaseCleaning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 121);
            this.Controls.Add(this.btnEmptyStatusupdater);
            this.Controls.Add(this.btnEmptyStatusintegration);
            this.Controls.Add(this.lblTotalcountStatusupdater2);
            this.Controls.Add(this.lblTotalcountStatusintegration2);
            this.Controls.Add(this.lblTotalcountStatusintegration);
            this.Controls.Add(this.lblTotalcountStatusupdater);
            this.Name = "FormDatabaseCleaning";
            this.Text = "DIS_DatabaseCleaning";
            this.Load += new System.EventHandler(this.FormDatabaseCleaning_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTotalcountStatusupdater;
        private System.Windows.Forms.Label lblTotalcountStatusintegration;
        private System.Windows.Forms.Label lblTotalcountStatusupdater2;
        private System.Windows.Forms.Label lblTotalcountStatusintegration2;
        private System.Windows.Forms.Button btnEmptyStatusintegration;
        private System.Windows.Forms.Button btnEmptyStatusupdater;
    }
}