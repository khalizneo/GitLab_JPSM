﻿namespace DISJPSM_Integration
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.DISjpsmNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnMinimized = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCounter = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnOther = new System.Windows.Forms.Button();
            this.btnSML = new System.Windows.Forms.Button();
            this.btnESMASH = new System.Windows.Forms.Button();
            this.btnPGDP = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DISjpsmNotifyIcon
            // 
            this.DISjpsmNotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.DISjpsmNotifyIcon.BalloonTipText = "abc123";
            this.DISjpsmNotifyIcon.BalloonTipTitle = "def987";
            this.DISjpsmNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("DISjpsmNotifyIcon.Icon")));
            this.DISjpsmNotifyIcon.Text = "Database Integration System JPSM ver.1";
            this.DISjpsmNotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.DISjpsmNotifyIcon_MouseDoubleClick);
            // 
            // btnMinimized
            // 
            this.btnMinimized.Location = new System.Drawing.Point(372, 12);
            this.btnMinimized.Name = "btnMinimized";
            this.btnMinimized.Size = new System.Drawing.Size(100, 125);
            this.btnMinimized.TabIndex = 0;
            this.btnMinimized.Text = "Minimized";
            this.btnMinimized.UseVisualStyleBackColor = true;
            this.btnMinimized.Click += new System.EventHandler(this.btnMinimized_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.Location = new System.Drawing.Point(12, 87);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(150, 50);
            this.btnSetting.TabIndex = 3;
            this.btnSetting.Text = "Setting";
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Start in :";
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.Location = new System.Drawing.Point(55, 161);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(60, 13);
            this.lblCounter.TabIndex = 7;
            this.lblCounter.Text = "[lblCounter]";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(354, 40);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "Full Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnOther
            // 
            this.btnOther.Location = new System.Drawing.Point(168, 87);
            this.btnOther.Name = "btnOther";
            this.btnOther.Size = new System.Drawing.Size(198, 50);
            this.btnOther.TabIndex = 10;
            this.btnOther.Text = "Other";
            this.btnOther.UseVisualStyleBackColor = true;
            this.btnOther.Click += new System.EventHandler(this.btnOther_Click);
            // 
            // btnSML
            // 
            this.btnSML.Location = new System.Drawing.Point(12, 58);
            this.btnSML.Name = "btnSML";
            this.btnSML.Size = new System.Drawing.Size(100, 23);
            this.btnSML.TabIndex = 11;
            this.btnSML.Text = "Start SML";
            this.btnSML.UseVisualStyleBackColor = true;
            this.btnSML.Click += new System.EventHandler(this.btnSML_Click);
            // 
            // btnESMASH
            // 
            this.btnESMASH.Location = new System.Drawing.Point(118, 58);
            this.btnESMASH.Name = "btnESMASH";
            this.btnESMASH.Size = new System.Drawing.Size(102, 23);
            this.btnESMASH.TabIndex = 12;
            this.btnESMASH.Text = "Start eSmash";
            this.btnESMASH.UseVisualStyleBackColor = true;
            this.btnESMASH.Click += new System.EventHandler(this.btnESMASH_Click);
            // 
            // btnPGDP
            // 
            this.btnPGDP.Location = new System.Drawing.Point(226, 58);
            this.btnPGDP.Name = "btnPGDP";
            this.btnPGDP.Size = new System.Drawing.Size(140, 23);
            this.btnPGDP.TabIndex = 13;
            this.btnPGDP.Text = "Start PGDP";
            this.btnPGDP.UseVisualStyleBackColor = true;
            this.btnPGDP.Click += new System.EventHandler(this.btnPGDP_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(478, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 125);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 261);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPGDP);
            this.Controls.Add(this.btnESMASH);
            this.Controls.Add(this.btnSML);
            this.Controls.Add(this.btnOther);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lblCounter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSetting);
            this.Controls.Add(this.btnMinimized);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database Integration System JPSM Version 1 (DIS ver1)";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon DISjpsmNotifyIcon;
        private System.Windows.Forms.Button btnMinimized;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCounter;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnOther;
        private System.Windows.Forms.Button btnSML;
        private System.Windows.Forms.Button btnESMASH;
        private System.Windows.Forms.Button btnPGDP;
        private System.Windows.Forms.Button btnClose;
    }
}