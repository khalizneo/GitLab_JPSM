﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DISJPSM_Integration
{
    public partial class FormOther : Form
    {
        public FormOther()
        {
            InitializeComponent();
        }

        private void btnFormConnectionTester_Click(object sender, EventArgs e)
        {
            FormConnectionTester formconnectiontester = new FormConnectionTester();
            formconnectiontester.Show(this);
        }

        private void btnFormDatabaseCleaning_Click(object sender, EventArgs e)
        {
            FormDatabaseCleaning formdatabasecleaning = new FormDatabaseCleaning();
            formdatabasecleaning.Show(this);
        }

        private void btnFormIntegrationTotal_Click(object sender, EventArgs e)
        {
            FormIntegrationTotal formintegrationtotal = new FormIntegrationTotal();
            formintegrationtotal.Show(this);
        }
    }
}
